# Projecto de Sistemas Distribuídos #

## Primeira entrega ##

Grupo de SD 64

Tiago Simoes 73100 tiagosimoes@tecnico.ulisboa.pt

Joao Alexandre 73754 joaopmalexandre@hotmail.com

Rita Moreira 73756 ritasophiam@gmail.com


Repositório:
[tecnico-softeng-distsys-2015/A_21_56_64-project](https://github.com/tecnico-softeng-distsys-2015/A_21_56_64-project/)


-------------------------------------------------------------------------------

## Serviço SD-STORE

### Instruções de instalação

[0] Iniciar sistema operativo: Linux

[1] Iniciar servidores de apoio

JUDDI:
>$CATALINA_HOME/bin/startup.sh

[2] Criar pasta temporária

> mkdir /tmp/p64
> cd /tmp/p64

[3] Obter versão entregue

> git clone --branch R_3 https://github.com/tecnico-softeng-distsys-2015/A_21_56_64-project.git

[4] Construir e executar **servidor**

> cd /tmp/p64/A_21_56_64-project/
> cd ./uddi
> mvn compile install
> cd ./../sd-store
> mvn compile
> mvn exec:java


[5] Construir **cliente**

> cd /tmp/p64/A_21_56_64-project/sd-store-cli
> mvn compile

-------------------------------------------------------------------------------

### Instruções de teste: ###

[1] Executar **cliente de testes** ...
[1.1] Correr Testes createDocTest
Consola 1:
> cd /tmp/p64/A_21_56_64-project/sd-store/
> mvn exec:java
Consola 2:
> cd /tmp/p64/A_21_56_64-project/sd-store-cli
> mvn test -Dtest=createDocTest


[1.2] Correr Testes listDocsTest
Consola 1:
> cd /tmp/p64/A_21_56_64-project/sd-store/
> mvn exec:java
Consola 2:
> cd /tmp/p64/A_21_56_64-project/sd-store-cli
> mvn test -Dtest=listDocsTest

[1.3] Correr Testes loadTest
Consola 1:
> cd /tmp/p64/A_21_56_64-project/sd-store/
> mvn exec:java
Consola 2:
> cd /tmp/p64/A_21_56_64-project/sd-store-cli
> mvn test -Dtest=loadTest

[1.4] Correr Testes loadTest
Consola 1:
> cd /tmp/p64/A_21_56_64-project/sd-store/
> mvn exec:java
Consola 2:
> cd /tmp/p64/A_21_56_64-project/sd-store-cli
> mvn test -Dtest=storeTest

-------------------------------------------------------------------------------
**FIM**
