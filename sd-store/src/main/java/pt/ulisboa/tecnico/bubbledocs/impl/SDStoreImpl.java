package pt.ulisboa.tecnico.bubbledocs.impl;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.crypto.SecretKey;
import javax.jws.*;
import javax.xml.ws.WebServiceContext;
import javax.xml.ws.handler.MessageContext;

import pt.ulisboa.tecnico.bubbledocs.domain.Document;
import pt.ulisboa.tecnico.bubbledocs.domain.Repository;
import pt.ulisboa.tecnico.bubbledocs.domain.User;
import pt.ulisboa.tecnico.bubbledocs.handler.RelayServerHandler;
import pt.ulisboa.tecnico.sdis.store.ws.*; // classes generated from WSDL

@WebService(
		endpointInterface="pt.ulisboa.tecnico.sdis.store.ws.SDStore",
		wsdlLocation="SD-STORE.wsdl",
		name="SdStore",
		portName="SDStoreImplPort",
		targetNamespace="urn:pt:ulisboa:tecnico:sdis:store:ws",
		serviceName="SDStore")
@HandlerChain(file="/handler-chain.xml")
public class SDStoreImpl implements SDStore {

	private SDStoreApplication app = new SDStoreApplication();
	public static final String CLASS_NAME = SDStoreImpl.class.getSimpleName();
	public String TOKEN = "server";

	@Resource
	private WebServiceContext webServiceContext;

	public void createDoc(DocUserPair docUserPair) throws DocAlreadyExists_Exception  {
		
		// retrieve message context
        MessageContext messageContext = webServiceContext.getMessageContext();

        // *** #6 ***
        // get token from message context
        String propertyValue = (String) messageContext.get(RelayServerHandler.REQUEST_PROPERTY);
        System.out.printf("Server: %s got token '%s' from response context%n", CLASS_NAME, propertyValue);
        String splited[] = propertyValue.split(",");
        int userid = Integer.parseInt(splited[0]);

        // server processing
		String UserId = docUserPair.getUserId();
		String DocId = docUserPair.getDocumentId();
		if(!app.hasUser(UserId) && UserId != ""){			// If user does NOT exist
			User newUser = new User(UserId); // Then add a new user
			app.addUser(newUser);									// And create a empty doc for it
			app.createEmptyDocForThisUser(docUserPair, userid);
		}
		else{
			// If user exists
			Repository repository = app.getRepository(UserId);
			if(repository.hasDoc(DocId))		// Checks if doc already exists throw exception
				throw new DocAlreadyExists_Exception("This doc already exists", new DocAlreadyExists());

			repository.addDoc(DocId, userid);			// If it doesn't, then create an empty doc
		}
		// *** #7 ***
        // put token in message context
        String newValue = propertyValue;
        //+ "," + TOKEN;
        //System.out.printf("Server: %s put token '%s' on request context%n", CLASS_NAME, TOKEN);
        messageContext.put(RelayServerHandler.RESPONSE_PROPERTY, newValue);
	}

	public List<String> listDocs(String userId) throws UserDoesNotExist_Exception{

        // server processing
		if(!app.hasUser(userId))										// If user does NOT exist throw exception
			throw new UserDoesNotExist_Exception("User does not exist", 
					new UserDoesNotExist());
		List<String> listDocs = new ArrayList<String>();				// If user exists, gets its repository
		Repository repository = app.getRepository(userId);				// Get all doc names and put them in a list
		for(Document doc : repository.getDocsList()){
			int i = repository.getLatestVersion(doc.getDocId());
			if(doc.getVersion() == i){
				listDocs.add(doc.getDocId());
			}
		}
		return listDocs;												// return results
	}


	public void store(DocUserPair docUserPair, byte[] contents) throws DocDoesNotExist_Exception, UserDoesNotExist_Exception, CapacityExceeded_Exception{
		// retrieve message context
        MessageContext messageContext = webServiceContext.getMessageContext();

        // *** #6 ***
        // get token from message context
        String propertyValue = (String) messageContext.get(RelayServerHandler.REQUEST_PROPERTY);
        System.out.printf("Server: %s got token '%s' from response context%n", CLASS_NAME, propertyValue);
        String splited[] = propertyValue.split(",");
        int userid = Integer.parseInt(splited[0]);
        
        // server processing
		User user = app.getUser(docUserPair.getUserId());

		if(user == null)											// If user does NOT exist throw exception
			throw new UserDoesNotExist_Exception("User does not exist", 
					new UserDoesNotExist());


		Repository repository = app.getRepository(docUserPair.getUserId());
		int lastversion = repository.getLatestVersion(docUserPair.getDocumentId());
		Document doc = repository.getDoc(docUserPair.getDocumentId(), lastversion);

		if(doc == null)	{													//Se o documento não existir
			throw new DocDoesNotExist_Exception("Doc does not exist", 
					new DocDoesNotExist()); }


		byte[] actualContent = doc.getContent();

		int Totalength = actualContent.length + contents.length;
		byte[] resultContent = new byte[Totalength];

		//O total em bytes do tamanho dos contents
		int contentssize = 0;
		for(int b: contents){
			contentssize+= b;
		}

		if(repository.getUsedspace() + contentssize <= repository.getSize()){ //Se existe espaço, entra
			for(int i = 0; i < Totalength ; i++){     						  // Junta o content existente com o content a ser adicionado
				if(i < actualContent.length){
					resultContent[i] = actualContent[i];
				}
				else resultContent[i] = contents[i - actualContent.length];
			}
			if(repository.hasVersionsDoc(docUserPair.getDocumentId())){		//Se o Doc tiver duas versoes
				//repository.removeDoc(docUserPair.getDocumentId(), 0);
				//doc.setVersion(0);
				Document newDoc = new Document(docUserPair.getDocumentId(), lastversion + 1, userid, resultContent);
				repository.addDoc(newDoc);
				repository.setUsedspace(contentssize + repository.getUsedspace());
			}
			else{
				//doc.setVersion(0);
				Document newDoc = new Document(docUserPair.getDocumentId(), 1, userid, resultContent);
				repository.addDoc(newDoc);
				repository.setUsedspace(contentssize + repository.getUsedspace());
			}
		}
		else throw new CapacityExceeded_Exception("Capacity Exceeded", new CapacityExceeded());

		// *** #7 ***
        // put token in message context
        String newValue = propertyValue;
        //+ "," + TOKEN;
        //System.out.printf("Server: %s put token '%s' on request context%n", CLASS_NAME, TOKEN);
        messageContext.put(RelayServerHandler.RESPONSE_PROPERTY, newValue);
	}

	public byte[] load(DocUserPair docUserPair) throws UserDoesNotExist_Exception, DocDoesNotExist_Exception{

        // retrieve message context
        MessageContext messageContext = webServiceContext.getMessageContext();
        
        // server processing
		byte [] loadedDoc;
		int lastversion;
		int userid;
		if(app.hasUser((docUserPair.getUserId()))){										// If user exists
			Repository repository = app.getRepository(docUserPair.getUserId());
			if(repository.hasDoc(docUserPair.getDocumentId())){	// get document
				lastversion = repository.getLatestVersion(docUserPair.getDocumentId());
				Document doc = repository.getDoc(docUserPair.getDocumentId(), lastversion);
				userid = doc.getUserId();
				loadedDoc = new byte[doc.getContent().length];
				loadedDoc = doc.getContent();// and load it
			}
			else { 
				throw new DocDoesNotExist_Exception("Document does not exist", new DocDoesNotExist());
			}
		}
		else{										// If user does NOT exist throw exception
			throw new UserDoesNotExist_Exception("User does not exist", new UserDoesNotExist());
		}
		// *** #7 ***
        // put token in message context
		TOKEN = String.valueOf(lastversion);
        String newValue = TOKEN + "," + userid;
        System.out.printf("Server: %s put token '%s' on request context%n", CLASS_NAME, TOKEN);
        messageContext.put(RelayServerHandler.RESPONSE_PROPERTY, newValue);
		return loadedDoc;
	}

}
