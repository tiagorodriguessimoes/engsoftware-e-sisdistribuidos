package pt.ulisboa.tecnico.bubbledocs.domain;

import java.util.ArrayList;
import java.util.List;

public class Repository {
	private int size;
	private int usedspace;
	private List<Document> docsList = new ArrayList<Document>();
	
	/**
	 * @param create a repository with default size 
	 */
	public Repository(){
		this.size = 10240000;  // 1024 bytes = 1 kbyte * 1000 = 1 Mbyte * 10 = 10 Mbytes
		this.usedspace = 0;
	}
	/**
	 * @param create a repository with this size 
	 */
	public Repository(int size){
		this.size = size;
		this.usedspace = 0;
	}
	/**
	 * @return the size
	 */
	public int getSize() {
		return this.size;
	}
	/**
	 * @param size the size to set
	 */
	public void setSize(int size) {
		this.size = size;
	}
	/**
	 * @return the docsList
	 */
	public List<Document> getDocsList() {
		return this.docsList;
	}
	/**
	 * @param docsList the docsList to set
	 */
	public void setDocsList(List<Document> docsList) {
		this.docsList = docsList;
	}
	/**
	 * @return check if doc exists (version 1 or 0, whatever is first)
	 */
	public boolean hasDoc(String docId){
		for(Document doc : getDocsList())
			if(docId.equals(doc.getDocId()))
				return true;
		return false;
	}
	/**
	 * @return doc (version 1 or 0, whatever is first)
	 */
	public Document getDoc(String docId){
		for(Document doc : getDocsList())
			if(docId.equals(doc.getDocId()))
				return doc;
		return null;
	}
	/**
	 * @return get doc by version
	 */
	public Document getDoc(String docId,int version){
		for(Document doc : getDocsList())
			if(docId.equals(doc.getDocId()) && version==doc.getVersion())
				return doc;
		return null;
	}
	
	public boolean hasVersionsDoc(String docId){
		
		int i = 0;
		for(Document doc : getDocsList())
			if(docId.equals(doc.getDocId()))
				i++;
		
		if(i == 2){
			return true;
		}
		else return false;
	}
	
	public int getLatestVersion(String docId){
		int i = -1;
		for(Document doc : getDocsList()){
			if(docId.equals(doc.getDocId())){
				if(doc.getVersion() > i)
					i = doc.getVersion();
			}
		}
		
		return i;
	}
	/**
	 * @param add doc
	 */
	public void addDoc(Document doc){
		docsList.add(doc);
	}
	/**
	 * @param add doc (version 0)
	 */
	public void addDoc(String docId, int userid){
		Document newDoc = new Document(docId, 0, userid);
		docsList.add(newDoc);
	}
	
	public void removeDoc(String docId, int version){

		for(Document doc : getDocsList())
			if(docId.equals(doc.getDocId()) && version==doc.getVersion())
				docsList.remove(doc);
	}
	
	public int getUsedspace() {
		return this.usedspace;
	}
	public void setUsedspace(int usedspace) {
		this.usedspace = usedspace;
	}
}
