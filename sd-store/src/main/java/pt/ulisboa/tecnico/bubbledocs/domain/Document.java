package pt.ulisboa.tecnico.bubbledocs.domain;

public class Document {

	private String DocId;
	private int Version;
	private int Userid;
	private byte[] Content;
	
	public Document() {
		super();
	}
	
	public Document(String docId, int version, int userid){
		super();
		this.setDocId(docId);
		this.setUserId(Userid);
		this.setVersion(version);
		Content = new byte[0];
	}
	
	public Document(String docId, int version, int Userid, byte[] content) {
		super();
		this.setDocId(docId);
		this.setVersion(version);
		this.setUserId(Userid);
		Content = new byte[content.length];
		this.setContent(content);
	}

	public String getDocId() {
		return DocId;
	}

	public void setDocId(String docId) {
		DocId = docId;
	}

	public int getVersion() {
		return Version;
	}

	public void setVersion(int version) {
		Version = version;
	}
	
	public int getUserId() {
		return Userid;
	}

	public void setUserId(int id) {
		Userid = id;
	}

	public byte[] getContent() {
		return Content;
	}

	public void setContent(byte[] content2) {
		Content = content2;
	}
}
