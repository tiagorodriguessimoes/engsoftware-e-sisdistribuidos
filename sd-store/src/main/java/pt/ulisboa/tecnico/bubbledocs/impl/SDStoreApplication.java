package pt.ulisboa.tecnico.bubbledocs.impl;

import static javax.xml.bind.DatatypeConverter.printHexBinary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.SecretKey;

import pt.ulisboa.tecnico.bubbledocs.domain.Repository;
import pt.ulisboa.tecnico.bubbledocs.domain.User;
import pt.ulisboa.tecnico.sdis.store.ws.DocUserPair;

public class SDStoreApplication {
	private List<User> usersList = new ArrayList<User>();

	public SDStoreApplication(){

	}

	/**
	 * @return the usersList
	 */
	public List<User> getUsersList() {
		return usersList;
	}

	/**
	 * @param usersList the usersList to set
	 */
	public void setUsersList(List<User> usersList) {
		this.usersList = usersList;
	}

	public void addUser(User newUser){
		newUser.createRepository();						// create a repository for him and add him to list.
		usersList.add(newUser);
	}

	public boolean hasUser(String username){
		for(User user : getUsersList())
			if(user.getUsername().equals(username))
				return true;
		return false;
	}


	public User getUser(String username){
		for(User user : getUsersList())
			if(user.getUsername().equals(username))
				return user;
		return null;
	}

	public Repository getRepository(String username){
		User user = getUser(username);
		return user.getRepository();
	}

	public void createEmptyDocForThisUser(DocUserPair docUserPair, int userid){
		Repository repository = getRepository(docUserPair.getUserId());
		repository.addDoc(docUserPair.getDocumentId(), userid);
	}
	
	/** auxiliary method to calculate new digest from text and compare it to the
    to deciphered digest */
	public static boolean verifyMAC(byte[] cipherDigest,
			byte[] bytes,
			SecretKey key) throws Exception {

		Mac cipher = Mac.getInstance("HmacMD5");
		cipher.init(key);
		byte[] cipheredBytes = cipher.doFinal(bytes);
		return Arrays.equals(cipherDigest, cipheredBytes);
	}
}
