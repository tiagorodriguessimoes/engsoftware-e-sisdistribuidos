package pt.ulisboa.tecnico.bubbledocs.domain;

public class User {

	private String username;
	private Repository repository;
	
	public User() {
		super();
	}
	
	public User(String username) {
		super();
		this.setUsername(username);
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	/**
	 * @return the repository
	 */
	public Repository getRepository() {
		return this.repository;
	}

	/**
	 * @param set repository size
	 */
	public void setRepository(int size) {
		this.repository.setSize(size);
	}
	/**
	 * @param create repository with specific size
	 */
	public void createRepository(int size){
		this.repository = new Repository(size);
	}
	
	public void createRepository(){
		this.repository = new Repository();
	}
}
