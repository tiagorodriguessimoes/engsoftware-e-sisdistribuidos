package pt.ulisboa.tecnico.bubbledocs.cli;

public class MissingServersException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public MissingServersException(int connected, int needed){
		System.err.println("Only connected to "+connected+" servers. You need at least "+needed+" more to work");
	}
}
