package pt.ulisboa.tecnico.bubbledocs.keys;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import pt.ulisboa.tecnico.sdis.store.ws.DocUserPair;

public class Key {
	private DocUserPair docUserPair;
	private SecretKey key;
	private byte[] digestCipher;
	private Cipher cipher;
	


	public Key(DocUserPair docUserPair, SecretKey key, byte[] digestCipher, Cipher cipher){
		setDocUserPair(docUserPair);
		setKey(key);
		setDigestCipher(digestCipher);
		setCipher(cipher);
	}
	/**
	 * @return the docUserPair
	 */
	public DocUserPair getDocUserPair() {
		return docUserPair;
	}
	/**
	 * @param docUserPair the docUserPair to set
	 */
	public void setDocUserPair(DocUserPair docUserPair) {
		this.docUserPair = docUserPair;
	}
	
	/**
	 * @return the key
	 */
	public SecretKey getKey() {
		return key;
	}
	/**
	 * @param key the key to set
	 */
	public void setKey(SecretKey key) {
		this.key = key;
	}
	/**
	 * @return the digestCipher
	 */
	public byte[] getDigestCipher() {
		return digestCipher;
	}
	/**
	 * @param digestCipher the to digestCipher
	 */
	public void setDigestCipher(byte[] digestCipher) {
		this.digestCipher = digestCipher;
	}
	/**
	 * @return the cipher
	 */
	public Cipher getCipher() {
		return cipher;
	}
	/**
	 * @param cipher the cipher to set
	 */
	public void setCipher(Cipher cipher) {
		this.cipher = cipher;
	}
}
