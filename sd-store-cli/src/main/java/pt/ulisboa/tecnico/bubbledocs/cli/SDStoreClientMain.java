package pt.ulisboa.tecnico.bubbledocs.cli;

import static javax.xml.bind.DatatypeConverter.printHexBinary;

import javax.xml.registry.JAXRException;

import pt.ulisboa.tecnico.sdis.store.ws.DocUserPair;

public class SDStoreClientMain {

	public static void main(String[] args) {

		if (args.length < 5) {
			System.err.println("Argument(s) missing!");
			System.err.printf("Usage: java %s uddiURL name clientId WT RT%n", SDStoreClientMain.class.getName());
			return;
		}

		String uddiURL = args[0];
		String name = args[1];
		String clientId = args[2];
		String WT = args[3];
		String RT = args[4];
		SDStoreClientFrontEnd frontEnd;


		try {

			frontEnd = new SDStoreClientFrontEnd(uddiURL,name,clientId,WT,RT);

			System.out.println("Available Servers: "+frontEnd.GetListServers());
			System.out.println("Remote call ...");

			// REMOTE CALLS HERE
//			DocUserPair docUserPair = new DocUserPair();
//			docUserPair.setDocumentId("doc");
//			docUserPair.setUserId("user");
//			frontEnd.createDoc(docUserPair);
//
//			String plainText = "ola";
//			byte[] plainBytes = plainText.getBytes();
//			System.out.println();
//			System.out.println("Text:");
//			System.out.println(plainText);
//			System.out.println("Bytes:");
//			System.out.println(printHexBinary(plainBytes));
//			System.out.println();
//
//
//			frontEnd.store(docUserPair, plainBytes);
//
//
//			System.out.println("load result: ");
//
//			System.out.println();
//			byte[] newPlainBytes = frontEnd.load(docUserPair);
//			System.out.println(printHexBinary(newPlainBytes));
//
//			System.out.println("Text:");
//			String newPlainText = new String(newPlainBytes); 
//			System.out.println(newPlainText);
//



		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}




	}
}
