package pt.ulisboa.tecnico.bubbledocs.keys;

import java.util.ArrayList;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import pt.ulisboa.tecnico.sdis.store.ws.DocUserPair;

public class KeyManagerBubbleDocs {
	private String ClientId;
	private List<Key> keysList = new ArrayList<Key>();
	
	public KeyManagerBubbleDocs(String ClientId){
		this.setClientId(ClientId);
	}
	
	public List<Key> getKeysList() {
		return this.keysList;
	}
	
	public void addKeys(DocUserPair docUserPair, SecretKey key, byte[] digestCipher,Cipher cipher){
		Key newKey = new Key(docUserPair,key,digestCipher,cipher);
		keysList.add(newKey);
	}
	
	
	public Key getKey(DocUserPair docUserPair){
		for(Key key : getKeysList()){
			if(key.getDocUserPair().equals(docUserPair))
				return key;
		}
		return null;
	}
	
	/**
	 * @return the clientId
	 */
	public String getClientId() {
		return ClientId;
	}

	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		ClientId = clientId;
	}
}
