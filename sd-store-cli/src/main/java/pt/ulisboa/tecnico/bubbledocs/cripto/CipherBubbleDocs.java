package pt.ulisboa.tecnico.bubbledocs.cripto;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

import javax.crypto.Cipher;
import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

public class CipherBubbleDocs {
	public CipherBubbleDocs(){
		
	}
	
	public SecretKey generate() throws Exception {
		// generate a DES secret key
		KeyGenerator keyGen = KeyGenerator.getInstance("DES");
		keyGen.init(56);
		return keyGen.generateKey();
	}

	/** auxiliary method to make the MAC */
	public byte[] makeMAC(byte[] bytes, SecretKey key) throws Exception {

		Mac cipher = Mac.getInstance("HmacMD5");
		cipher.init(key);
		byte[] cipherDigest = cipher.doFinal(bytes);

		//System.out.println("CipherDigest:");
		//System.out.println(printHexBinary(cipherDigest));

		return cipherDigest;
	}

	/** auxiliary method to calculate new digest from text and compare it to the
         to deciphered digest */
	public boolean verifyMAC(byte[] cipherDigest, byte[] bytes, SecretKey key) throws Exception {

		Mac cipher = Mac.getInstance("HmacMD5");
		cipher.init(key);
		byte[] cipheredBytes = cipher.doFinal(bytes);
		return Arrays.equals(cipherDigest, cipheredBytes);
	}
	
	public byte[] cipherContents(byte[] contents, SecretKey key, Cipher cipher) throws Exception{
		// generate a DES Key
		// get a DES cipher object and print the provider
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] cipherBytes = cipher.doFinal(contents);
		return cipherBytes;
	}

	public byte[] decipherContents(byte[] cipherBytes, SecretKey key, Cipher cipher) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException{
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] newPlainBytes = cipher.doFinal(cipherBytes);
		return newPlainBytes;
	}
	
	public Cipher getDESCipher() throws NoSuchAlgorithmException, NoSuchPaddingException{
		return Cipher.getInstance("DES/ECB/PKCS5Padding");
	}
}
