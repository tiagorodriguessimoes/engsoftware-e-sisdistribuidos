package pt.ulisboa.tecnico.bubbledocs.cli;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.xml.registry.JAXRException;
import javax.xml.ws.BindingProvider;

import pt.ulisboa.tecnico.bubbledocs.cripto.CipherBubbleDocs;
import pt.ulisboa.tecnico.bubbledocs.handler.*;
import pt.ulisboa.tecnico.bubbledocs.keys.Key;
import pt.ulisboa.tecnico.bubbledocs.keys.KeyManagerBubbleDocs;
import static javax.xml.bind.DatatypeConverter.printHexBinary;
import static javax.xml.ws.BindingProvider.ENDPOINT_ADDRESS_PROPERTY;
import example.ws.uddi.UDDINaming;
import pt.ulisboa.tecnico.sdis.store.ws.*;

import pt.ulisboa.tecnico.bubbledocs.cli.MissingServersException;

public class SDStoreClientFrontEnd {

	public static final String CLASS_NAME = SDStoreClientMain.class.getSimpleName();
	public static final String TOKEN = "client";
	private String ClientId;
	private SDStore port;
	private String nameServers;
	private UDDINaming uddiNaming;
	private String WT;
	private String RT;
	private CipherBubbleDocs cipherBubbleDocs;
	private KeyManagerBubbleDocs keyManagerBubbleDocs;

	public SDStoreClientFrontEnd(String uddiURL, String name,String ClientId, String WT, String RT) throws JAXRException {
		this.ClientId = ClientId;
		this.nameServers = name;
		this.RT = RT;
		this.WT = WT;
		this.cipherBubbleDocs = new CipherBubbleDocs();
		this.keyManagerBubbleDocs = new KeyManagerBubbleDocs(ClientId);
		// contact uddi
		System.out.printf("Contacting UDDI at %s%n", uddiURL);
		uddiNaming = new UDDINaming(uddiURL);
		// create stub
		System.out.println("Creating stub ...");
		SDStore_Service service = new SDStore_Service();
		port = service.getSDStoreImplPort();
		System.out.println("ID : "+ ClientId);
		System.out.println("WT : "+ WT);
		System.out.println("RT : "+ RT);
	}

	public void createDoc(DocUserPair docUserPair){
		try{
			for(String url : GetListServers()){
				BindingProvider bindingProvider = (BindingProvider) port;
				Map<String, Object> requestContext = bindingProvider.getRequestContext();
				// *** #1 ***
				// put token in request context
				String initialValue = this.ClientId; 
				//envia id do cliente para o servidor por handler
				//System.out.printf("Client: %s put token '%s' on request context%n", CLASS_NAME, initialValue);
				requestContext.put(RelayClientHandler.REQUEST_PROPERTY, initialValue);
				// set endpoint address
				requestContext.put(ENDPOINT_ADDRESS_PROPERTY, url);
				// make remote call
				port.createDoc(docUserPair);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	} 

	public List<String> listDocs(String userId) {
		//		try {
		//			return port.listDocs(userId);
		//		} catch (UserDoesNotExist_Exception e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		return null;
	}

	public void store(DocUserPair docUserPair, byte[] contents) {
		try {
			// Generate Key
			SecretKey key = cipherBubbleDocs.generate();
			// Cipher contents with DES
			Cipher cipher = cipherBubbleDocs.getDESCipher();
			byte[] cypheredContents = cipherBubbleDocs.cipherContents(contents,key,cipher);
			// make MAC
			byte[] cipherDigest = cipherBubbleDocs.makeMAC(cypheredContents, key);
			// k -> KeyManager for this Client
			Key k = keyManagerBubbleDocs.getKey(docUserPair);
			// add data to KeyManager
			keyManagerBubbleDocs.addKeys(docUserPair, key, cipherDigest, cipher);
			// Contact all servers
			for(String url : GetListServers()){
				BindingProvider bindingProvider = (BindingProvider) port;
				Map<String, Object> requestContext = bindingProvider.getRequestContext();
				// *** #1 ***
				// put token in request context
				String initialValue = this.ClientId; //envia id do cliente para o servidor por handler
				//System.out.printf("Client: %s put token '%s' on request context%n", CLASS_NAME, initialValue);
				requestContext.put(RelayClientHandler.REQUEST_PROPERTY, initialValue);
				// set endpoint address
				requestContext.put(ENDPOINT_ADDRESS_PROPERTY, url);
				// make remote call
				port.store(docUserPair, cypheredContents);
				// access response context
				Map<String, Object> responseContext = bindingProvider.getResponseContext();
				// *** #12 ***
				// get token from response context
				String finalValue = (String) responseContext.get(RelayClientHandler.RESPONSE_PROPERTY);
				//System.out.printf("Client: %s got token '%s' from response context%n", CLASS_NAME, finalValue);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public byte[] load(DocUserPair docUserPair) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException{
		try {
			if(Integer.parseInt(this.RT) > GetTotalServers()){
				throw new MissingServersException(GetTotalServers(),Integer.parseInt(this.RT));
			}
			else{
			// stuff for version control
			int[] versions = new int[GetTotalServers()];
			int[] clientids = new int[GetTotalServers()];
			byte[][] allcontentsstored = new byte[GetTotalServers()][];
			int position = 0;
			// Contact all servers
			for(String url : GetListServers()){
				BindingProvider bindingProvider = (BindingProvider) port;
				Map<String, Object> requestContext = bindingProvider.getRequestContext();
				// *** #1 ***
				// put token in request context

				String initialValue = this.ClientId; //envia id do cliente para o servidor por handler
				//				System.out.printf("Client: %s put token '%s' on request context%n", CLASS_NAME, initialValue);
				requestContext.put(RelayClientHandler.REQUEST_PROPERTY, initialValue);
				// set endpoint address
				requestContext.put(ENDPOINT_ADDRESS_PROPERTY, url);
				// get Key, Cipher and Digest Cipher for this Document
				Key k = keyManagerBubbleDocs.getKey(docUserPair);
				byte[] cipherDigest = k.getDigestCipher();
				SecretKey key = k.getKey();
				Cipher cipher = k.getCipher();
				// receive encrypted file
				byte[] dataReceived = port.load(docUserPair);
				// if file is tampered throws error
				// else continues
				if(!cipherBubbleDocs.verifyMAC(cipherDigest, dataReceived, key)){
					System.err.println("WRONG DIGEST");
					byte[] EmptyContent={};
					allcontentsstored[position] = EmptyContent;
					// access response context
					Map<String, Object> responseContext = bindingProvider.getResponseContext();
					// *** #12 ***
					// get token from response context
					String finalValue = (String) responseContext.get(RelayClientHandler.RESPONSE_PROPERTY);
					
					versions[position] = -1;
					clientids[position]= -1;
					position++;
				}
				else{
					//System.out.println(printHexBinary(cipherBubbleDocs.decipherContents(dataReceived,key,cipher)));
					allcontentsstored[position] = cipherBubbleDocs.decipherContents(dataReceived,key,cipher);
					//
					// access response context
					Map<String, Object> responseContext = bindingProvider.getResponseContext();
					// *** #12 ***
					// get token from response context
					String finalValue = (String) responseContext.get(RelayClientHandler.RESPONSE_PROPERTY);

					// string split para dividir as informacoes do handler <id do cliente, versao do doc >
					String[] parts = finalValue.split(",");
					// colocar a versao num array e comparar no final do for
					versions[position] = Integer.parseInt(parts[0]);
					clientids[position]= Integer.parseInt(parts[1]);


					//System.out.printf("Client: %s got token '%s' from response context%n", CLASS_NAME, finalValue);
					position++;
				}
			}
			int bestidclient = -1;  
			int bestindice = 0;

			int versaopopular = findPopular(versions);       // versao mais popular nos gestores de replicacao
			for(int i = 0; i < versions.length; i++){        //comparacao de idclients para saber a melhor tag
				if(versions[i] == versaopopular){
					if(clientids[i] > bestidclient){
						bestidclient = clientids[i];
						bestindice = i;
					}
				}
			}

			// MELHOR TAG = < versaopopular , bestidclient > com indice bestindice
			return allcontentsstored[bestindice];
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


	public Collection<String> GetListServers () throws JAXRException{
		return this.uddiNaming.list(this.nameServers);
	}

	public int GetTotalServers() throws JAXRException{
		return GetListServers().size();
	}

	public int findPopular(int[] a) {

		int indice = 0;
		int indicemaisfrequente = 0;
		int novocontador = 0;
		int contador = 0;
		int tudolido = 0;

		while(tudolido < a.length){
			for(int ver : a){
				if(ver == indice){
					novocontador++;
				}
			}
			if(contador < novocontador){
				contador = novocontador;
				indicemaisfrequente = indice;
			}
			tudolido += novocontador;
			novocontador = 0;
			indice++;
		}

		return indicemaisfrequente;
	}
}
