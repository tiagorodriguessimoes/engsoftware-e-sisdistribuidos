package pt.ulisboa.tecnico.bubbledocs.impl;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.jws.WebService;
import javax.xml.parsers.ParserConfigurationException;

import org.jdom2.JDOMException;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.joda.time.LocalTime;
import org.xml.sax.SAXException;

import pt.ulisboa.tecnico.bubbledocs.domain.User;
import pt.ulisboa.tecnico.sdis.id.ws.AuthReqFailed_Exception;
import pt.ulisboa.tecnico.sdis.id.ws.EmailAlreadyExists_Exception;
import pt.ulisboa.tecnico.sdis.id.ws.InvalidEmail_Exception;
import pt.ulisboa.tecnico.sdis.id.ws.InvalidUser_Exception;
import pt.ulisboa.tecnico.sdis.id.ws.SDId;
import pt.ulisboa.tecnico.sdis.id.ws.UserAlreadyExists_Exception;
import pt.ulisboa.tecnico.sdis.id.ws.UserDoesNotExist_Exception;

@WebService(endpointInterface = "pt.ulisboa.tecnico.sdis.id.ws.SDId", wsdlLocation = "SD-ID.wsdl", name = "SdId", portName = "SDIdImplPort", targetNamespace = "urn:pt:ulisboa:tecnico:sdis:id:ws", serviceName = "SDId")
public class SDIdImpl implements SDId {

	private static SDIdApplication app = new SDIdApplication();

	public void createUser(String userId, String emailAddress)
			throws EmailAlreadyExists_Exception, InvalidEmail_Exception,
			InvalidUser_Exception, UserAlreadyExists_Exception {
		System.out.println("CHEGUEI AQUI");
		// verificar user
		if (userId == null || userId.length() == 0) {
			throw new InvalidUser_Exception(userId, null);
		}
		// verificar mail (a@b)
		if (emailAddress == null) {
			throw new InvalidEmail_Exception(emailAddress, null);
		}
		String[] auxmail = emailAddress.split("@");
		// mail invalido: a@, @b, a, ...
		if (auxmail.length < 2 || auxmail[0].length() < 1
				|| auxmail[1].length() < 1) {
			throw new InvalidEmail_Exception(emailAddress, null);
		}

		User u = new User(userId, emailAddress);

		System.out.println(u.getPassword());

		for (User user : app.getUsersList()) {
			if (user.getUserid().equals(u.getUserid())
					&& user.getMail().equals(u.getMail())) {
				throw new UserAlreadyExists_Exception(userId, null);
			} else if (user.getUserid().equals(u.getUserid())) {
				throw new UserAlreadyExists_Exception(userId, null);
			} else if (user.getMail().equals(u.getMail())) {
				throw new EmailAlreadyExists_Exception(emailAddress, null);
			}
		}
		app.addUser(u);
	}

	public void renewPassword(String userId) throws UserDoesNotExist_Exception {
		String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		Random rnd = new Random();

		if (app.existUser(userId)) {
			StringBuilder sb = new StringBuilder(9);
			for (int i = 0; i < 4; i++)
				sb.append(AB.charAt(rnd.nextInt(AB.length())));
			app.getUser(userId).setPassword(sb.toString());
		} else
			throw new UserDoesNotExist_Exception(userId, null);
	}

	public void removeUser(String userId) throws UserDoesNotExist_Exception {
		if (app.existUser(userId)) {
			app.getUsersList().remove(app.getUser(userId));
		} else
			throw new UserDoesNotExist_Exception(userId, null);
	}

	public byte[] requestAuthentication(String userId, byte[] reserved)
			throws AuthReqFailed_Exception {
		System.out.println("USER: "+ userId);
		LocalTime itime= new LocalTime();
		LocalTime etime= itime.plusHours(2);
		User u = app.getUser(userId);
		String password=new String();
		password=u.getPassword();
		byte[] bPassword = password.getBytes();
		byte[] encryptedPass= new byte[]{};
		byte[] ks=new byte[]{};
		byte[] kc=new byte[]{};
		try{
		KeyGenerator keyGen = KeyGenerator.getInstance("DES");
		keyGen.init(56);
		 Key keyc = keyGen.generateKey();
		 Key keys = keyGen.generateKey();
//		// Generating Cipher
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");

//		// Encryptation
		cipher.init(Cipher.ENCRYPT_MODE, keyc);
		encryptedPass = cipher.doFinal(bPassword);
		ks=keys.getEncoded();
		kc=keyc.getEncoded();}
		catch(NoSuchAlgorithmException |IllegalBlockSizeException
				| InvalidKeyException |NoSuchPaddingException|BadPaddingException e ){}
		byte[] returnarray=new byte[]{};;
		try {
			ReservedXML xml = new ReservedXML(reserved);
			org.jdom2.Document doc = xml.convertToXML();
			System.out.println("I'M HERE");
	//		ticketXML ticket= new ticketXML(u.getUserid(), servicename, starttime, endtime, sessionkey)
			/*XMLOutputter xmlprint = new XMLOutputter();
			xmlprint.setFormat(Format.getPrettyFormat());
			System.out.println(xmlprint.outputString(xml.convertToXML()));*/
			
			if (!(xml.getServername().equals("SD-ID"))) {
				throw new AuthReqFailed_Exception(userId, null);
			}

			int nounce = xml.getNounce();

			if (userId != null && reserved != null) {
				if (!(app.existUser(userId))) {
					throw new AuthReqFailed_Exception(userId, null);
				}
			}
			
			clientcredXML clientcredxml = new clientcredXML(encryptedPass,ks, nounce);
			ticketXML ticketxml = new ticketXML(userId,"SD-STORE", itime.toString(),etime.toString() , ks);
			servercredXML servercredxml = new servercredXML(ticketxml.convertXMLtoByte()); 
			CredentialsXML credentials = new CredentialsXML(clientcredxml.convertXMLtoByte(), servercredxml.convertXMLtoByte()); 
			returnarray = credentials.convertXMLtoByte();
					 
			/*
			 * byte[] ks; byte[] dataencryptedkey; String startime; String
			 * endtime;
			 * 
			 * clientcredXML clientcredxml = new clientcredXML(dataencryptedkey,
			 * ks, nounce); ticketXML ticketxml = new ticketXML(userId,
			 * "SD-STORE", startime, endtime, ks); servercredXML servercredxml =
			 * new servercredXML(ticketxml.convertXMLtoByte()); CredentialsXML
			 * credentials = new
			 * CredentialsXML(clientcredxml.convertXMLtoByte(),
			 * servercredxml.convertXMLtoByte()); byte[] returnarray =
			 * credentials.convertXMLtoByte();
			 */
		} catch (JDOMException | IOException | SAXException
				| ParserConfigurationException e) {
			e.getStackTrace();
		}
	
		return returnarray;
	}

	// for testing
	static void reset() {
		app.getUsersList().clear();

		User a = new User("alice", "alice@tecnico.pt");
		a.setPassword("Aaa1");
		User b = new User("bruno", "bruno@tecnico.pt");
		b.setPassword("Bbb2");
		User c = new User("carla", "carla@tecnico.pt");
		c.setPassword("Ccc3");
		User d = new User("duarte", "duarte@tecnico.pt");
		d.setPassword("Ddd4");
		User e = new User("eduardo", "eduardo@tecnico.pt");
		e.setPassword("Eee5");

		app.getUsersList().add(a);
		app.getUsersList().add(b);
		app.getUsersList().add(c);
		app.getUsersList().add(d);
		app.getUsersList().add(e);
	}

}
