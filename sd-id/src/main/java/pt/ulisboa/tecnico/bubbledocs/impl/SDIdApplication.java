package pt.ulisboa.tecnico.bubbledocs.impl;

import java.util.ArrayList;
import java.util.List;

import pt.ulisboa.tecnico.bubbledocs.domain.User;

public class SDIdApplication {
	private List<User> list = new ArrayList<User>();

	public SDIdApplication() {}
	
	/**
	 * @return the usersList
	 */
	public List<User> getUsersList() {
		return list;
	}
	
	/**
	 * @param usersList the usersList to set
	 */
	public void setUsersList(List<User> usersList) {
		this.list = usersList;
	}

	public void addUser(User user){
		list.add(user);
	}
	
	public User getUser(String userId){
		for(User user : list){
			if(user.getUserid().equals(userId)){
				return user;
			}
		}
		return null;
	}
	
	public boolean existUser(String userId){
		boolean exist = false;
		for(User user : list){
			if(user.getUserid().equals(userId)){
				exist = true;
			}
		}
		return exist;
	}
}
