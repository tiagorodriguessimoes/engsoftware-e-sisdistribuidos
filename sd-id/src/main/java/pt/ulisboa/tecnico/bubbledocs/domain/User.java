package pt.ulisboa.tecnico.bubbledocs.domain;

import java.util.Random;

import pt.ulisboa.tecnico.sdis.id.ws.UserDoesNotExist_Exception;

public class User {
	private String userid;
	private String mail;
	private String password;
	
	public User(String userid, String mail) {
		this.userid = userid;
		this.mail = mail;
		String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
		Random rnd = new Random();
		StringBuilder sb = new StringBuilder(9);
		for(int i = 0; i < 4; i++) 
		      sb.append(AB.charAt(rnd.nextInt(AB.length())));
		this.password = sb.toString();
	}
	
	public String getUserid() {
		return userid;
	}
	
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	public String getMail() {
		return mail;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public String toString() {
		return "User [userid=" + userid + ", mail=" + mail + ", password="
				+ password + "]";
	}
}
