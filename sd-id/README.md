# Projecto de Sistemas Distribu�dos #

## Primeira entrega ##

Grupo de SD 56

Diogo Cruz 72861 diogocruz_93@hotmail.com

Miguel Ferreira 74152 miguel.oliveira.f@hotmail.com

Pedro Vieira 72742 pmfv93@gmail.com


Reposit�rio:
[tecnico-softeng-distsys-2015/A_21_56_64-project](https://github.com/tecnico-softeng-distsys-2015/A_21_56_64-project/)


-------------------------------------------------------------------------------

## Servi�o SD-ID

### Instru��es de instala��o 
*(Como colocar o projecto a funcionar numa m�quina do laborat�rio)*

[0] Iniciar sistema operativo

Indicar Windows ou Linux
*(escolher um dos dois, que esteja dispon�vel nos laborat�rios)*


[1] Iniciar servidores de apoio

JUDDI:
> ...

[2] Criar pasta tempor�ria

> cd ...
> mkdir ...

[3] Obter vers�o entregue

> git clone ... 
*(comandos git para obter a vers�o entregue - tag)*


[4] Construir e executar **servidor**

> cd ...
> mvn clean package 
> mvn exec:java


[5] Construir **cliente**

> cd ...
> mvn clean package

...


-------------------------------------------------------------------------------

### Instru��es de teste: ###
*(Como verificar que todas as funcionalidades est�o a funcionar correctamente)*


[1] Executar **cliente de testes** ...

> cd ...
> mvn test


[2] Executar ...



...


-------------------------------------------------------------------------------
**FIM**