package pt.ulisboa.tecnico.bubbledocs.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;

public class ReservedXML {
	private String servername;
	private int nounce;
	private org.jdom2.Document doc;
	
	public String getServername() {
		return servername;
	}
	public void setServername(String servername) {
		this.servername = servername;
	}
	public int getNounce() {
		return nounce;
	}
	public void setNounce(int nounce) {
		this.nounce = nounce;
	}
	public org.jdom2.Document getDoc() {
		return doc;
	}
	public void setDoc(org.jdom2.Document doc) {
		this.doc = doc;
	}
	public ReservedXML(String servername, int nounce){
		this.servername = servername;
		this.nounce = nounce;
		this.doc = new org.jdom2.Document();
	}
	public ReservedXML(byte[] xml) throws JDOMException, IOException, SAXException, ParserConfigurationException{
		/*InputStream stream = new ByteArrayInputStream(xml);
		DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
		fac.setNamespaceAware(true);
		DocumentBuilder build = fac.newDocumentBuilder();
		this.doc = new Document();
		this.doc = (Document) build.parse(stream);
		*/
		InputStream stream = new ByteArrayInputStream(xml);
		String d = new String(xml, "UTF-8");
		System.out.println("D:" + d);
		try{
			SAXBuilder builder = new SAXBuilder();
			this.doc= new Document();
			this.doc = builder.build(stream);
		}
		catch(JDOMException | IOException e){
			//throw e;
		}
		
		XMLOutputter xmlprint = new XMLOutputter();
		xmlprint.setFormat(Format.getPrettyFormat());
		System.out.println(xmlprint.outputString(this.doc));
		
		//if(this.doc.toString().contains("reserved")){
		//Element root = this.doc.getRootElement();
		//this.servername = root.getChildText("service-name");
		//this.nounce = Integer.parseInt(root.getChildText("nounce"));
		//}
		//else System.out.println("SDLIHGBFSDPUIGODFSIBGSERUBGSEUIYGKB");*/
		
	}
		
	
	public org.jdom2.Document convertToXML(){
		Element element = new Element("reserved");
		
		Element server = new Element("service-name");
		server.setText(this.servername);
		element.addContent(server);
		
		Element nounce = new Element("nounce");
		nounce.setText(String.valueOf(this.nounce));
		element.addContent(nounce);
		
		return this.doc.setRootElement(element);
	}
	
	public byte[] convertXMLtoByte(){
		XMLOutputter xml = new XMLOutputter();
		return xml.outputString(this.doc).getBytes();
	}
}