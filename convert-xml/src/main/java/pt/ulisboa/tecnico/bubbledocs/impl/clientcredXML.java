package pt.ulisboa.tecnico.bubbledocs.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;

public class clientcredXML {
	private byte[] dataencryptedkey;
	private byte[] sessionkey;
	private int nounce;	
	private org.jdom2.Document doc;

	public clientcredXML(byte[] dataencryptedkey, byte[] sessionkey, int nounce) {
		this.dataencryptedkey = dataencryptedkey;
		this.sessionkey = sessionkey;
		this.nounce = nounce;
		this.doc = new org.jdom2.Document();
	}
	
	public byte[] getDataencryptedkey() {
		return dataencryptedkey;
	}

	public void setDataencryptedkey(byte[] dataencryptedkey) {
		this.dataencryptedkey = dataencryptedkey;
	}

	public byte[] getSessionkey() {
		return sessionkey;
	}

	public void setSessionkey(byte[] sessionkey) {
		this.sessionkey = sessionkey;
	}

	public int getNounce() {
		return nounce;
	}

	public void setNounce(int nounce) {
		this.nounce = nounce;
	}

	public org.jdom2.Document getDoc() {
		return doc;
	}

	public void setDoc(org.jdom2.Document doc) {
		this.doc = doc;
	}

	public clientcredXML(byte[] xml) throws JDOMException, IOException, SAXException, ParserConfigurationException{
		InputStream stream = new ByteArrayInputStream(xml);
		DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
		fac.setNamespaceAware(true);
		DocumentBuilder build = fac.newDocumentBuilder();
		org.jdom2.Document docaux = (Document) build.parse(stream);
		
		Element root = docaux.getRootElement();
		Element dataencryptedkey = root.getChild("dataencryptedkey");
		this.dataencryptedkey = DatatypeConverter.parseBase64Binary(dataencryptedkey.getText());
		Element sessionkey = root.getChild("sessionkey");
		this.sessionkey = DatatypeConverter.parseBase64Binary(sessionkey.getText());
		this.nounce = Integer.parseInt(root.getChildText("nounce"));
		
	}
	
	public org.jdom2.Document convertToXML(){
		Element element = new Element("clientcred");
		
		Element data = new Element("data-encrypted-key");
		String d = DatatypeConverter.printBase64Binary(this.dataencryptedkey);
		data.setText(d);
		element.addContent(data);
		
		Element session = new Element("session-key");
		String s = DatatypeConverter.printBase64Binary(this.sessionkey);
		session.setText(s);
		element.addContent(session);
		
		Element nounce = new Element("nounce");
		nounce.setText(String.valueOf(this.nounce));
		element.addContent(nounce);
		
		return this.doc.setRootElement(element);
	}
	
	public byte[] convertXMLtoByte(){
		XMLOutputter xml = new XMLOutputter();
		return xml.outputString(this.doc).getBytes();
	}
}

//byte[] s = DatatypeConverter.parseBase64Binary(string);
//string s = DatatypeConverter.printBase64Binary(byte[]);
