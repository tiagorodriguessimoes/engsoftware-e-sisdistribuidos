package pt.ulisboa.tecnico.bubbledocs.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;

public class CredentialsXML {
	private byte[] clientcred;
	private byte[] servercred;
	private org.jdom2.Document doc;
	
	public byte[] getClientcred() {
		return clientcred;
	}

	public void setClientcred(byte[] clientcred) {
		this.clientcred = clientcred;
	}

	public byte[] getServercred() {
		return servercred;
	}

	public void setServercred(byte[] servercred) {
		this.servercred = servercred;
	}

	public org.jdom2.Document getDoc() {
		return doc;
	}

	public void setDoc(org.jdom2.Document doc) {
		this.doc = doc;
	}

	public CredentialsXML(byte[] clientcred, byte[] servercred) {
		this.clientcred = clientcred;
		this.servercred = servercred;
		this.doc = new org.jdom2.Document();
	}
	
	public CredentialsXML(byte[] xml) throws JDOMException, IOException, SAXException, ParserConfigurationException{
		InputStream stream = new ByteArrayInputStream(xml);
		DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
		fac.setNamespaceAware(true);
		DocumentBuilder build = fac.newDocumentBuilder();
		org.jdom2.Document docaux = (Document) build.parse(stream);
		
		Element root = docaux.getRootElement();
		Element clientcred = root.getChild("client-credentials");
		this.clientcred = DatatypeConverter.parseBase64Binary(clientcred.getText());
		Element servercred = root.getChild("server-credentials");
		this.servercred = DatatypeConverter.parseBase64Binary(servercred.getText());
	}
	
	public org.jdom2.Document convertToXML(){
		Element element = new Element("clientcred");
		
		Element client = new Element("client-credentials");
		String d = DatatypeConverter.printBase64Binary(this.clientcred);
		client.setText(d);
		element.addContent(client);
		
		Element server = new Element("server-credentials");
		String s = DatatypeConverter.printBase64Binary(this.servercred);
		server.setText(s);
		element.addContent(server);
		
		return this.doc.setRootElement(element);
	}
	
	public byte[] convertXMLtoByte(){
		XMLOutputter xml = new XMLOutputter();
		return xml.outputString(this.doc).getBytes();
	}
}
