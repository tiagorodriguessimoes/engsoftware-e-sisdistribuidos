package pt.ulisboa.tecnico.bubbledocs.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;

public class ticketXML {
	private String username;
	private String servicename;
	private String starttime;
	private String endtime;
	private byte[] sessionkey;
	private org.jdom2.Document doc;
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getServicename() {
		return servicename;
	}

	public void setServicename(String servicename) {
		this.servicename = servicename;
	}

	public String getStarttime() {
		return starttime;
	}

	public void setStarttime(String starttime) {
		this.starttime = starttime;
	}

	public String getEndtime() {
		return endtime;
	}

	public void setEndtime(String endtime) {
		this.endtime = endtime;
	}

	public byte[] getSessionkey() {
		return sessionkey;
	}

	public void setSessionkey(byte[] sessionkey) {
		this.sessionkey = sessionkey;
	}

	public org.jdom2.Document getDoc() {
		return doc;
	}

	public void setDoc(org.jdom2.Document doc) {
		this.doc = doc;
	}

	public ticketXML(String username, String servicename, String starttime,
			String endtime, byte[] sessionkey) {
		this.username = username;
		this.servicename = servicename;
		this.starttime = starttime;
		this.endtime = endtime;
		this.sessionkey = sessionkey;
		this.doc = new org.jdom2.Document();
	}
	
	public ticketXML(byte[] xml) throws JDOMException, IOException, SAXException, ParserConfigurationException{
		InputStream stream = new ByteArrayInputStream(xml);
		DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
		fac.setNamespaceAware(true);
		DocumentBuilder build = fac.newDocumentBuilder();
		org.jdom2.Document docaux = (Document) build.parse(stream);
		
		Element root = docaux.getRootElement();
		this.username = root.getChildText("username");
		this.servicename = root.getChildText("service-name");
		this.starttime = root.getChildText("starttime");
		this.endtime = root.getChildText("endtime");
		Element sessionkey = root.getChild("sessionkey");
		this.sessionkey = DatatypeConverter.parseBase64Binary(sessionkey.getText());
	}
	
	public org.jdom2.Document convertToXML(){
		Element element = new Element("ticket");
		
		Element name = new Element("username");
		name.setText(this.username);
		element.addContent(name);
		
		Element server = new Element("service-name");
		server.setText(this.servicename);
		element.addContent(server);

		Element stime = new Element("startime");
		stime.setText(this.starttime);
		element.addContent(stime);
		
		Element etime = new Element("endtime");
		etime.setText(this.endtime);
		element.addContent(etime);
		
		Element session = new Element("session-key");
		String s = DatatypeConverter.printBase64Binary(this.sessionkey);
		session.setText(s);
		element.addContent(session);
		
		return this.doc.setRootElement(element);
	}
	
	public byte[] convertXMLtoByte(){
		XMLOutputter xml = new XMLOutputter();
		return xml.outputString(this.doc).getBytes();
	}
	
}
