package pt.ulisboa.tecnico.bubbledocs.impl;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.output.XMLOutputter;
import org.xml.sax.SAXException;

public class servercredXML {
	private byte[] ticket;
	private org.jdom2.Document doc;
	
	public servercredXML(byte[] xml) throws JDOMException, IOException, SAXException, ParserConfigurationException{
		InputStream stream = new ByteArrayInputStream(xml);
		DocumentBuilderFactory fac = DocumentBuilderFactory.newInstance();
		fac.setNamespaceAware(true);
		DocumentBuilder build = fac.newDocumentBuilder();
		org.jdom2.Document docaux = (Document) build.parse(stream);
		this.doc = docaux;
		
		Element root = docaux.getRootElement();
		Element ticket = root.getChild("ticket");
		this.ticket = DatatypeConverter.parseBase64Binary(ticket.getText());
	}
	
	public byte[] getTicket() {
		return ticket;
	}
	public void setTicket(byte[] ticket) {
		this.ticket = ticket;
	}
	public org.jdom2.Document getDoc() {
		return doc;
	}
	public void setDoc(org.jdom2.Document doc) {
		this.doc = doc;
	}
	public org.jdom2.Document convertToXML(){
		Element element = new Element("clientcred");
		
		Element ticket = new Element("ticket");
		String t = DatatypeConverter.printBase64Binary(this.ticket);
		ticket.setText(t);
		element.addContent(ticket);
		
		return this.doc.setRootElement(element);
	}
	
	public byte[] convertXMLtoByte(){
		XMLOutputter xml = new XMLOutputter();
		return xml.outputString(this.doc).getBytes();
	}
}
