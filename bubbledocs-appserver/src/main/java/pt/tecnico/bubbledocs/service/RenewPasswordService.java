package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;

public class RenewPasswordService extends BubbleDocsService {
	private String username;
	
	public RenewPasswordService(String userId){
		this.username = userId;
	}
	
	@Override
	public void dispatch() {
		checkAutorizathion();
		bd.getUserByUsername(this.username).setPassword(null);
	}
	
	@Override
	protected void checkAutorizathion() {
		if(bd.getUserByUsername(username)==(null))
			throw new UserDoesNotExistException();
	}
}