package pt.tecnico.bubbledocs.service.remote;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.xml.registry.JAXRException;

import pt.tecnico.bubbledocs.exception.CannotLoadDocumentException;
import pt.tecnico.bubbledocs.exception.CannotStoreDocumentException;
import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.ulisboa.tecnico.bubbledocs.cli.SDStoreClientFrontEnd;
import pt.ulisboa.tecnico.sdis.store.ws.CapacityExceeded_Exception;
import pt.ulisboa.tecnico.sdis.store.ws.DocDoesNotExist_Exception;
import pt.ulisboa.tecnico.sdis.store.ws.DocUserPair;
import pt.ulisboa.tecnico.sdis.store.ws.UserDoesNotExist_Exception;

public class StoreRemoteServices {
	private SDStoreClientFrontEnd client;
	private static final String UDDI_URL = "http://localhost:8081/";
	private static final String SERVER_NAME = "sdstore";
	private static final String CLIENT_ID = "1";

	public StoreRemoteServices(){
		try{
			this.client = new SDStoreClientFrontEnd(UDDI_URL,SERVER_NAME,CLIENT_ID, "1", "2");
		}catch(JAXRException e){
			e.printStackTrace();
		}
	}

	public void storeDocument(String username, String docName, byte[] document)
			throws CannotStoreDocumentException, RemoteInvocationException {
		DocUserPair docUserPair = new DocUserPair();
		docUserPair.setDocumentId(docName);
		docUserPair.setUserId(username);

		this.client.store(docUserPair, document);
	}
	public byte[] loadDocument(String username, String docName)
			throws CannotLoadDocumentException, RemoteInvocationException {
		DocUserPair docUserPair = new DocUserPair();
		docUserPair.setDocumentId(docName);
		docUserPair.setUserId(username);

		try {
			return this.client.load(docUserPair);
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalBlockSizeException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;

	} 
}