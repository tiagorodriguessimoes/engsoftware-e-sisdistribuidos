package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;

public class LoginUserService extends BubbleDocsService {

	private String userToken;
	private String username;
	private String password;

	public LoginUserService(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public final String getUserToken() {
		return this.userToken;
	}

	@Override
	protected void dispatch() throws BubbleDocsException {
		checkAutorizathion();								
		this.userToken = sm.CreateSession(username);
	}

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		User u = bd.getUserByUsername(username);
		if(u == null)
			throw new LoginBubbleDocsException(username,password);
		/*if(!u.getPassword().equals(password))
			throw new LoginBubbleDocsException(username,password);*/
		if(sm.UserHasValidSession(username))
			sm.DeleteSession(username);
	}
}
