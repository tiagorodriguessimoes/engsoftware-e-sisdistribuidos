package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;

public class GetUserInfoService extends BubbleDocsService{

	private String username;
	private User user;

	public GetUserInfoService(String username){
		this.username = username;
	}

	@Override
	protected void dispatch() throws BubbleDocsException {
		boolean found = false;
		for(User u : bd.getUserSet())
			if(u.getUsername().equals(this.username)){
				this.user = u;
				found = true;
				break;
			}
		if(!found)
			throw new UserDoesNotExistException(this.username);
	}

	public String getUsername(){
		return this.user.getUsername();
	}
	
	public String getName(){
		return this.user.getUsername();
	}

	public String getEmail(){
		return this.user.getEmail();
	}
	// Add more if needed
	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		// TODO Auto-generated method stub

	}
}
