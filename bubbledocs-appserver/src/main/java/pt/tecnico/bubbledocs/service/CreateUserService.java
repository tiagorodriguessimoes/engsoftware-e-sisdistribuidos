package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.DuplicateEmailException;
import pt.tecnico.bubbledocs.exception.DuplicateUsernameException;
import pt.tecnico.bubbledocs.exception.SessionExpiredException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UsernameAlreadyExistsException;

public class CreateUserService extends BubbleDocsService {

	private String userToken;
	private String newUsername;
	private String name;
	private String email;

	public CreateUserService(String userToken, String newUsername, String email, String name) {
		this.userToken = userToken;
		this.newUsername = newUsername;
		this.name = name;
		this.email = email;
	}

	@Override
	protected void dispatch() throws UsernameAlreadyExistsException {
		checkAutorizathion();
		checkOther();
		User user = new User(newUsername,name, email);
		bd.addUser(user);
	}

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		if(!userToken.equals("root"))
			throw new UserNoPermissionException(this.newUsername);
		if(!sm.UserHasValidSession("root"))
			throw new SessionExpiredException(newUsername);
	}
	
	protected void checkOther() throws BubbleDocsException {
		if(bd.getUserByUsername(this.newUsername) != null)
			throw new DuplicateUsernameException(newUsername);
		if(bd.emailExists(email))
			throw new DuplicateEmailException(email);
	}
}
