package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.ColumnsWithoutSizeException;
import pt.tecnico.bubbledocs.exception.EmptyNameException;
import pt.tecnico.bubbledocs.exception.RowsWithoutSizeException;
import pt.tecnico.bubbledocs.exception.SessionExpiredException;

public class CreateSpreadSheetService extends BubbleDocsService {

	private String sheetId;  // id of the new sheet
	private String name;
	private int rows;
	private int columns;
	private String username; 

	public String getSheetId() {
		return sheetId;
	}

	public CreateSpreadSheetService(String userToken, String name, int rows, int columns) {
		this.name = name;
		this.rows = rows;
		this.columns = columns;
		this.username = sm.getUsernameByToken(userToken);
	}

	@Override
	protected void dispatch() throws BubbleDocsException,SessionExpiredException {
		checkAutorizathion();
		checkOther();
		SpreadSheet ss = new SpreadSheet(name, rows, columns, username);
		bd.addSpreadsheet(ss);
		AddAccessToSheetService aats = new AddAccessToSheetService(username, ss.getId(), true);
		aats.dispatch();
	}

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		if(!sm.UserHasValidSession(username))
			throw new SessionExpiredException(username);
		sm.RenewSession(username);
	}
	
	protected void checkOther() throws BubbleDocsException {
		if(rows<=0) 
			throw new RowsWithoutSizeException();
		if(columns<=0) 
			throw new ColumnsWithoutSizeException();
		if(name.length() <=0)
			throw new EmptyNameException();
	}

}