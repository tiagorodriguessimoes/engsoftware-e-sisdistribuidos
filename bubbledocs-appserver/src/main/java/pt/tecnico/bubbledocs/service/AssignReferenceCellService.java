package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.domain.Access;
import pt.tecnico.bubbledocs.domain.Cell;
import pt.tecnico.bubbledocs.domain.Reference;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.OperationException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;

public class AssignReferenceCellService extends BubbleDocsService {

	private String result;
	private String username;
	private String ssid;
	private String cell;
	private String ref;
	
	public AssignReferenceCellService(String username, String docId, String cellId, String reference) {
		this.username = username;
		this.ssid = docId;
		this.cell = cellId;
		this.ref = reference;
		//this.username = sm.getUsernameByToken(userToken);
	}

	@Override
	protected void dispatch() throws BubbleDocsException {
		checkAutorizathion();
		String delim = ";";
		String[] cellTok = cell.split(delim);
		String[] refTok = ref.split(delim);
		int lin = Integer.parseInt(cellTok[0]);
		int col = Integer.parseInt(cellTok[1]);
		int rl = Integer.parseInt(refTok[0]);
		int rc = Integer.parseInt(refTok[1]);
		User u = bd.getUserByUsername(username);
		SpreadSheet s = bd.getSpreadSheetByID(ssid);
		

		if(!(lin>0 &&lin<s.getMaxlines() &&col>0 &&col<s.getMaxcolumns() &&rl>0 &&rl<s.getMaxlines() &&rc>0 &&rc<s.getMaxcolumns()))
			throw new OperationException();
		for(Access a: u.getAccessSet())
			if (a.getSpreadsheet().getId().equals(s.getId())) {
				if (a.getPermission() == false) 
					throw new UserNoPermissionException(username);
				s.getCellfromSS(lin,col).setContent(new Reference(new Cell(rl,rc,false)));
			} 
	}

	public final String getResult() {
		return result;
	}

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		if(sm.UserHasValidSession(username) == false)
			throw new UserNotInSessionException(username);
		sm.RenewSession(username);
	}
}