package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.domain.Access;
import pt.tecnico.bubbledocs.domain.Literal;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.OperationException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;


public class AssignLiteralCellService extends BubbleDocsService {
	private String result;
	private String username;
	private String ssid;
	private String cell;
	private String lit;

	public AssignLiteralCellService(String accessUsername, String docId, String cellId, String literal) {
		this.username = accessUsername;
		this.ssid = docId;
		this.cell = cellId;
		this.lit = literal;
	}

	@Override
	protected void dispatch() throws BubbleDocsException {
		checkAutorizathion();
		String delim = ";";
		String[] cellTok = cell.split(delim);
		int lin = Integer.parseInt(cellTok[0]);
		int col = Integer.parseInt(cellTok[1]);
		int literalvalue = Integer.parseInt(lit);
		User u = bd.getUserByUsername(username);
		SpreadSheet s = bd.getSpreadSheetByID(ssid);

		if(lin>0 && lin<s.getMaxlines() && col>0 && col<s.getMaxcolumns()){
			for(Access a: u.getAccessSet())
				s.getCellfromSS(lin,col).setContent(new Literal(literalvalue));
		}else 
			throw new OperationException();
	}

	public String getResult() {
		return result;
	}

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		if(!sm.UserHasValidSession(username))
			throw new UserNotInSessionException(username);
		if(bd.getUserByUsername(username).getAccessBySpreadsheet(bd.getSpreadSheetByID(ssid)) == false)
			throw new UserNoPermissionException(username);
		sm.RenewSession(username);
	}

}