package pt.tecnico.bubbledocs.service;

import pt.ist.fenixframework.Atomic;
import pt.tecnico.bubbledocs.domain.BubbleDocs;
import pt.tecnico.bubbledocs.domain.SessionManager;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;

public abstract class BubbleDocsService {

	protected BubbleDocs bd = BubbleDocs.getInstance();
	protected SessionManager sm = bd.getSessionmanager();

	@Atomic
	public final void execute() throws BubbleDocsException {
		dispatch();
	}

	protected abstract void dispatch() throws BubbleDocsException;

	protected abstract void checkAutorizathion() throws BubbleDocsException;

}



