package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;

public class GetUsername4TokenService extends BubbleDocsService{
	private String token;
	private String username;
	
	public GetUsername4TokenService (String token){
		this.token = token;
	}

	public String getResult(){
		return this.username;
	}
	
	@Override
	protected void dispatch() throws BubbleDocsException {
		this.username = sm.getUsernameByToken(token);
	}

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		// TODO Auto-generated method stub
		
	}

}
