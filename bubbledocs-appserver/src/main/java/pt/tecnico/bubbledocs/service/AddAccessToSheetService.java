package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.domain.Access;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;

public class AddAccessToSheetService extends BubbleDocsService {
	private String username;
	private String documentId;
	private boolean perm;

	public AddAccessToSheetService(String accessUsername, String docId,boolean type) {
		this.username = accessUsername;
		this.documentId = docId;
		this.perm = type;
	}

	@Override
	protected void dispatch() throws BubbleDocsException {
		checkAutorizathion();
		
		SpreadSheet s= bd.getSpreadSheetByID(documentId);
		boolean owner = false;
		if(s.getFounder().equals(username)) 
			owner=true;
		Access a = new Access(perm,owner);
		s.addAccess(a);
		User u = bd.getUserByUsername(this.username);
		u.addAccess(a);
	}

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		if((!sm.UserHasValidSession(username)))
			throw new UserNotInSessionException(username);
		sm.RenewSession(username);
	}
}