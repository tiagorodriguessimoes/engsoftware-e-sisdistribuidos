package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.domain.ADD;
import pt.tecnico.bubbledocs.domain.Access;
import pt.tecnico.bubbledocs.domain.ArgLit;
import pt.tecnico.bubbledocs.domain.ArgRef;
import pt.tecnico.bubbledocs.domain.Argument;
import pt.tecnico.bubbledocs.domain.Cell;
import pt.tecnico.bubbledocs.domain.Content;
import pt.tecnico.bubbledocs.domain.DIV;
import pt.tecnico.bubbledocs.domain.Literal;
import pt.tecnico.bubbledocs.domain.MUL;
import pt.tecnico.bubbledocs.domain.SUB;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.InvalidOperatorException;
import pt.tecnico.bubbledocs.exception.OperationException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;

public class AssignBinaryFucntionToCellService extends BubbleDocsService{

	private String cell;
	private String binFunction;
	private String ssId;
	private String userToken;
	private int result;
	
	
	public AssignBinaryFucntionToCellService(String cellId, String expression, String docId, String token) {
		this.cell = cellId;
		this.binFunction = expression;
		this.ssId = docId;
		this.userToken = token;

		
	}
	@Override
	protected void dispatch() throws BubbleDocsException { //ADD(5;2 , 3;5)
		// TODO Auto-generated method stub
		checkAutorizathion();
		String delim = ";";
		String delim2 = "\\(";
		String delim3 = ",";
		String delim4 = "\\)";
		String[] cellTok = cell.split(delim);
		int lin = Integer.parseInt(cellTok[0]);
		int col = Integer.parseInt(cellTok[1]);
		String[] exprTok = binFunction.split(delim2);
		String[] exprTok2 = exprTok[1].split(delim3);
		String[] exprTok3 = exprTok2[1].split(delim4);
		String funct = exprTok[0];
		String firstArg = exprTok2[0];
		String secondArg = exprTok3[0];
		Argument arg1 = checkReference (firstArg);
		Argument arg2 = checkReference (secondArg);
		
		if (funct.equals("ADD")) {
			ADD a = new ADD(arg1, arg2);
			//###ver se o a.calc nao da merda
			result = Integer.parseInt(a.calc());
		}
		else if (funct.equals("SUB")) {
			SUB s = new SUB(arg1, arg2);
			result = Integer.parseInt(s.calc());
			}
		else if(funct.equals("DIV")) {
			DIV d = new DIV(arg1, arg2);
			result = Integer.parseInt(d.calc());
			}
		else if(funct.equals("MUL")) {
			MUL m = new MUL(arg1, arg2);
			result = Integer.parseInt(m.calc());
		}
		else throw new InvalidOperatorException();
		
		String username = sm.getUsernameByToken(userToken);
		User u = bd.getUserByUsername(username);
		SpreadSheet s = bd.getSpreadSheetByID(ssId);
		
		if(lin>0 && lin<s.getMaxlines() && col>0 && col<s.getMaxcolumns()){
			for(Access a: u.getAccessSet())
				s.getCellfromSS(lin,col).setContent(new Literal(result));
		}else 
			throw new OperationException();
	}	

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		String username = sm.getUsernameByToken(userToken);
		if(!sm.UserHasValidSession(username))
			throw new UserNotInSessionException(username);
		if(bd.getUserByUsername(username).getAccessBySpreadsheet(bd.getSpreadSheetByID(ssId)) == false)
			throw new UserNoPermissionException(username);
		sm.RenewSession(username);
		
	}
	
	Argument checkReference (String arg){ //5;2
		String[] reference = arg.split(";");
		if (reference.length == 1)
			return new ArgLit(Integer.parseInt(reference[0]));
		//adicionar erro caso os argumentos nao sejam inteiros
		int lin = Integer.parseInt(reference[0]);
		int col = Integer.parseInt(reference[1]);
		SpreadSheet ss = bd.getSpreadSheetByID(ssId);
		Cell c = ss.getCellfromSS(lin, col);
		return new ArgRef(c);
	}
	
	int getResult(){
		return result;
	}

}
