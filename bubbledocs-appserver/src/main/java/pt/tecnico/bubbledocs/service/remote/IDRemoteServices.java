package pt.tecnico.bubbledocs.service.remote;

import pt.tecnico.bubbledocs.exception.DuplicateEmailException;
import pt.tecnico.bubbledocs.exception.DuplicateUsernameException;
import pt.tecnico.bubbledocs.exception.InvalidEmailException;
import pt.tecnico.bubbledocs.exception.InvalidUsernameException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.ulisboa.tecnico.bubbledocs.cli.SDIdClient;
import pt.ulisboa.tecnico.bubbledocs.cli.SDIdClientException;

public class IDRemoteServices {
	private SDIdClient client;
	private static final String UDDI_URL = "http://localhost:8081/";
	
	public IDRemoteServices(){
		try {
			this.client = new SDIdClient(UDDI_URL);
		} catch (SDIdClientException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void createUser(String username, String email) throws InvalidUsernameException, 
			DuplicateUsernameException,
			DuplicateEmailException, 
			InvalidEmailException,
			RemoteInvocationException{		
		this.client.createUser(username, email);
	}

	public void loginUser(String username, String password)
			throws LoginBubbleDocsException, RemoteInvocationException {
		this.client.requestAuthentication(username, password.getBytes());
	}

	public void removeUser(String username) throws LoginBubbleDocsException,
			RemoteInvocationException {
		this.client.removeUser(username);
	}

	public void renewPassword(String username) throws LoginBubbleDocsException,
			RemoteInvocationException {
		this.client.renewPassword(username);
	}
}