package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.NotSessionNorRootException;
import pt.tecnico.bubbledocs.exception.RootNotInSessionException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;

public class DeleteUserService extends BubbleDocsService {

	private String userToken;
	private String toDeleteUsername;

    public DeleteUserService(String userToken, String toDeleteUsername) {
    	this.userToken = userToken;
    	this.toDeleteUsername = toDeleteUsername;
    }

    @Override
    protected void dispatch() throws BubbleDocsException {
    	checkAutorizathion();
    	User user = bd.getUserByUsername(toDeleteUsername);
    	user.delete();
    }

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
    	if(!sm.UserHasValidSession("root"))
    		throw new RootNotInSessionException();
    	if((!sm.UserHasValidSession(toDeleteUsername))&&(!userToken.equals("root")))
    		throw new NotSessionNorRootException();
    	if(!(userToken.equals("root")))
    		throw new UserNoPermissionException(userToken);
    	if(!bd.hasUser(toDeleteUsername))
    		throw new LoginBubbleDocsException(toDeleteUsername,"");
	}
}
