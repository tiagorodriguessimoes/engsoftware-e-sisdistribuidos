package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;

public class GetSpreadSheetContentService extends BubbleDocsService{

	private String documentId;
	private String username;
	private int maxColumns;
	private int maxRows;
	private SpreadSheet spreadsheet;
	private String[][] content;
	private String token;

	public GetSpreadSheetContentService(String token, String documentId) {
		this.documentId = documentId;
		this.token = token;
		this.username = sm.getUsernameByToken(token);
		this.spreadsheet = bd.getSpreadSheetByID(documentId);
	}
	
	public String[][] getResult (){
		return content;
	}

	@Override
	protected void dispatch() throws BubbleDocsException {
		checkAutorizathion();
		this.maxColumns = this.spreadsheet.getMaxcolumns();
		this.maxRows = this.spreadsheet.getMaxlines();
		//Get Content
		this.content = new String[maxRows][maxColumns];
		for(int row = 0 ; row < this.maxRows ; row++)
			for(int column = 0 ; column < this.maxColumns ; column++){
				if(this.spreadsheet.getCellfromSS(row + 1, column + 1).getHavecontent()){
					this.content[row][column] = this.spreadsheet.getCellfromSS(row + 1, column + 1).getContent().calc();
				}
				else this.content[row][column] = "";
			}
	}

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		if(!sm.UserHasValidSession(this.username))
			throw new UserNotInSessionException(this.username);
		if(!bd.userHasAccessToSpreadSheet(this.username, this.documentId))
			throw new UserNoPermissionException(this.username);
		sm.RenewSession(this.username);
	}
	
	
}
