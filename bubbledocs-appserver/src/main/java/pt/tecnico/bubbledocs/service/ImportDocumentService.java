package pt.tecnico.bubbledocs.service;

import java.io.IOException;
import java.io.StringReader;

import org.jdom2.input.SAXBuilder;
import org.joda.time.LocalDate;
import org.jdom2.Document;
import org.jdom2.JDOMException;

import pt.tecnico.bubbledocs.domain.Access;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.CannotLoadDocumentException;
import pt.tecnico.bubbledocs.exception.CannotStoreDocumentException;
import pt.tecnico.bubbledocs.exception.SessionExpiredException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.service.remote.StoreRemoteServices;

public class ImportDocumentService extends BubbleDocsService{

	StoreRemoteServices StoreRem;

	private String DocumentId;
	private String token;
	private String username;
	private Document XML;
	private byte[] b;
	public ImportDocumentService(String DocumentId, String token) {
		this.DocumentId = DocumentId;
		this.token = token;
		this.username = sm.getUsernameByToken(this.token);
		this.XML= null;
	}

	@Override
	protected void dispatch()  {
		checkAutorizathion();
		User u = bd.getUserByUsername(username);
		
		String document = new String(StoreRem.loadDocument(DocumentId, username));
		SpreadSheet s = new SpreadSheet();
		try{
			SAXBuilder builder = new SAXBuilder();
			XML= new Document();
			XML = builder.build(new StringReader(document));}
		catch(JDOMException | IOException e){
			//throw e;
		}
		if(XML!=null){
			s.importFromXML(XML.getRootElement());}
		else throw new CannotLoadDocumentException(DocumentId);
		if(!s.getFounder().equals(username)){
			throw new UserNoPermissionException(username);
		}

		for(SpreadSheet ss : bd.getSpreadsheetSet()){
			if(s.getId() == ss.getId()){		
				ss.delete();
			}
		}
		bd.insertLoadedSS(username, s);

	}

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		if(this.username == null){
			throw new SessionExpiredException();
		}
		if(!sm.UserHasValidSession(username)){
			throw new CannotLoadDocumentException(DocumentId);
		}else sm.RenewSession(username);
	}

}
