package pt.tecnico.bubbledocs.service;

import pt.tecnico.bubbledocs.domain.Access;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.CannotStoreDocumentException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;

public class ExportDocumentService extends BubbleDocsService {
	
	private org.jdom2.Document docXML;
	private String ssid;
	private String username;
	

	public ExportDocumentService(String userToken,String docId) {
		this.ssid = docId;
		username = sm.getUsernameByToken(userToken);
		docXML = new org.jdom2.Document();
	}

	public org.jdom2.Document getDoc(){
		return this.docXML;
	}
	
	public byte[] getDocInByte(){
		
		String docInString = this.docXML.toString();
		return docInString.getBytes();
	}
	
	@Override
	protected void dispatch() throws BubbleDocsException {
		
		checkAutorizathion();
		boolean found = false;
		User u = bd.getUserByUsername(username);
		for(Access a : u.getAccessSet()){
			if(a.getSpreadsheet().getId().equals(ssid) && a.getSpreadsheet().getFounder().equals(username)){
				found = true;
				docXML.setRootElement(a.getSpreadsheet().exportToXML());	
			}
		}
		if(!found)
			throw new SpreadSheetDoesNotExist(ssid);
	}

	@Override
	protected void checkAutorizathion() throws BubbleDocsException {
		if(!sm.UserHasValidSession(username))
			throw new CannotStoreDocumentException(ssid);
		sm.RenewSession(username);
	}
}
