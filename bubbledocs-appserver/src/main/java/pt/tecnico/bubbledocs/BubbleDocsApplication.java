package pt.tecnico.bubbledocs;

import java.io.IOException;
import java.util.ArrayList;

import javax.transaction.HeuristicMixedException;
import javax.transaction.RollbackException;

import org.jdom2.JDOMException;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import pt.ist.fenixframework.Atomic;
import pt.tecnico.bubbledocs.domain.Access;
import pt.tecnico.bubbledocs.domain.BubbleDocs;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.BadLoginException;
import pt.tecnico.bubbledocs.exception.OperationException;
import pt.tecnico.bubbledocs.exception.SessionExpiredException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UsernameAlreadyExistsException;
import pt.tecnico.bubbledocs.service.*;


public class BubbleDocsApplication {
	public static void main(String[] args) throws SecurityException, IllegalStateException, RollbackException, HeuristicMixedException, JDOMException, IOException {
		System.out.println("Welcome to the BubbleDocs Application!");
		populateDomain(); 					//Meter os dados da primeira entrega
	}
	@Atomic
	private static boolean isInicialized() {
		BubbleDocs bd = BubbleDocs.getInstance();
		return !bd.getUserSet().isEmpty();
	}
	@Atomic
	static void populateDomain() {			 //ALTERAR OS DADOS
        if (isInicialized())
            return;
        createUser("root","root","pass1","noma1", "root");
	}
	@Atomic
	private static void printUsersInfo(BubbleDocs bd){
		System.out.println("De seguida é apresentada toda a informaçao dos utilizadores registados na aplicaçao");
		ArrayList<User> users = new ArrayList<User>(bd.getUserSet());
		for(User u : users){
			System.out.println("Utilizador " + u.getUsername() + " tem o nome " + u.getName() + " e password " + u.getPassword());
		}
	}
	@Atomic
	static public void printUserSpreadSheet(User u){
		System.out.print("Utilizador "+u.getUsername()+" tem as seguintes folhas: ");
		ArrayList<Access> userSpreads = new ArrayList<Access>(u.getAccessSet());
		if(userSpreads.isEmpty()){
			System.out.println("Nao tem nenhuma SpreadSheet");
			return;
		}
		for(Access a : userSpreads){
			System.out.println(a.getSpreadsheet().getName());
		}
	}

	@Atomic
	public static org.jdom2.Document convertToXML(SpreadSheet ss) {
		org.jdom2.Document jdomDoc = new org.jdom2.Document();
		jdomDoc.setRootElement(ss.exportToXML());

		return jdomDoc;
	} 
	@Atomic
	static public org.jdom2.Document printSpreadSheetinXML(User u){		
		ArrayList<SpreadSheet> listss = new ArrayList<SpreadSheet>();
		ArrayList<Access> access = new ArrayList<Access>(u.getAccessSet());
		for(Access acc : access){
			SpreadSheet ss = acc.getSpreadsheet();
			if(ss.getFounder().equals(u.getUsername())){
				listss.add(ss);
			}
		}
		org.jdom2.Document doc = new org.jdom2.Document();
		for(SpreadSheet s: listss){
			doc = convertToXML(s);
			printDomainInXML(doc);
		}
		return doc;
	}
	@Atomic
	static public void removeSpreadsheet(String ssname, User u){		
		u.deleteOwnedSS(ssname);
	}
	@Atomic
	static public void printDomainInXML(org.jdom2.Document doc) {
		XMLOutputter xml = new XMLOutputter();
		xml.setFormat(Format.getPrettyFormat());
		System.out.println(xml.outputString(doc));
	}
	@Atomic
	private static void loginUser(String username, String password){
		try {
			LoginUserService loginuser = new LoginUserService(username,password);
			loginuser.execute();
		} catch (BadLoginException e) {
			System.err.println("Error login a user: " + e.getMessage());
		}

	}
	@Atomic
	private static void createUser(String token, String username,String password, String name, String email) {
		try {
			CreateUserService createUser = new CreateUserService(token,username,email,name);
			createUser.execute();
		} catch (UsernameAlreadyExistsException|OperationException|SessionExpiredException|UserDoesNotExistException nae) {
			System.err.println("Error creating a user: " + nae.getMessage());
		}
	}
	@Atomic
	private static void deleteUser(String userToken, String toDeleteUsername){
		try{
			DeleteUserService deleteUser = new DeleteUserService(userToken,toDeleteUsername);
			deleteUser.execute();
		}catch(UserNoPermissionException e){
			System.err.println("Error deleting a user: " + e.getMessage());
		}
	}
	@Atomic
	private static void createSpreadSheet(String userToken, String name, int rows, int columns){
		try{
			CreateSpreadSheetService css = new CreateSpreadSheetService(userToken,name,rows,columns);
			css.execute();
		} catch(SessionExpiredException e){
			System.err.println("Session Expired or Bad Login:" + e.getUsername());
		}
	}
	@Atomic
	private static void assignLiteralCell(String accessUsername, String docId, String cellId, String literal){
		try{
			AssignLiteralCellService alc = new AssignLiteralCellService(accessUsername, docId, cellId, literal);
			alc.execute();
		} catch(UserNoPermissionException | SpreadSheetDoesNotExist e){ 			
			System.err.println("Error assigning literal to cell: "+ e.getMessage());
		}
	}
	@Atomic
	private static void assignReferenceCell(String tokenUser, String docId, String cellId, String reference) {
		try{
			AssignReferenceCellService arc = new AssignReferenceCellService(tokenUser, docId, cellId, reference);
			arc.execute();
		} catch(UserNoPermissionException | SpreadSheetDoesNotExist e){
			System.err.println("Error assigning referece to cell: "+ e.getMessage());
		}
	}
	@Atomic
	private static void exportDocument(String userToken,String docId){
		try{
			ExportDocumentService ed = new ExportDocumentService(userToken, docId);
			ed.execute();
			ed.getDoc();
		} catch(SpreadSheetDoesNotExist e){ 		
			System.err.println("Error exporting : " + e.getMessage());
		}
	}
}