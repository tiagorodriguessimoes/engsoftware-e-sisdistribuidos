package pt.tecnico.bubbledocs;

import pt.tecnico.bubbledocs.domain.*;

public class SetupDomain {
	
	public static void populateDomain(BubbleDocs bd) {
		System.out.println("Loading Base...");
		SpreadSheet ss = new SpreadSheet("Notas ES", "ESN01",	6, 6, "pf");
		
		for(User u: bd.getUserSet()){	
			if(u.getUsername().equals(ss.getFounder())){
				u.createSpreadsheet(ss);
			}
		}
		for(Cell c : ss.getCellSet()){		
			if(c.getLin() == 3 && c.getCol() == 4){
				c.setContent(new Literal (5));
				c.setHavecontent(true);
			}
			else if(c.getLin() == 1 && c.getCol() == 1){
				c.setContent(new Reference(ss.getCellfromSS(5, 6)));
				c.setHavecontent(true);
				//c.setContent(new Reference(new Cell(5, 6, false)));
			}
			else if(c.getLin() == 5 && c.getCol() == 6){
				c.setContent(new ADD(new ArgLit(2), new ArgRef(ss.getCellfromSS( 3, 4))));
				c.setHavecontent(true);
				//c.setContent(new ADD(new ArgLit(2), new ArgRef(new Cell(3, 4, false))));
				
			}
			else if(c.getLin() == 2 && c.getCol() == 2){
				c.setContent(new DIV(new ArgRef(ss.getCellfromSS( 1, 1)), new ArgRef(ss.getCellfromSS(3, 4))));
				c.setHavecontent(true);
				//c.setContent(new DIV(new ArgRef(new Cell(1,1, false)), new ArgRef(new Cell(3, 4, false))));
			}
		}
		for(Cell c : ss.getCellSet()){
			System.out.println("line= " + c.getLin() + ";" + "column= " + c.getCol());
			if(c.getHavecontent()){
				System.out.println(c.getContent().calc());
			}
			else System.out.println("#VALUE");
		}
	}

}
