package pt.tecnico.bubbledocs.exception;

public class CellDoesNotHaveLiteralException extends BubbleDocsException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public CellDoesNotHaveLiteralException(){
		
	}
	
	@Override
	public String getMessage() {
        return "#VALUE";
	}	

}
