package pt.tecnico.bubbledocs.exception;

public class IncorrectPasswordException extends BubbleDocsException{
	private static final long serialVersionUID = 1L;
	
	private String username;
	
	public IncorrectPasswordException(String username) {
		this.username = username;
	}
	
	public String getUsername(){
		return this.username;
	}
	
	@Override
	public String getMessage() {
        return "Incorrect Password for user " + this.username;
	}	
}