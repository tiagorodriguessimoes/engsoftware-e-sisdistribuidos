package pt.tecnico.bubbledocs.exception;

public class RowsWithoutSizeException extends BubbleDocsException {
	
	private static final long serialVersionUID = 1L;

    public RowsWithoutSizeException() {
        super("Rows Without Size.");
    }

}
