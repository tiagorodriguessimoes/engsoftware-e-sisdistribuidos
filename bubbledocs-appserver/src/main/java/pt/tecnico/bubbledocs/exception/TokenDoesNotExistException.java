package pt.tecnico.bubbledocs.exception;

public class TokenDoesNotExistException extends BubbleDocsException {

	private static final long serialVersionUID = 1L;



	public TokenDoesNotExistException() {
	}


	@Override
	public String getMessage() {
		return "This token does not exist";
	}

}
