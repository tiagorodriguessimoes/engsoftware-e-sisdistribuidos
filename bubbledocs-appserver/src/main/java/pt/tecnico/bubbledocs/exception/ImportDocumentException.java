package pt.tecnico.bubbledocs.exception;

public class ImportDocumentException extends BubbleDocsException {
	private static final long serialVersionUID = 1L;

	public ImportDocumentException() {
	}
	
	@Override
	public String getMessage() {
		return "Error in importing person from XML";
	}
}