package pt.tecnico.bubbledocs.exception;

public class NotSessionNorRootException extends BubbleDocsException{
    private static final long serialVersionUID = 1L;

    public NotSessionNorRootException() {
    }

    
    @Override
    public String getMessage(){
    	return "Not in session nor in root";
    }
}
