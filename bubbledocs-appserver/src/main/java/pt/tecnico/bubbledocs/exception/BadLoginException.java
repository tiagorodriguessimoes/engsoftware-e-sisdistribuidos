package pt.tecnico.bubbledocs.exception;

public class BadLoginException extends BubbleDocsException {
    private static final long serialVersionUID = 1L;

    private String username;

    public BadLoginException(String username) {
        this.username = username;
    }

    @Override
    public String getMessage() {
        return "This username/password " + this.username + " : *********** incorrect";
    }


}
