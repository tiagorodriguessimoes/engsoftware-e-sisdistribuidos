package pt.tecnico.bubbledocs.exception;

public class DuplicateEmailException extends BubbleDocsException{

	private static final long serialVersionUID = 1L;

	private String email;

	public DuplicateEmailException(String email) {
        this.email = email;
    }

	public String getEmail() {
	    return email;
	}
	    
	@Override
	public String getMessage(){
		return "The email" + this.email + "already exists";
	}
}
