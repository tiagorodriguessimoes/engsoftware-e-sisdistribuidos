package pt.tecnico.bubbledocs.exception;

public class UserNotInSessionException extends BubbleDocsException{
	
    private static final long serialVersionUID = 1L;

    private String username;

    public UserNotInSessionException(String user) {
        super(user);
        username = user;
    }

    public String getUsername() {
        return username;
    }

}
