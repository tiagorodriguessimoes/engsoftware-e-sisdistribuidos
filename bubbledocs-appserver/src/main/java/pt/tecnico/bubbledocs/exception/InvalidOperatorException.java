package pt.tecnico.bubbledocs.exception;

public class InvalidOperatorException extends BubbleDocsException{

	private static final long serialVersionUID = 1L;

	public InvalidOperatorException() {
    }
	    
	@Override
	public String getMessage(){
		return "The operator is invalid, try again";
	}

}
