package pt.tecnico.bubbledocs.exception;

public class ColumnsWithoutSizeException extends BubbleDocsException{
	
	private static final long serialVersionUID = 1L;

    public ColumnsWithoutSizeException() {
        super("Columns Without Size.");
    }

}
