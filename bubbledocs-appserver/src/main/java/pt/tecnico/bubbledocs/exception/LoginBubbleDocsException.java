package pt.tecnico.bubbledocs.exception;

public class LoginBubbleDocsException extends BubbleDocsException{

	private static final long serialVersionUID = 1L;
	
	private String password, username;

    public LoginBubbleDocsException(String username, String password) {
    	this.username = username;
    	this.password = password;
	}

    public String getUsername() {
    	return username;
	}
    

    public String getPassword() {
    	return password;
	}
	    
	@Override
	public String getMessage(){
		return "The credentials you have used are incorrect";
	}
}
