package pt.tecnico.bubbledocs.exception;

public class InvalidReferenceException extends BubbleDocsException{
	
	private static final long serialVersionUID = 1L;
	
	public InvalidReferenceException() {
	}

	@Override
	public String getMessage() {
        return "#VALUE";
	}	
}
