package pt.tecnico.bubbledocs.exception;

public class UserNoPermissionException extends BubbleDocsException {
	private static final long serialVersionUID = 1L;
	
	private String userName;
	
	public UserNoPermissionException(String userName) {
		this.userName = userName;
	}
	
	public String getUserName(){
		return this.userName;
	}
	
	@Override
    public String getMessage() {
        return "This user " + this.userName + " doesn't have permissions to complete this action";
    }
}
