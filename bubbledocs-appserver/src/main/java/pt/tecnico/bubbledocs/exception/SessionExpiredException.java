package pt.tecnico.bubbledocs.exception;

public class SessionExpiredException extends BubbleDocsException {
    private static final long serialVersionUID = 1L;

    private String username;

    public SessionExpiredException(String user) {
        super(user);
        username = user;
    }
    public SessionExpiredException() {
    	username="";
    }

    public String getUsername() {
        return username;
    }
    
    @Override
    public String getMessage(){
    	return "Required login. Your session has expired. "+getUsername()+"Login again.";
    }

}
