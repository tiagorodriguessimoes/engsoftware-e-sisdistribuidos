package pt.tecnico.bubbledocs.exception;

public class SpreadSheetDoesNotExist extends BubbleDocsException {
    private static final long serialVersionUID = 1L;

    private String docID;

    public SpreadSheetDoesNotExist(String docID) {
        super(docID);
        this.docID = docID;
    }

	public String getDocID() {
        return this.docID;
    }
	@Override
	public String getMessage(){
		return "Spreadsheet: "+getDocID()+" does not exists!";
	}
}
