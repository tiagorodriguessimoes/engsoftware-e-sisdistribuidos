package pt.tecnico.bubbledocs.exception;

public class UserDoesNotExistException extends BubbleDocsException{

	private static final long serialVersionUID = 1L;
		
	private String userName;
	
	public UserDoesNotExistException(String userName) {
		this.userName = userName;
	}
	
	public UserDoesNotExistException() {
		this.userName = "MissingValue";
	}
	public String getUserName(){
		return this.userName;
	}
	
	@Override
    public String getMessage() {
        return "This user " + this.userName + " does not exist";
    }

	
}
