package pt.tecnico.bubbledocs.exception;

public class OperationException extends BubbleDocsException{
	
	private static final long serialVersionUID = 1L;
	
	public OperationException() {
	}
	
	@Override
	public String getMessage() {
        return "Invalid Operation";
	}
}
