package pt.tecnico.bubbledocs.exception;

public class RootNotInSessionException extends BubbleDocsException{
	private static final long serialVersionUID = 1L;

    public RootNotInSessionException() {
    }

    
    @Override
    public String getMessage(){
    	return "Root not in session";
    }

}
