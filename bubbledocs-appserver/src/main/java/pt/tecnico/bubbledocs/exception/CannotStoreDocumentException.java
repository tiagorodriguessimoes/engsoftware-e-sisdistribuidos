package pt.tecnico.bubbledocs.exception;

public class CannotStoreDocumentException extends BubbleDocsException{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String docName;
	
	public CannotStoreDocumentException(String docName){
		this.docName=docName;
	}
	
	public String getDocName(){
		return this.docName;
	}
	
	@Override
	public String getMessage(){
		return "This document " + this.docName + " cannot be stored. Capacity exceeded.";
	}
}
