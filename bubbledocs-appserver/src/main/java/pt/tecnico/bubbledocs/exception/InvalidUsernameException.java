package pt.tecnico.bubbledocs.exception;

public class InvalidUsernameException extends BubbleDocsException{

	private static final long serialVersionUID = 1L;
	
	private String username;

    public InvalidUsernameException(String user) {
    	this.username = user;
	}

    public String getUsername() {
    	return username;
	}
	    
	@Override
	public String getMessage(){
		return "The username" + this.username + "is invalid";
	}
}
