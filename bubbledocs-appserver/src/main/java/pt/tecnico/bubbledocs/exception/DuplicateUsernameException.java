package pt.tecnico.bubbledocs.exception;

public class DuplicateUsernameException extends BubbleDocsException {

    private static final long serialVersionUID = 1L;

    private String username;

    public DuplicateUsernameException(String user) {
        this.username = user;
    }

    public String getUsername() {
        return username;
    }
    
	@Override
	public String getMessage(){
		return "The username" + this.username + "already exists";
	}

}