package pt.tecnico.bubbledocs.exception;

public class InvalidEmailException extends BubbleDocsException{

	private static final long serialVersionUID = 1L;
	
	private String email;

	public InvalidEmailException(String email) {
        this.email = email;
    }

	public String getEmail() {
	    return email;
	}
	    
	@Override
	public String getMessage(){
		return "The email" + this.email + "is invalid";
	}

}
