package pt.tecnico.bubbledocs.exception;

public class EmptyIDException extends BubbleDocsException{

    private static final long serialVersionUID = 1L;

    public EmptyIDException() {
        super("Empty ID.");
    }
}
