package pt.tecnico.bubbledocs.exception;

public class RemoteInvocationException extends BubbleDocsException{
	
	
	private static final long serialVersionUID = 1L;

	public RemoteInvocationException(){
		
	}
	@Override
	public String getMessage(){
		return "Impossible to call remote service SD-Store.";
	}
}
