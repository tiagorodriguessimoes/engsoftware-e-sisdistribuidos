package pt.tecnico.bubbledocs.domain;

import org.jdom2.Element;

import pt.tecnico.bubbledocs.exception.InvalidReferenceException;

public abstract class Content extends Content_Base {
    
    public Content() {
        super();
    }
    
    public Element exportToXML() {
        Element element = new Element("Content");
        //element.addContent(this.getValue().toString());
        return element;
    }
    
    public void importFromXML(Element contentElement) {
    	setValue(contentElement.getAttribute("value").getValue());
        }
    public abstract String calc()throws InvalidReferenceException;
}
