package pt.tecnico.bubbledocs.domain;

import org.jdom2.Element;
import org.jdom2.DataConversionException;

import pt.tecnico.bubbledocs.exception.ImportDocumentException;



public class ArgLit extends ArgLit_Base {

	public ArgLit() {
		super();
	}

	public ArgLit(int i) {
		super();
		this.setArgvalue(i);
	}

	public Element exportToXML() {
		Element element = new Element("Literal");

		element.setAttribute("val", this.getArgvalue().toString());

		return element;
	}

	public void importFromXML(Element arglitElement) {
		try {
			setArgvalue(arglitElement.getAttribute("arglit").getIntValue());
		} catch (DataConversionException e) {
			throw new ImportDocumentException();
		}
	}

	public String calc() {
		String out = new String(String.valueOf(this.getArgvalue()));
		return out;
	}

}
