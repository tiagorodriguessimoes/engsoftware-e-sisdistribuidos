package pt.tecnico.bubbledocs.domain;

import java.io.IOException;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.joda.time.LocalDate;

import pt.ist.fenixframework.FenixFramework;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;


public class BubbleDocs extends BubbleDocs_Base {

	public BubbleDocs() {
		FenixFramework.getDomainRoot().setBubbledocs(this);
	}

	public static BubbleDocs getInstance() {
		BubbleDocs bd = FenixFramework.getDomainRoot().getBubbledocs();
		if (bd == null) {
			bd = new BubbleDocs();
			bd.setSessionmanager(new SessionManager());
		}
		return bd;
	}

	public boolean hasUser(String username) {
		for (User u : getUserSet()) {
			if (u.getUsername().equals(username)) {
				return true;
			}
		}
		return false;
	}

	public boolean emailExists(String email){
		for(User user : getUserSet())
				if(email.equals(user.getEmail()))
					return true;
		return false;
	}
	
	public void createRoot() {
		User root = new User("root", "root", "root");
		this.addUser(root);
	}

	public void creatSS(String name, String id, int maxlines, int maxcolumns,
			String founder) {
		SpreadSheet ss = new SpreadSheet(name, id, maxlines, maxcolumns,
				founder);
		this.addSpreadsheet(ss);
	}

	public User getUserByUsername(String username) {
		for (User u : getUserSet()) {
			if (u.getUsername().equals(username)) {
				return u;
			}
		}
		return null;
	}
	public void insertLoadedSS(String founder,SpreadSheet s){
		User u = this.getUserByUsername(founder);
		u.createSpreadsheet(s);		
	}
	
	
	
	
	public void ImportfromXML(org.jdom2.Document doc) throws JDOMException,
			IOException {
		// CHANGE PATH :xmlSource

		String delims = "-";
		String xmlSource = "ficheiroxml.xml";
		SAXBuilder jdomBuilder = new SAXBuilder();
		// jdomDocument is the JDOM2 Object
		Document jdomDocument = jdomBuilder.build(xmlSource);
		String ssname = jdomDocument.getRootElement().getAttributeValue("name"); // spreedsheet
																					// name
		String ssid = jdomDocument.getRootElement().getAttributeValue("id");
		String ssdate = jdomDocument.getRootElement().getAttributeValue("data");
		String[] tokens = ssdate.split(delims);
		int y = Integer.valueOf(tokens[0]);
		int m = Integer.valueOf(tokens[1]);
		int d = Integer.valueOf(tokens[2]);
		int ssmaxl = Integer.valueOf(jdomDocument.getRootElement()
				.getAttributeValue("maxlines"));
		int ssmaxc = Integer.valueOf(jdomDocument.getRootElement()
				.getAttributeValue("maxcolumns"));
		String ssfounder = jdomDocument.getRootElement().getAttributeValue(
				"date");
		LocalDate data = new LocalDate(y, m, d);
		for (User u : this.getUserSet()) {
			if (u.getUsername().equals(ssfounder)) {
				for (Access a : u.getAccessSet()) {
					if (!a.getSpreadsheet().getId().equals(ssid)) {
						SpreadSheet s = new SpreadSheet(ssname, ssid, data,
								ssmaxl, ssmaxc, ssfounder);
						Element cells = jdomDocument.getRootElement().getChild(
								"Cells");
						for (Element cellElement : cells.getChildren()) {
							Cell c = s.getCellfromSS(Integer
									.parseInt(cellElement
											.getAttributeValue("line")),
									Integer.parseInt(cellElement
											.getAttributeValue("column")));
							c.importFromXML(cellElement);
						}
					}
				}
			}
		}
	}

	public void printAllUsers() {
		for (User a : this.getUserSet()) 
			System.out.println("Username:" + a.getUsername() + " Name:"+ a.getName());
	}

	public Element exportToXML() {
		Element element = new Element("BD");
		element.setAttribute("Users", this.getUserSet().toString());
		element.setAttribute("Spreadsheet", this.getSpreadsheetSet().toString());
		return element;
	}

	public SpreadSheet getSpreadSheetByID(String idDoc) throws SpreadSheetDoesNotExist {
		for (SpreadSheet ss : getSpreadsheetSet()) 
			if (ss.getId().equals(idDoc)) 
				return ss;
		throw new SpreadSheetDoesNotExist(idDoc);
	}

	public boolean userHasAccessToSpreadSheet(String username, String documentId){
		User user = getUserByUsername(username);
		SpreadSheet spreadSheet = getSpreadSheetByID(documentId);
		return user.getAccessBySpreadsheet(spreadSheet);
	}
}
