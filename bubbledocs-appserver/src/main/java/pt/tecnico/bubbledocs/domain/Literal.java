package pt.tecnico.bubbledocs.domain;

import org.jdom2.DataConversionException;
import org.jdom2.Element;

import pt.tecnico.bubbledocs.exception.ImportDocumentException;

public class Literal extends Literal_Base {

	public Literal() {
		
		
		super();
	}

	public Literal(int val) {
		super();
		this.setIntvalue(val);
	}
	
	public Element exportToXML() {
        Element element = new Element("Literal");

        element.setAttribute("val", this.getIntvalue().toString());
       	
        return element;
    }

	public void importFromXML(Element literalElement) {
		
		try {
		    setIntvalue(literalElement.getAttribute("value").getIntValue());
		} catch (DataConversionException e) { 
		    throw new ImportDocumentException();
		}
	    }
	public String calc(){
		String out=new String(String.valueOf(this.getIntvalue()));
		return out;
	}
}
