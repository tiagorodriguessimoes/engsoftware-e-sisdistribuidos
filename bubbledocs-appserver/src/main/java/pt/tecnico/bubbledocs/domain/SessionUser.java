package pt.tecnico.bubbledocs.domain;
import org.joda.time.LocalTime;

public class SessionUser extends SessionUser_Base {
    
    public SessionUser() {
        super();
    }
    
    
    public SessionUser(String username, LocalTime time, String usertoken) {
        super();
        this.setUsername(username);
        this.setTime(time);
        this.setToken(usertoken);
    }
    
    public void deleteUser(){
    	setSessionmanager(null);
    	this.deleteDomainObject();
    }
    
}
