package pt.tecnico.bubbledocs.domain;

import org.joda.time.LocalTime;
import org.joda.time.Minutes;

import pt.tecnico.bubbledocs.exception.SessionExpiredException;

import java.util.Random;

public class SessionManager extends SessionManager_Base {

	public SessionManager() {
		super();
	}

	public String GenerateToken (String username){
		Random rand = new Random();
		int number = rand.nextInt(9) + 0;
		String result;
		result = username + number;
		return result;
	}

	public String CreateSession(String username){
		LocalTime time = new LocalTime();
		if(username.equals("root")){
			SessionUser su = new SessionUser (username, time, "root");
			this.addSessionuser(su);
			return "root";
		}

		String t = GenerateToken (username);
		SessionUser su = new SessionUser (username, time, t);
		this.addSessionuser(su);
		RenewSession(username);
		return t;

	}

	public boolean UserHasValidSession(String username){
		boolean bool = false;
		for(SessionUser su : this.getSessionuserSet()){
			if(su.getUsername().equals(username)){
				LocalTime currTime = new LocalTime();
				int minutes = Minutes.minutesBetween(currTime, su.getTime()).getMinutes();
				if(minutes <= 120){
					bool = true;
					break;
				}
			}
		}
		return bool;
	}

	public String getUsernameByToken(String token){
		for(SessionUser su : this.getSessionuserSet()){
			if(su.getToken().equals(token)){
				return su.getUsername();
			}
		}
		return null;
	}

	public String getTokenByUsername(String username){

		for(SessionUser su : this.getSessionuserSet()){
			if(su.getUsername().equals(username)){
				return su.getToken();
			}
		}
		return null;
	}

	public void DeleteSession(String username){
		for(SessionUser su : this.getSessionuserSet()){
			if(su.getUsername().equals(username)){
				su.deleteUser();
				break;
			}
		}
	}

	public void RenewSession(String username){
		for(SessionUser su : this.getSessionuserSet()){
			if(su.getUsername().equals(username)){
				su.getTime().plusMinutes(120);
			}
		}
	}

	public void CleanSessions(){
		for(SessionUser su : this.getSessionuserSet()){
			if(!(UserHasValidSession(su.getUsername())))
				su.deleteUser();
		}
	}
}

