package pt.tecnico.bubbledocs.domain;

import org.jdom2.DataConversionException;
import org.jdom2.Element;

import pt.tecnico.bubbledocs.exception.ImportDocumentException;

public class Access extends Access_Base {

	public Access() {
		super();
	}

	public Access(boolean permission, boolean owner){
		super();
		this.setPermission(permission);
		this.setOwner(owner);
	}

	public Access(boolean permission) {
		super();
		this.setPermission(permission);
	}

	public Element exportToXML() {
		Element element = new Element("Access");
		element.setAttribute("permission", Boolean.toString(this.getPermission()));
		element.setAttribute("owner", Boolean.toString(this.getOwner()));
		return element;
	}

	public void importFromXML(Element AccessElement) throws ImportDocumentException {
		try {
			setPermission(AccessElement.getAttribute("permission").getBooleanValue());
			setOwner(AccessElement.getAttribute("owner").getBooleanValue());
		} catch (DataConversionException e) { 
			throw new ImportDocumentException();
		}
	}
}
