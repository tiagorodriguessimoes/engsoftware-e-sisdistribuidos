package pt.tecnico.bubbledocs.domain;

import org.jdom2.Element;

import pt.tecnico.bubbledocs.exception.InvalidEmailException;
import pt.tecnico.bubbledocs.exception.InvalidUsernameException;

public class User extends User_Base {

	public User() {
		super();
	}

	public User(String username, String name, String email) {
		super();
		if(username.length() < 3 || username.length() > 8)
			throw new InvalidUsernameException(username);
		if(!email.contains("@"))
			throw new InvalidEmailException(email);
		this.setUsername(username);
		this.setName(name);
		this.setEmail(email);
	}

	public void delete(){
		for(Access a :this.getAccessSet()){
			if(a.getSpreadsheet().getFounder().equals(this.getUsername())){
				a.getSpreadsheet().delete();
			}
			this.removeAccess(a);
		}
		BubbleDocs bd = BubbleDocs.getInstance();
		SessionManager sm = bd.getSessionmanager();
		sm.DeleteSession(this.getUsername());
		setBubbledocs(null);
		this.deleteDomainObject();
	}

	public void createSpreadsheet(SpreadSheet ss) {		
		if(this.getUsername().equals(ss.getFounder())){
			Access ac = new Access();
			ac.setSpreadsheet(ss);
			ac.setUser(this);
			ac.setOwner(true);
			ac.setPermission(true);
			ss.addAccess(ac);
			this.addAccess(ac);
		}
	}

	public void deleteOwnedSS(String ss){
		for(Access a : this.getAccessSet()){
			if(a.getSpreadsheet().getName().equals(ss)){
				if(a.getOwner()){
					a.getSpreadsheet().delete();
					this.removeAccess(a);

				}		
			}
		}
	}

	public Element exportToXML() {
		Element element = new Element("User");
		element.setAttribute("name", this.getName());
		element.setAttribute("username", this.getUsername());
		element.setAttribute("password", this.getPassword());
		return element;
	}

	public void importFromXML(Element userElement) {
		setName(userElement.getAttribute("name").getValue());
		setName(userElement.getAttribute("username").getValue());
		setName(userElement.getAttribute("password").getValue());
	}

	public boolean getAccessBySpreadsheet(SpreadSheet s){	
		boolean permission = false;
		for(Access a : getAccessSet()){
			a.getSpreadsheet().equals(s);
			permission = a.getPermission();
		}
		return permission;
	}
}
