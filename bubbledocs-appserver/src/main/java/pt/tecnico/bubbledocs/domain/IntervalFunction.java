package pt.tecnico.bubbledocs.domain;

import org.jdom2.Element;

public abstract class IntervalFunction extends IntervalFunction_Base {
    
    public IntervalFunction() {
        super();
    }
    
    public IntervalFunction(ArgInterval arginterval) {
    	super();
    	this.setArginterval(arginterval);
    }
    
    public Element exportToXML() {
        Element element = new Element("intervalFunc");
        
        element.addContent(this.getArginterval().exportToXML());
 
        
        return element;
    }
    
}
