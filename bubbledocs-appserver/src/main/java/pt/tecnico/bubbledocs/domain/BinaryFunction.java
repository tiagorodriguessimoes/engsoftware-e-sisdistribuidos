package pt.tecnico.bubbledocs.domain;



import org.jdom2.Element;


public abstract class BinaryFunction extends BinaryFunction_Base {
	
	public BinaryFunction(){
		super();
	}
   
    public BinaryFunction(Argument arg1,Argument arg2) {
    	super();
    	this.setArg1(arg1);
    	this.setArg2(arg2);
    }
    
    public Element exportToXML() {
        Element element = new Element("binaryFunc");
        
        element.addContent(this.getArg1().exportToXML());
        element.addContent(this.getArg2().exportToXML());
        
        return element;
    }
    
}
