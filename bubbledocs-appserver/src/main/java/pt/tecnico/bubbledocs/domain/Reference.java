package pt.tecnico.bubbledocs.domain;

import org.jdom2.Element;

import pt.tecnico.bubbledocs.exception.InvalidReferenceException;

public class Reference extends Reference_Base {

	public Reference() {
		super();
	}

	public Reference(Cell c) {
		super();
		this.setCellm(c);
	}
	
	public Element exportToXML() {
        Element element = new Element("Reference");

        element.setAttribute("refl", this.getCellm().getLin().toString());
        element.setAttribute("refc", this.getCellm().getCol().toString());
        return element;
    }

	public String calc() throws InvalidReferenceException{
		if(this.getCellm().getContent()==null){
			throw new InvalidReferenceException();
		}
		else return this.getCellm().getContent().calc();
	}
}
