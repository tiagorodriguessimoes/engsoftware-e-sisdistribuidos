package pt.tecnico.bubbledocs.domain;

import java.util.Random;

import org.jdom2.DataConversionException;
import org.jdom2.Element;
import org.joda.time.LocalDate;

import pt.tecnico.bubbledocs.exception.EmptyIDException;
import pt.tecnico.bubbledocs.exception.ImportDocumentException;

public class SpreadSheet extends SpreadSheet_Base {

	public SpreadSheet() {
		super();
	}

	public SpreadSheet(String name, String id, LocalDate l, int maxlines,
			int maxcolumns, String founder) {

		super();
		if (name.length() < 1)
			throw new EmptyIDException();
		this.setName(name);
		this.setId(id);
		this.setCreatedate(l);
		this.setMaxlines(maxlines);
		this.setMaxcolumns(maxcolumns);
		this.setFounder(founder);
		for (int i = 1; i <= maxlines; i++) {
			for (int j = 1; j <= maxcolumns; j++) {
				Cell cell = new Cell(i, j, false);
				this.addCell(cell);
			}
		}
	}

	public SpreadSheet(String name, String id, int maxlines, int maxcolumns,
			String founder) {

		super();
		LocalDate date = new LocalDate();
		this.setName(name);
		this.setId(id);
		this.setCreatedate(date);
		this.setMaxlines(maxlines);
		this.setMaxcolumns(maxcolumns);
		this.setFounder(founder);
		for (int i = 1; i <= maxlines; i++) {
			for (int j = 1; j <= maxcolumns; j++) {
				Cell cell = new Cell(i, j, false);
				this.addCell(cell);
			}
		}
	}

	public SpreadSheet(String name, int maxlines, int maxcolumns, String founder) {

		super();
		LocalDate date = new LocalDate();
		this.setName(name);
		this.setCreatedate(date);
		this.setMaxlines(maxlines);
		this.setMaxcolumns(maxcolumns);
		this.setFounder(founder);
		for (int i = 1; i <= maxlines; i++) {
			for (int j = 1; j <= maxcolumns; j++) {
				Cell cell = new Cell(i, j, false);
				this.addCell(cell);
			}
		}
		String id = this.GenerateID(name);
		this.setId(id);

	}

	public String GenerateID(String name) {
		Random rand = new Random();
		int number = rand.nextInt(9) + 0;
		String result;
		result = name + number;
		return result;
	}

	public Element exportToXML() {
		Element element = new Element("SpreadSheet");
		String dateString = null;

		element.setAttribute("name", this.getName());
		element.setAttribute("id", this.getId());
		LocalDate localDate = this.getCreatedate();
		if (localDate != null) {
			dateString = String.format("%d-%02d-%02d", localDate.getYear(),
					localDate.getMonthOfYear(), localDate.getDayOfMonth());
		}
		element.setAttribute("data", dateString);
		element.setAttribute("maxlines", this.getMaxlines().toString());
		element.setAttribute("maxcolumns", this.getMaxcolumns().toString());
		element.setAttribute("founder", this.getFounder());
		Element cellsElement = new Element("cells");
		element.addContent(cellsElement);

		for (Cell c : this.getCellSet()) {
			cellsElement.addContent(c.exportToXML());
		}
		return element;
	}

	public void importFromXML(Element spreadsheetElement) {
		setName(spreadsheetElement.getAttribute("name").getValue());
		setId(spreadsheetElement.getAttribute("id").getValue());
		String localDateAsString = new String();
		localDateAsString = spreadsheetElement.getAttribute("data")
				.getValue();
		LocalDate ld;
		int year = Integer.parseInt(localDateAsString.substring(0, 4));
		int month = Integer.parseInt(localDateAsString.substring(5, 7));
		int day = Integer.parseInt(localDateAsString.substring(8, 10));
		ld = new LocalDate(year, month, day);
		setCreatedate(ld);
		setFounder(spreadsheetElement.getAttribute("founder").getValue());
		try {
			setMaxlines(spreadsheetElement.getAttribute("maxlines")
					.getIntValue());
		} catch (DataConversionException e) {
			throw new ImportDocumentException();
		}
		try {
			setMaxcolumns(spreadsheetElement.getAttribute("maxcolumns")
					.getIntValue());
		} catch (DataConversionException e) {
			throw new ImportDocumentException();
		}

		Element cells = spreadsheetElement.getChild("cells");
		for (int i = 1; i <= getMaxlines(); i++) {
			for (int j = 1; j <= getMaxcolumns(); j++) {
				Cell cell = new Cell(i, j, false);
				this.addCell(cell);
			}
		}
		for (Element e : cells.getChildren("Cell")) {
			try {
				Cell c = this.getCellfromSS(e.getAttribute("lin").getIntValue(),e.getAttribute("col").getIntValue());
				c.importFromXML(e);
			} catch (DataConversionException e1) {
				// TODO Auto-generated catch block
				//e1.printStackTrace();
			}
		}
	}

	public void delete() {

		for (Cell c : this.getCellSet()) {
			this.removeCell(c);
			c.delete();

		}
		for (Access a : this.getAccessSet()) {
			this.removeAccess(a);
		}
		setBubbledocs(null);
		this.deleteDomainObject();

	}

	public String descript() {
		String out = new String(this.getName() + "|" + this.getId() + "|"
				+ this.getCreatedate() + "|" + this.getMaxlines() + "|"
				+ this.getMaxcolumns() + "|" + this.getFounder());
		return out;
	}

	public Cell getCellfromSS(int line, int column) {
		for (Cell c : this.getCellSet()) {
			if (c.getLin() == line && c.getCol() == column) {
				return c;
			}
		}
		return null;
	}

}
