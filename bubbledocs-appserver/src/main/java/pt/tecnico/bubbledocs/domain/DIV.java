package pt.tecnico.bubbledocs.domain;


import org.jdom2.Element;

import pt.tecnico.bubbledocs.exception.InvalidReferenceException;



public class DIV extends DIV_Base {
    
	public DIV(){

		super();
	}
    
    public DIV(Argument arg1,Argument arg2) {
    	super();
        this.setArg1(arg1);
        this.setArg2(arg2);
    }
   
    public Element exportToXML() {
        Element element = new Element("DIV");
  
        element.addContent(this.getArg1().exportToXML());
        element.addContent(this.getArg2().exportToXML());
 
        return element;
    }
    public String calc() {
		String out;
		Argument arg1 = this.getArg1();
		Argument arg2 = this.getArg2();
		try {

			int value1 = Integer.parseInt(arg1.calc());
			int value2 = Integer.parseInt(arg2.calc());
			int result = value1 % value2;
			out = String.valueOf(result);
			return out;

		} catch (InvalidReferenceException e) {
			out=e.getMessage();
		}
		return out;
	}
}

