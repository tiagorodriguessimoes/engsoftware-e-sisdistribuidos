package pt.tecnico.bubbledocs.domain;

import javax.xml.parsers.DocumentBuilderFactory;

import org.jdom2.DataConversionException;
import org.jdom2.Document;
import org.jdom2.Element;

import pt.tecnico.bubbledocs.exception.ImportDocumentException;
public class Cell extends Cell_Base {


	public Cell() {
		super();
	}

	public Cell(int lin, int col, boolean protection) {
		this();
		this.setLin(lin);
		this.setCol(col);
		this.setProtection(protection);
		this.setHavecontent(false);
		this.setContent(null);
	}

	public Cell(int lin, int col, boolean protection,SpreadSheet s) {
		this();
		this.setLin(lin);
		this.setCol(col);
		this.setProtection(protection);
		this.setHavecontent(false);
		this.setContent(null);
		this.setSpreadsheet(s);
	}

	public Cell(int lin, int col,boolean protection, Content content){
		this();
		this.setLin(lin);
		this.setCol(col);
		this.setProtection(protection);
		this.setHavecontent(true);
		this.setContent(content);
	}

	public void delete(){
		this.deleteDomainObject();
	}

	public String address(){

		String res;
		res = getLin() + ";" + getCol();
		return res;
	}

	public Element exportToXML() {
		Element element = new Element("Cell");
		element.setAttribute("lin", Integer.toString(this.getLin()));
		element.setAttribute("col", Integer.toString(this.getCol()));
		element.setAttribute("protection", Boolean.toString(this.getProtection()));
		//element.setAttribute("s", this.getSpreadsheet().getName());
		if(this.getContent() != null){
			element.addContent(this.getContent().exportToXML());
		}
		//element.setAttribute("content", this.getContent().toString());
/*
		Element cont = new Element("content");
		element.addContent(cont);*/
		return element;
	}

	public void importFromXML(Element cellElement) {
		try {
			setLin(cellElement.getAttribute("lin").getIntValue());
			setCol(cellElement.getAttribute("col").getIntValue());
		//	System.out.println(this.getLin()+";"+this.getCol());
			setProtection(cellElement.getAttribute("protection").getBooleanValue());
			for(Element s : cellElement.getChildren()){
				if(s.toString().contains("Literal")){
					Literal lit = new Literal(s.getAttribute("val").getIntValue());
					this.setContent(lit);
					this.setHavecontent(true);
				}
				else if(s.toString().contains("Reference")){
					int linaux= s.getAttribute("refl").getIntValue();
					int colaux= s.getAttribute("refc").getIntValue();
					Cell cellaux= this.getSpreadsheet().getCellfromSS(linaux, colaux);
					Reference ref= new Reference(cellaux);
					this.setContent(ref);
					this.setHavecontent(true);
				}
				else if(s.toString().contains("ADD")){
					/*Argument arg1;
					Argument arg2;
					int count=1;
					for(Element e:s.getChildren()){
						if(count==1){
							int linaux= e.getAttribute("refl").getIntValue();
							int colaux= e.getAttribute("refc").getIntValue();
							Cell cellaux= this.getSpreadsheet().getCellfromSS(linaux, colaux);
							arg1= new ArgRef(cellaux);
						}
						else{
							int linaux= e.getAttribute("refl").getIntValue();
							int colaux= e.getAttribute("refc").getIntValue();
							Cell cellaux= this.getSpreadsheet().getCellfromSS(linaux, colaux);
							arg2= new ArgRef(cellaux);
						}
						this.setContent(new ADD(arg1,arg2));
						this.setHavecontent(true)
					}*/
				}
				else if(s.toString().contains("SUB")){}
				else if(s.toString().contains("DIV")){}
				else if(s.toString().contains("MUL")){}
				else if(s.toString().contains("AVG")){}
			}
		//	System.out.println("content: "+this.getContent().toString());
			
		} catch (DataConversionException e) { 
			throw new ImportDocumentException();
		}
		
	}
	
}