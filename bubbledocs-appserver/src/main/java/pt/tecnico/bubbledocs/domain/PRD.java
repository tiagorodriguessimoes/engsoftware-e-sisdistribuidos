package pt.tecnico.bubbledocs.domain;

import org.jdom2.Element;

public class PRD extends PRD_Base {
    
    public PRD() {
        super();
    }
    
    public PRD(ArgInterval arginterval) {
		super();
    	this.setArginterval(arginterval);
    }
	
	public Element exportToXML() {
        Element element = new Element("PRD");
  
        element.addContent(this.getArginterval().exportToXML());
 
        return element;
    }
	
	
	
	public String calc() {
		String out = "";

		ArgInterval arg = this.getArginterval();
		Cell c1 = arg.getCell1();
		Cell c2 = arg.getCell2();

		if (c1.getSpreadsheet().getId().equals(c2.getSpreadsheet().getId())) {
			if (arg.getColi() && arg.getLinei()) {
				return c1.getContent().calc();
			}
			SpreadSheet s = c1.getSpreadsheet();
			float val = 0;
			
			for (Cell c : s.getCellSet()) {
				if (arg.getColi()) {
					if (c.getCol() == c1.getCol() && c.getLin() >= c1.getLin()
							&& c.getLin() <= c2.getLin()) {
						val *= Integer.parseInt(c.getContent().calc());
						
					}
				}
				if (arg.getLinei()) {
					if (c.getLin() == c1.getLin() && c.getCol() >= c1.getCol()
							&& c.getCol() <= c2.getCol()) {
						val *= Integer.parseInt(c.getContent().calc());
						
					}
				}
			}
			
			out=String.valueOf(val);
		}
		
		return out;
	}
}
