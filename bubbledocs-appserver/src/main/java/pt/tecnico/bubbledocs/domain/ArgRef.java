package pt.tecnico.bubbledocs.domain;

import org.jdom2.Element;

import pt.tecnico.bubbledocs.exception.InvalidReferenceException;

public class ArgRef extends ArgRef_Base {
    
    public ArgRef() {
        super();
    }
    public ArgRef(Cell c){
    	super();
    	this.setCellm(c);
    }
    
    public Element exportToXML() {
        Element element = new Element("Reference");

        element.setAttribute("refl", this.getCellm().getLin().toString());
        element.setAttribute("refc", this.getCellm().getCol().toString());
       	
        return element;
    }
    
    public String calc() throws InvalidReferenceException{
		if(!this.getCellm().getHavecontent()){
			throw new InvalidReferenceException();
		}
		else return this.getCellm().getContent().calc();
	}
}
