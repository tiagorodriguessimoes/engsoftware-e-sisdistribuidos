package pt.tecnico.bubbledocs.domain;

import org.jdom2.Element;

public class AVG extends AVG_Base {

	public AVG() {
		super();
	}

	public AVG(ArgInterval arginterval) {
		super();
		this.setArginterval(arginterval);
	}

	public Element exportToXML() {
		Element element = new Element("AVG");

		element.addContent(this.getArginterval().exportToXML());

		return element;
	}

	public String calc() {
		String out = "";

		ArgInterval arg = this.getArginterval();
		Cell c1 = arg.getCell1();
		Cell c2 = arg.getCell2();

		if (c1.getSpreadsheet().getId().equals(c2.getSpreadsheet().getId())) {
			if (arg.getColi() && arg.getLinei()) {
				return c1.getContent().calc();
			}
			SpreadSheet s = c1.getSpreadsheet();
			float val = 0;
			int count = 0;
			for (Cell c : s.getCellSet()) {
				if (arg.getColi()) {
					if (c.getCol() == c1.getCol() && c.getLin() >= c1.getLin()
							&& c.getLin() <= c2.getLin()) {
						val += Integer.parseInt(c.getContent().calc());
						++count;
					}
				}
				if (arg.getLinei()) {
					if (c.getLin() == c1.getLin() && c.getCol() >= c1.getCol()
							&& c.getCol() <= c2.getCol()) {
						val += Integer.parseInt(c.getContent().calc());
						++count;
					}
				}
			}
			val=val/count;
			out=String.valueOf(val);
		}
		
		return out;
	}

}
