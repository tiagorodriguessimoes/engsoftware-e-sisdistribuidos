package pt.tecnico.bubbledocs.domain;

import org.jdom2.Element;

import pt.tecnico.bubbledocs.exception.InvalidReferenceException;



public abstract class Argument extends Argument_Base {
    
    public Argument() {
        super();
    }
    public Element exportToXML() {
        Element element = new Element("Reference");
        
        return element;
    }
    
    public abstract String calc() throws InvalidReferenceException;
}
