package pt.tecnico.bubbledocs.domain;

import org.jdom2.Element;




public class ArgInterval extends ArgInterval_Base {
    
    public ArgInterval() {
        super();
    }
    
    public ArgInterval(Cell c1, Cell c2){
    	super();
    	this.setCell1(c1);
    	this.setCell2(c2);
    	if(c1.getLin()==c2.getLin()){
    		this.setLinei(true);
    	}
    	if(c1.getCol()==c2.getCol()){
    		this.setColi(true);
    	}
    }
    

    public Element exportToXML() {
        Element element = new Element("Interval");

        element.setAttribute("cell1", this.getCell1().toString());
        element.setAttribute("cell2", this.getCell2().toString());
       	
        return element;
    }
    
}
