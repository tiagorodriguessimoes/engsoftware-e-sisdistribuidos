package pt.tecnico.bubbledocs.integration;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.service.remote.IDRemoteServices;
import pt.tecnico.bubbledocs.service.remote.StoreRemoteServices;

public abstract class BubbleDocsIntegrator  {
	
	protected IDRemoteServices IDRem;
	protected StoreRemoteServices StoreRem;
	
	public final void execute() throws BubbleDocsException{
		IDRem = new IDRemoteServices();
		StoreRem = new StoreRemoteServices();
		
		dispatch();
	}

	protected abstract void dispatch() throws BubbleDocsException;
	
}
