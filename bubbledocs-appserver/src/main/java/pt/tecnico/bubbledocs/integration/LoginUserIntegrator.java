package pt.tecnico.bubbledocs.integration;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.tecnico.bubbledocs.exception.UnavailableServiceException;
import pt.tecnico.bubbledocs.service.LoginUserService;

public class LoginUserIntegrator extends BubbleDocsIntegrator{

	private String userToken;
	private String username;
	private String password;

	public LoginUserIntegrator(String username, String password) {
		this.username = username;
		this.password = password;
	}

	public final String getUserToken() {
		return this.userToken;
	}
	
	@Override
	protected void dispatch() throws BubbleDocsException {
		try {
			LoginUserService loginUserService = new LoginUserService(this.username, this.password);
			loginUserService.execute();
		} catch (RemoteInvocationException rie) {
			throw new UnavailableServiceException();
		} catch (LoginBubbleDocsException e){
			throw e;								//fazer throw da excepção?
		}
	}
}
