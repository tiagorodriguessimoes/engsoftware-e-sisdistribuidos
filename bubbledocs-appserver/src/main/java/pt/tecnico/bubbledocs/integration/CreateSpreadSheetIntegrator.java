package pt.tecnico.bubbledocs.integration;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.service.CreateSpreadSheetService;
import pt.tecnico.bubbledocs.service.GetUsername4TokenService;

public class CreateSpreadSheetIntegrator extends BubbleDocsIntegrator{

	private String token;
	private String spreadSheetId;
	private String username;
	private int rows;
	private int columns;
	private byte[] content;
	
	public CreateSpreadSheetIntegrator(String token, String spreadSheetId, int rows, int columns){
		this.token = token;
		this.spreadSheetId = spreadSheetId;
		this.rows = rows;
		this.columns = columns;
		GetUsername4TokenService userInfo = new GetUsername4TokenService(token);
		this.username = userInfo.getResult();
		this.content = new byte[0];						// EMPTY CONTENT
		
	}
	@Override
	protected void dispatch() throws BubbleDocsException {
		CreateSpreadSheetService createSpreadSheetLocal = new CreateSpreadSheetService(this.token, this.spreadSheetId, this.rows, this.columns);
		createSpreadSheetLocal.execute();
		StoreRem.storeDocument(this.username, this.spreadSheetId, this.content );
	}
}
