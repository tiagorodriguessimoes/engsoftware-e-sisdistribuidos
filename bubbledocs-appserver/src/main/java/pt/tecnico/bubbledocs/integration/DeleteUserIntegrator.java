package pt.tecnico.bubbledocs.integration;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.tecnico.bubbledocs.exception.UnavailableServiceException;
import pt.tecnico.bubbledocs.service.DeleteUserService;

public class DeleteUserIntegrator extends BubbleDocsIntegrator{
	private DeleteUserService deleteuserlocal;
	private String userToken;
	private String toDeleteUsername;
	
	public DeleteUserIntegrator(String userToken, String name) {
		this.userToken = userToken;
		this.toDeleteUsername = name;
		deleteuserlocal = new DeleteUserService(this.userToken, this.toDeleteUsername);
	}


	@Override
	protected void dispatch() throws BubbleDocsException {
		deleteuserlocal.execute();
		try{
			IDRem.removeUser(this.toDeleteUsername);
		} catch(RemoteInvocationException e){
			//new CreateUserService(this.userToken, this.toDeleteUsername, this.email, this.name);
			throw new UnavailableServiceException();
		} catch(LoginBubbleDocsException l){
			throw l;	
		}

	}
}
