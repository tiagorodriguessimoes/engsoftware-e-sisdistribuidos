package pt.tecnico.bubbledocs.integration;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.CannotStoreDocumentException;

import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UnavailableServiceException;

import pt.tecnico.bubbledocs.service.ExportDocumentService;

import pt.tecnico.bubbledocs.service.GetUsername4TokenService;

public class ExportDocumentIntegrator extends BubbleDocsIntegrator{

	private String userToken;
	private String username;
	private String docID;
	private byte[] doc;
	public ExportDocumentIntegrator(String userToken, String docID) {
		this.userToken = userToken;
		this.docID = docID;
		GetUsername4TokenService n4t=new GetUsername4TokenService(userToken);
		this.username=n4t.getResult();
		
	}
	
	public byte[] getDocAsByte(){
		return doc;
	}
	@Override
	protected void dispatch() throws BubbleDocsException {
		ExportDocumentService service ;
		try{	
			service  =new  ExportDocumentService(userToken,docID);
			service.execute();
			doc=service.getDocInByte();
			StoreRem.storeDocument(username,docID, service.getDocInByte());
		} catch(RemoteInvocationException | CannotStoreDocumentException c){
			throw new UnavailableServiceException();
		} catch(SpreadSheetDoesNotExist s){
			throw s;
		} 
		
	}
}