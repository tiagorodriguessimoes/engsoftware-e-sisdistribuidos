package pt.tecnico.bubbledocs.integration;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;
import pt.tecnico.bubbledocs.service.AssignReferenceCellService;

public class AssignReferenceCellIntegrator extends BubbleDocsIntegrator{

	private String username;
	private String ssID;
	private String cellID;
	private String cellToRefer;

	public AssignReferenceCellIntegrator(String username, String ssId, String cellId, String celltoRefer) {
		this.username = username;
		this.ssID = ssId;
		this.cellID = cellId;
		this.cellToRefer = celltoRefer;
	}

	@Override
	protected void dispatch() throws BubbleDocsException {
		try{
			AssignReferenceCellService service = new AssignReferenceCellService(this.username, this.ssID, this.cellID, this.cellToRefer);
			service.execute();
		}
		catch (LoginBubbleDocsException e){
			throw e;
		} catch(UserNotInSessionException | UserNoPermissionException | UserDoesNotExistException | SpreadSheetDoesNotExist a){
			throw a;
		}
		
	}
}
