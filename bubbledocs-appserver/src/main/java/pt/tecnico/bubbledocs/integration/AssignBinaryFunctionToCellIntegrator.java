package pt.tecnico.bubbledocs.integration;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;
import pt.tecnico.bubbledocs.service.AssignBinaryFucntionToCellService;

public class AssignBinaryFunctionToCellIntegrator extends BubbleDocsIntegrator{

	private String username;
	private String ssID;
	private String cellID;
	private String function;
	
	public AssignBinaryFunctionToCellIntegrator(String username, String ssId, String cellId, String func) {
		this.username = username;
		this.ssID = ssId;
		this.cellID = cellId;
		this.function = func;
	}

	@Override
	protected void dispatch() throws BubbleDocsException {
		try{
			AssignBinaryFucntionToCellService service = new AssignBinaryFucntionToCellService(this.username, this.ssID, this.cellID, this.function);
			service.execute();
		}
		catch (LoginBubbleDocsException e){
			throw e;
		} catch(UserNotInSessionException | UserNoPermissionException | UserDoesNotExistException | SpreadSheetDoesNotExist a){
			throw a;
		}
		
	}
}
