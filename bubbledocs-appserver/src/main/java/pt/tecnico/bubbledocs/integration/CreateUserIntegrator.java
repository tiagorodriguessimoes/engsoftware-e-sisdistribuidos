package pt.tecnico.bubbledocs.integration;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.DuplicateEmailException;
import pt.tecnico.bubbledocs.exception.DuplicateUsernameException;
import pt.tecnico.bubbledocs.exception.InvalidEmailException;
import pt.tecnico.bubbledocs.exception.InvalidUsernameException;
import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.tecnico.bubbledocs.exception.UnavailableServiceException;
import pt.tecnico.bubbledocs.service.CreateUserService;
import pt.tecnico.bubbledocs.service.DeleteUserService;

public class CreateUserIntegrator extends BubbleDocsIntegrator{
	private String userToken;
	private String newUsername;
	private String name;
	private String email;
	
	public CreateUserIntegrator(String userToken, String newUsername, String email, String name) {
		this.userToken = userToken;
		this.newUsername = newUsername;
		this.name = name;
		this.email = email;
	}
	
	
	@Override
	protected void dispatch() throws BubbleDocsException {
		CreateUserService createuserlocal = new CreateUserService(this.userToken, this.newUsername, this.email, this.name);
		createuserlocal.execute();
		try{
			IDRem.createUser(this.newUsername, this.email);
		} catch(RemoteInvocationException e){
			System.out.println("####################oioiioio");
			new DeleteUserService(this.userToken, this.newUsername);
			throw new UnavailableServiceException();
		} catch(InvalidUsernameException | DuplicateUsernameException | DuplicateEmailException | InvalidEmailException a){
			new DeleteUserService(this.userToken, this.newUsername);
			throw a;
		} 
		
	}
}
