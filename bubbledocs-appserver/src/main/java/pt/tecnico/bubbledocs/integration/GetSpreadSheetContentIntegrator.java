package pt.tecnico.bubbledocs.integration;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UnavailableServiceException;
import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;
import pt.tecnico.bubbledocs.service.GetSpreadSheetContentService;

public class GetSpreadSheetContentIntegrator extends BubbleDocsIntegrator{
	private String documentId;
	private String token;
	
	public GetSpreadSheetContentIntegrator(String token, String documentId) {
		this.documentId = documentId;
		this.token = token;
	}

	@Override
	protected void dispatch() throws BubbleDocsException {
		try{
			GetSpreadSheetContentService spreadsheetcontentlocal = new GetSpreadSheetContentService(this.token, this.documentId);
			spreadsheetcontentlocal.execute();
		} catch (RemoteInvocationException rie) {
			throw new UnavailableServiceException();
		} catch (LoginBubbleDocsException e){
			throw e;
		} catch(UserNotInSessionException | UserNoPermissionException | UserDoesNotExistException | SpreadSheetDoesNotExist a){
			throw a;
		}
	}
}
