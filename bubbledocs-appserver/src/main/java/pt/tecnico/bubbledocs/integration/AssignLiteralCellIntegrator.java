package pt.tecnico.bubbledocs.integration;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;
import pt.tecnico.bubbledocs.service.AssignLiteralCellService;

public class AssignLiteralCellIntegrator extends BubbleDocsIntegrator{
	
	private String username;
	private String ssID;
	private String cellID;
	private String literal;

	public AssignLiteralCellIntegrator(String username, String ssId, String cellId, String literal) {
		this.username = username;
		this.ssID = ssId;
		this.cellID = cellId;
		this.literal = literal;
	}

	@Override
	protected void dispatch() throws BubbleDocsException {
		try{
			AssignLiteralCellService service = new AssignLiteralCellService(this.username, this.ssID, this.cellID, this.literal);
			service.execute();
		}
		catch (LoginBubbleDocsException e){
			throw e;
		} catch(UserNotInSessionException | UserNoPermissionException | UserDoesNotExistException | SpreadSheetDoesNotExist a){
			throw a;
		}
	}
}