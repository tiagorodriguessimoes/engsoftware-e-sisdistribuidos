package pt.tecnico.bubbledocs.integration;

import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.tecnico.bubbledocs.exception.UnavailableServiceException;
import pt.tecnico.bubbledocs.service.RenewPasswordService;

public class RenewPasswordIntegrator extends BubbleDocsIntegrator {

	private String username;
	
	public RenewPasswordIntegrator(String userId){
		this.username = userId;
	}
	
	@Override
	protected void dispatch() throws BubbleDocsException {
		RenewPasswordService renewpasswordlocal = new RenewPasswordService(this.username);
		renewpasswordlocal.execute();
		try{
			IDRem.renewPassword(this.username);
		}
		catch(RemoteInvocationException e){
			throw new UnavailableServiceException();
		}
		catch(LoginBubbleDocsException l){
			throw l;
		}
	}
}
