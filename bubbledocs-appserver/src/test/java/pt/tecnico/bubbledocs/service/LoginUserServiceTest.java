package pt.tecnico.bubbledocs.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.joda.time.LocalTime;
import org.joda.time.Seconds;
import org.junit.Test;

import pt.tecnico.bubbledocs.domain.BubbleDocs;
import pt.tecnico.bubbledocs.domain.SessionManager;
import pt.tecnico.bubbledocs.domain.SessionUser;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;

// add needed import declarations

public class LoginUserServiceTest extends BubbleDocsServiceTest {

	private static final String USERNAME_JOAO = "jp123";
	private static final String NAME_JOAO = "joao";
	private static final String EMAIL_JOAO = "joao@as";
	private static final String PASSWORD = "jp#";
	private static final String USERNAME_INVALID = "random";
	private static final String PASSWORD_INVALID = "jpp#";

	@Override
	public void populate4Test() {
		createUser(USERNAME_JOAO, NAME_JOAO,EMAIL_JOAO);
	}

	// returns the time of the last access for the user with token userToken.
	// It must get this data from the session object of the application
	private LocalTime getLastAccessTimeInSession(String userToken) {

		BubbleDocs bd = BubbleDocs.getInstance();
		SessionManager sm = bd.getSessionmanager();
		for (SessionUser su : sm.getSessionuserSet()) {
			if (su.getToken().equals(userToken)) {
				return 	su.getTime();
			}
		}
		return null;
		// add code here
	}

	@Test
	public void success() {

		LoginUserService service = new LoginUserService(USERNAME_JOAO, PASSWORD);
		service.execute();
		LocalTime currentTime = new LocalTime();
		String token = service.getUserToken();
		User user = getUserFromSession(token);

		assertEquals(USERNAME_JOAO, user.getUsername());
		int difference = Seconds.secondsBetween(getLastAccessTimeInSession(token), currentTime).getSeconds();
		assertTrue("Access time in session not correctly set", difference >= 0);
		assertTrue("diference in seconds greater than expected", difference < 2);
	}

	@Test
	public void successLoginTwice() {
		LoginUserService service = new LoginUserService(USERNAME_JOAO, PASSWORD);
		service.execute();
		service.getUserToken();
		service.execute();
		String token2 = service.getUserToken();
		User user = getUserFromSession(token2);
		assertEquals(USERNAME_JOAO, user.getUsername());
	}

	@Test(expected = LoginBubbleDocsException.class)
	public void loginUnknownUser() {
		LoginUserService service = new LoginUserService(USERNAME_INVALID, PASSWORD_INVALID);
		service.execute();
	}
}
