package pt.tecnico.bubbledocs.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.Cell;
import pt.tecnico.bubbledocs.domain.Literal;
import pt.tecnico.bubbledocs.domain.Reference;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;

public class GetSpreadSheetContentServiceTest extends BubbleDocsServiceTest {
	
	private static final String USERNAME_JOSE = "jose";
	private static final String NAME_JOSE = "jose";
	private static final String EMAIL_JOSE = "jose@email";
    private static final String USERNAME_ANTONIO = "Antonio";
    private static final String EMAIL_ANTONIO = "antonio@es.com";
    private static final String NAME_ANTONIO ="António";
    private static final String SPREADSHEET_1 = "spreadsheet1";
    private static final String SID_1 = "id1";
    private static final String SPREADSHEET_2 = "spreadsheet1";
    private static final String SID_2 = "id2";
    private static final String USERNAME_ROOT = "root";
    private String ars;
    private String ars2;
    
    @Override
    public void populate4Test() {
    	addUserToSession(USERNAME_ROOT);
		User a = createUser(USERNAME_ANTONIO, NAME_ANTONIO,EMAIL_ANTONIO);
		ars = addUserToSession(USERNAME_ANTONIO);
		createUser(USERNAME_JOSE, NAME_JOSE,EMAIL_JOSE);
		ars2 = addUserToSession(USERNAME_JOSE);
		
		SpreadSheet ss = createSpreadSheet(a,SPREADSHEET_1, 5, 5, SID_1);
        Cell cell = ss.getCellfromSS(1, 1);
        Literal literal = new Literal(10);
        Cell cell2 = ss.getCellfromSS(1,2);
        Reference ref = new Reference(cell);
        cell.setContent(literal);
        cell.setHavecontent(true);
        cell2.setContent(ref);
        cell2.setHavecontent(true);
    }

    @Test
    public void success() {
    	GetSpreadSheetContentService service = new GetSpreadSheetContentService(ars, SID_1);
    	service.execute();
    	assertEquals(5, service.getResult().length);     /// Numero de linhas da matrix
    	assertEquals(5, service.getResult()[0].length);    //Numero de cols da matrix
    	assertEquals(String.valueOf(10), service.getResult()[0][0]);
    	assertEquals(String.valueOf(10), service.getResult()[0][1]);
    }
    
    @Test(expected = SpreadSheetDoesNotExist.class)
    public void SpreadSheetDoesNotExist(){
    	GetSpreadSheetContentService service = new GetSpreadSheetContentService(ars, SID_2);
    	service.execute();
    }
    
    @Test(expected = UserNoPermissionException.class)
    public void UserNoPermission(){
    	GetSpreadSheetContentService service = new GetSpreadSheetContentService(ars2, SID_1);
    	service.execute(); 
    }
}
