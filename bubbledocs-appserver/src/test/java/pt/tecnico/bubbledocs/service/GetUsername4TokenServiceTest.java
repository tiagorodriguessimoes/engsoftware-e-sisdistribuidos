package pt.tecnico.bubbledocs.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import pt.tecnico.bubbledocs.exception.SessionExpiredException;

public class GetUsername4TokenServiceTest extends BubbleDocsServiceTest{
	private static final String USERNAME_ANTONIO = "António";
    private static final String EMAIL_ANTONIO = "antonio@es.com";
    private static final String NAME_ANTONIO ="António";
    private static final String root = "root";
    private static final String TOKEN_RANDOM = "tok";

    @Override
    public void populate4Test() {
    	addUserToSession("root");
        createUser(USERNAME_ANTONIO, NAME_ANTONIO, EMAIL_ANTONIO);
        addUserToSession("ars");
    }

    @Test
    public void success() {
    	GetUsername4TokenService service = new GetUsername4TokenService(root);
    	service.execute();
    	assertEquals(root,service.getResult());
    }
    
    @Test(expected = SessionExpiredException.class)
    public void tokenDoesNotExist(){
    	GetUsername4TokenService service = new GetUsername4TokenService(TOKEN_RANDOM);
    	service.execute();
    }
}
