package pt.tecnico.bubbledocs.service;

import static org.junit.Assert.*;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.NotSessionNorRootException;
import pt.tecnico.bubbledocs.exception.RootNotInSessionException;
import pt.tecnico.bubbledocs.exception.SessionExpiredException;

public class DeleteUserServiceTest extends BubbleDocsServiceTest {

	private static final String USERNAME_TO_DELETE = "smf";
	private static final String USERNAME = "ars";
	private static final String ROOT_USERNAME = "root";
	private static final String USERNAME_DOES_NOT_EXIST = "no-one";
	private static final String SPREADSHEET_NAME = "spread";
	private static final String SPREADSHEET_ID = "spreadsheet1";
	private String root;

	@Override
	public void populate4Test() {
		createUser(USERNAME,"António","ars@es2015");
		User smf = createUser(USERNAME_TO_DELETE, "Sérgio Fernandes","smf@es");
		createSpreadSheet(smf, SPREADSHEET_NAME, 20, 20, SPREADSHEET_ID);
		root = addUserToSession(ROOT_USERNAME);
	};

	public void success() {
		DeleteUserService service = new DeleteUserService(root, USERNAME_TO_DELETE);
		service.execute();
		boolean deleted = getUserFromUsername(USERNAME_TO_DELETE) == null;
		assertTrue("user was not deleted", deleted);
		assertNull("Spreadsheet was not deleted", getSpreadSheet(SPREADSHEET_NAME));
	}

	/*
	 * accessUsername exists, is in session and is root toDeleteUsername exists
	 * and is not in session
	 */
	@Test
	public void successToDeleteIsNotInSession() {
		success();
	}

	/*
	 * accessUsername exists, is in session and is root toDeleteUsername exists
	 * and is in session Test if user and session are both deleted
	 */
	@Test(expected = SessionExpiredException.class)
	public void successToDeleteIsInSession() {
		addUserToSession(ROOT_USERNAME);
		String token = addUserToSession(USERNAME_TO_DELETE);
		DeleteUserService service = new DeleteUserService(root,USERNAME_TO_DELETE);
		service.execute();
		getUserFromSession(token);
	}

	@Test(expected = NotSessionNorRootException.class)
	public void notRootUser() {
		String ars = addUserToSession(USERNAME);
		new DeleteUserService(ars, USERNAME_TO_DELETE).execute();
	}

	@Test(expected = RootNotInSessionException.class)
	public void rootNotInSession() {
		removeUserFromSession(root);
		new DeleteUserService(root, USERNAME_TO_DELETE).execute();
	}

	@Test(expected = NotSessionNorRootException.class)
	public void notInSessionAndNotRoot() {
		String ars = addUserToSession(USERNAME);
		removeUserFromSession(ars);
		new DeleteUserService(ars, USERNAME_TO_DELETE).execute();
	}

	@Test(expected = NotSessionNorRootException.class)
	public void accessUserDoesNotExist() {
		new DeleteUserService(USERNAME_DOES_NOT_EXIST, USERNAME_TO_DELETE).execute();
	}
}