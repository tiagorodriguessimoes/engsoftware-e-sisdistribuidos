package pt.tecnico.bubbledocs.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import pt.tecnico.bubbledocs.exception.DuplicateUsernameException;
import pt.tecnico.bubbledocs.exception.InvalidEmailException;
import pt.tecnico.bubbledocs.exception.InvalidUsernameException;
import pt.tecnico.bubbledocs.exception.SessionExpiredException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;

public class CreateUserServiceTest extends BubbleDocsServiceTest {
 	
    private String ars;
    private static final String root = "root";
    private static final String USERNAME_2_SMALL = "i";
    private static final String NAME_2_SMALL = "i es";
    private static final String EMAIL_2_SMALL = "i@es";
    private static final String USERNAME_2_BIG = "qwertyuiop";
    private static final String NAME_2_BIG = "i es";
    private static final String EMAIL_2_BIG = "i@es";
    private static final String USERNAME_EMPTY = "";
    private static final String NAME_EMPTY = "";
    private static final String EMAIL_EMPTY = "";
    private static final String USERNAME_JOSE = "jose";
    private static final String NAME_JOSE ="José Ferreira";
    private static final String EMAIL_JOSE ="JoseFerreira@es.com";
    private static final String WRONG_TOKEN = "rootie";
    private static final String USERNAME_ANTONIO = "António";
    private static final String EMAIL_ANTONIO = "antonio@es.com";
    private static final String NAME_ANTONIO ="António Rito Silva";

    @Override
    public void populate4Test() {
    	addUserToSession(root);
        createUser(USERNAME_ANTONIO, NAME_ANTONIO, EMAIL_ANTONIO);
        ars = addUserToSession("ars");
    }

    @Test
    public void success() {
        CreateUserService service = new CreateUserService(root, USERNAME_JOSE, EMAIL_JOSE,NAME_JOSE);
        service.execute();
        assertEquals(true, hasUser(USERNAME_JOSE));
    }

    @Test(expected = InvalidUsernameException.class)
    public void username2Small(){
    	CreateUserService service = new CreateUserService(root,USERNAME_2_SMALL,EMAIL_2_SMALL,NAME_2_SMALL);
    	service.execute();
    }
    
    @Test(expected = InvalidUsernameException.class)
    public void username2Big(){
    	CreateUserService service = new CreateUserService(root,USERNAME_2_BIG,EMAIL_2_BIG,NAME_2_BIG);
    	service.execute();
    }
    
    @Test(expected = DuplicateUsernameException.class)
    public void usernameExists() {
        CreateUserService service = new CreateUserService(root, USERNAME_ANTONIO, EMAIL_ANTONIO,NAME_ANTONIO);
        service.execute();
    }

    @Test(expected = InvalidUsernameException.class)
    public void emptyUsername() {
        CreateUserService service = new CreateUserService(root, USERNAME_EMPTY, EMAIL_EMPTY,NAME_EMPTY);
        service.execute();
    }
    
    @Test(expected = InvalidEmailException.class)
    public void invalidEmail() {
        CreateUserService service = new CreateUserService(root, USERNAME_JOSE, EMAIL_EMPTY,NAME_EMPTY);
        service.execute();
    }

    @Test(expected = UserNoPermissionException.class)
    public void unauthorizedUserCreation() {
        CreateUserService service = new CreateUserService(ars, USERNAME_ANTONIO,EMAIL_ANTONIO,NAME_ANTONIO);	
        service.execute();
    }

    @Test(expected = SessionExpiredException.class)
    public void accessUsernameNotExist() {
        removeUserFromSession(root);
        CreateUserService service = new CreateUserService(root,USERNAME_ANTONIO,EMAIL_ANTONIO,NAME_ANTONIO);
        service.execute();
    }
    
    @Test(expected = UserNoPermissionException.class)
    public void invalidToken(){
    	CreateUserService service = new CreateUserService(WRONG_TOKEN, USERNAME_ANTONIO,EMAIL_ANTONIO,NAME_ANTONIO);
    	service.execute();
    }
}
