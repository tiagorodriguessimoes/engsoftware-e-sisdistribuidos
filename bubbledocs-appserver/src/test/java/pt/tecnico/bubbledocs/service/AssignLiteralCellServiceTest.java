package pt.tecnico.bubbledocs.service;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.Cell;
import pt.tecnico.bubbledocs.domain.Literal;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.OperationException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;


public class AssignLiteralCellServiceTest extends BubbleDocsServiceTest {

    private static final String USERNAME = "ars";
    private static final String USERNAME2 = "arse";
    private static final String USERNAME_DOES_NOT_EXIST = "no-one";
    
    @Override
    public void populate4Test() {

        User a = createUser(USERNAME, "António Rito Silva","jf@es2015.crl");
        addUserToSession(USERNAME);
        SpreadSheet ss = createSpreadSheet(a,"examplesheet", 7, 4, "id0");
        
        createUser(USERNAME2, "Yolanda","yola@es2015.crl");
        addUserToSession(USERNAME2);
        
        for(Cell c : ss.getCellSet()){		
			if(c.getLin() == 2 && c.getCol() == 3){
				c.setContent(new Literal (5));
				c.setHavecontent(true);
			}
			if(c.getLin() == 6 && c.getCol() == 1){
				c.setContent(new Literal (10));
				c.setHavecontent(true);
			}
			if(c.getLin() == 3 && c.getCol() == 3){
				c.setContent(new Literal (9));
				c.setHavecontent(true);
			}
        }
    }
    
    @Test
    public void success() {
    	AssignLiteralCellService service = new AssignLiteralCellService(USERNAME, "id0", "2;3", "5");
        service.execute();
    }

    @Test(expected = UserNotInSessionException.class)
    public void Userdoesntexist() {
    	AssignLiteralCellService service = new AssignLiteralCellService(USERNAME_DOES_NOT_EXIST, "id0", "2;3", "5");
        service.execute();
    }
    
    @Test(expected = SpreadSheetDoesNotExist.class)
    public void SSdoesntexist() {
    	AssignLiteralCellService service = new AssignLiteralCellService(USERNAME, "falseid", "2;3", "5");
        service.execute();
    }
    
    @Test(expected = OperationException.class)
    public void CellisOutofBonds() {
    	AssignLiteralCellService service = new AssignLiteralCellService(USERNAME, "id0", "20;12", "10");
        service.execute();
    }
    
    @Test(expected = UserNoPermissionException.class)
    public void UserWrongWritePermission() {
    	AssignLiteralCellService service = new AssignLiteralCellService(USERNAME2, "id0", "2;3", "10");
        service.execute();
    }
    
}