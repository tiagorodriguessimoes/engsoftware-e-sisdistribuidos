package pt.tecnico.bubbledocs.service;

import static org.junit.Assert.assertEquals;
import mockit.Expectations;
import mockit.Mocked;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.CannotStoreDocumentException;
import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.service.remote.StoreRemoteServices;


public class ExportServiceTest extends BubbleDocsServiceTest{
	
	
	private String token;
	private static final String USERNAME_JOSE = "jose";
    private static final String NAME_JOSE ="José";
    private static final String EMAIL_JOSE ="JoseFerreira@es.com";
	private final static String USERNAME = "DOLAN";
	private final static String PASSWORD = "DOLAN";
	private final static String USERNAME_INVALID = "randomGuy";
	private static final String DOCUMENT_NAME = "docName";
	private static final byte[] DOCUMENT_EMPTY = DOCUMENT_NAME.getBytes();
	private static final String USERNAME_INVALID_TOKEN = "toktok";

	private static final String NAME = "ES_Sheet";
	private static final int ROWS = 3;
	private static final int COLUMNS = 3;
	private static final String SID = "fd";
	private SpreadSheet s;
	@Override
	public void populate4Test(){

		User user = createUser(USERNAME_JOSE,NAME_JOSE,EMAIL_JOSE);
    	addUserToSession("root");
        token = addUserToSession(USERNAME_JOSE);
        s= createSpreadSheet(user, NAME, ROWS, COLUMNS, SID);

	}
	@Test
	public void success(){
		ExportDocumentService service = new ExportDocumentService(token,s.getId());
		service.execute();
		org.jdom2.Document serviceGen = service.getDoc();
		org.jdom2.Document test= new  org.jdom2.Document();
		test.setRootElement(s.exportToXML());

		String testFounder = test.getRootElement().getAttributeValue(
				"founder");
		String sGenFounder = serviceGen.getRootElement().getAttributeValue(
				"founder");
		String testName = test.getRootElement().getAttributeValue(
				"name");
		String sGenName = serviceGen.getRootElement().getAttributeValue(
				"name");
		String testID = test.getRootElement().getAttributeValue(
				"id");
		String sGenID = serviceGen.getRootElement().getAttributeValue(
				"id");
		String testML = test.getRootElement().getAttributeValue(
				"maxlines");
		String sGenML = serviceGen.getRootElement().getAttributeValue(
				"maxlines");
		String testMC = test.getRootElement().getAttributeValue(
				"maxcolumns");
		String sGenMC = serviceGen.getRootElement().getAttributeValue(
				"maxcolumns");
		String testData = test.getRootElement().getAttributeValue(
				"data");
		String sGenData = serviceGen.getRootElement().getAttributeValue(
				"data");        
		assertEquals(testFounder, sGenFounder);
		assertEquals(testName, sGenName);
		assertEquals(testID, sGenID);
		assertEquals(testML, sGenML);
		assertEquals(testMC, sGenMC);
		assertEquals(testData, sGenData);

	}
	@Test(expected = SpreadSheetDoesNotExist.class)
	public void inexistantSpreadSheet() {
		ExportDocumentService service = new ExportDocumentService(token,"bananas");
		service.execute();
	}
	
}
