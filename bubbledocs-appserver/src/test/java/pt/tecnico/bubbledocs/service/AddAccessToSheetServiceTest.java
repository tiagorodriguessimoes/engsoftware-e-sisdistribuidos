package pt.tecnico.bubbledocs.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.Access;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;

public class AddAccessToSheetServiceTest extends BubbleDocsServiceTest {

	// the tokens

	private String ars;

	private static final String USERNAME = "ars";
	private static final String SSID = "ss10";
	private static final String SPREADSHEET_NAME = "spread";
	private static final boolean TYPE = true;

	@Override
	public void populate4Test() {
		User u = createUser(USERNAME, "António Rito Silva","jf@es2015.crl");
		addUserToSession("root");
		ars = addUserToSession(USERNAME);
		//createSpreadSheet addAccess already in it body
		createSpreadSheet(u, SPREADSHEET_NAME, 20, 20, SSID);
	};

	@Test
	public void success() {
		AddAccessToSheetService service = new AddAccessToSheetService(USERNAME, SSID, TYPE);
		service.execute();

		Access ac = null;
		for(Access a : getSpreadSheet(SPREADSHEET_NAME).getAccessSet()){
			if(a.getUser().getUsername().equals(getUserFromSession(ars).getUsername())){
				ac = a;
			}	
		}
		assertEquals(ac.getUser().getUsername(),getUserFromSession(ars).getUsername());
		Access ace = null;
		for(Access a : getUserFromUsername(USERNAME).getAccessSet()){
			if(a.getSpreadsheet().getName().equals(getSpreadSheet(SPREADSHEET_NAME).getName())){
				ace = a;
			}
		}
		assertEquals(ace.getSpreadsheet().getName(),getSpreadSheet(SPREADSHEET_NAME).getName());
	}

	@Test(expected = UserNotInSessionException.class)
	public void UserNotInSessionException() {
		removeUserFromSession(ars);
		AddAccessToSheetService service = new AddAccessToSheetService(USERNAME, SSID, TYPE);
		service.execute();
	}
}
