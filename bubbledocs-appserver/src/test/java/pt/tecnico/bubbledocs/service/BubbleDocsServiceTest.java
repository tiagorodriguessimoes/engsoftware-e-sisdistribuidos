package pt.tecnico.bubbledocs.service;

import javax.transaction.NotSupportedException;
import javax.transaction.SystemException;

import org.junit.After;
import org.junit.Before;

import pt.ist.fenixframework.FenixFramework;
import pt.ist.fenixframework.core.WriteOnReadError;
import pt.tecnico.bubbledocs.domain.Access;
import pt.tecnico.bubbledocs.domain.BubbleDocs;
import pt.tecnico.bubbledocs.domain.SessionManager;
import pt.tecnico.bubbledocs.domain.SessionUser;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;

public class BubbleDocsServiceTest {

	@Before
	public void setUp() throws Exception {
		try {
			FenixFramework.getTransactionManager().begin(false);
			populate4Test();
		} catch (WriteOnReadError | NotSupportedException | SystemException e1) {
			e1.printStackTrace();
		}
	}

	@After
	public void tearDown() {
		try {
			FenixFramework.getTransactionManager().rollback();
		} catch (IllegalStateException | SecurityException | SystemException e) {
			e.printStackTrace();
		}
	}

	// should redefine this method in the subclasses if it is needed to specify
	// some initial state
	public void populate4Test() {

	}

	// auxiliary methods that access the domain layer and are needed in the test
	// classes
	// for defining the initial state and checking that the service has the
	// expected behavior
	public User createUser(String username, String name, String email) {
		BubbleDocs bd = BubbleDocs.getInstance();
		User u = new User(username, name, email);
		bd.addUser(u);
		return u;
	}

	public SpreadSheet createSpreadSheet(User user, String name, int row, int column, String sid) {
		BubbleDocs bd = BubbleDocs.getInstance();
		SpreadSheet s = new SpreadSheet(name, sid, row, column,user.getUsername());
		bd.addSpreadsheet(s);
		Access a = new Access(true, true);
		user.addAccess(a);
		s.addAccess(a);
		return s;
	}

	// returns a spreadsheet whose name is equal to name
	public SpreadSheet getSpreadSheet(String name) {
		BubbleDocs bd = BubbleDocs.getInstance();
		for(SpreadSheet s:bd.getSpreadsheetSet())
			if(s.getName().equals(name))
				return s;
		return null;
	}

	public boolean hasUser(String username){
		BubbleDocs db = BubbleDocs.getInstance();
		return db.hasUser(username);
	}
	
	// returns the user registered in the application whose username is equal to
	// username
	public User getUserFromUsername(String username) {
		BubbleDocs db = BubbleDocs.getInstance();
		if(!db.hasUser(username))
			return null;
		User u = db.getUserByUsername(username);
		return u;
	}

	// put a user into session and returns the token associated to it
	public String addUserToSession(String username) {
		BubbleDocs db = BubbleDocs.getInstance();
		SessionManager sm = db.getSessionmanager();
		return sm.CreateSession(username);
	}

	// remove a user from session given its token
	public void removeUserFromSession(String token) {
		BubbleDocs db = BubbleDocs.getInstance();
		SessionManager sm = db.getSessionmanager();
		for(SessionUser su : sm.getSessionuserSet())
			if(su.getToken().equals(token))
				su.deleteUser();
	}

	// return the user registered in session whose token is equal to token
	public User getUserFromSession(String token) {
		//System.out.println("TOKEN:" + token);
		BubbleDocs db = BubbleDocs.getInstance();
		SessionManager sm = db.getSessionmanager();
		User u = db.getUserByUsername(sm.getUsernameByToken(token));
		return u;
	}
}
