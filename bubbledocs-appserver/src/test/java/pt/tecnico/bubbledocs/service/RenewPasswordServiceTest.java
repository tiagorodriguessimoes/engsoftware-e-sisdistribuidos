package pt.tecnico.bubbledocs.service;

import static org.junit.Assert.assertNull;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;

public class RenewPasswordServiceTest extends BubbleDocsServiceTest {

	private static final String ROOT_USERNAME = "root";
	private final static String USER_PASS = "usi";
	private static final String USERNAME_NO_ONE = "noone";
	private final static String USERNAME_JOSE = "jose";
	private final static String NAME_JOSE = "jose";
	private final static String EMAIL_JOSE = "jose@email";

	@Override
	public void populate4Test() {
		addUserToSession(ROOT_USERNAME);
		User u = createUser(USERNAME_JOSE,NAME_JOSE, EMAIL_JOSE);
		addUserToSession(USERNAME_JOSE);
		u.setPassword(USER_PASS);
	}

	@Test
	public void success() {
		RenewPasswordService renewservice = new RenewPasswordService(USERNAME_JOSE);
		renewservice.execute();
		assertNull(getUserFromUsername(USERNAME_JOSE).getPassword());
	}

	@Test(expected = UserDoesNotExistException.class)
	public void UserDoesNotExist(){
		RenewPasswordService renewservice = new RenewPasswordService(USERNAME_NO_ONE);
		renewservice.execute();
	}
}