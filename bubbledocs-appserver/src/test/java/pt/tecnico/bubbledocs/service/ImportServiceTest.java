package pt.tecnico.bubbledocs.service;

import static org.junit.Assert.assertEquals;
import mockit.Expectations;
import mockit.Mocked;

import org.jdom2.Document;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.junit.Test;

import pt.tecnico.bubbledocs.domain.ADD;
import pt.tecnico.bubbledocs.domain.ArgLit;
import pt.tecnico.bubbledocs.domain.ArgRef;
import pt.tecnico.bubbledocs.domain.Literal;
import pt.tecnico.bubbledocs.domain.Reference;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.SessionExpiredException;
import pt.tecnico.bubbledocs.service.remote.StoreRemoteServices;

public class ImportServiceTest extends BubbleDocsServiceTest {
	@Mocked
	StoreRemoteServices store;
	
	private final static String USERNAME_DOLAN = "dolan";
	private final static String NAME_DOLAN = "Royal McDolan";
	private final static String EMAIL_DOLAN = "dolan@bestshitEU.com";

	private String token;
	private SpreadSheet s;
	private int ROWS = 3;
	private int COLUMNS = 3;
	private String SID = "dform0";
	private byte[] bytegen;
	private String NAME="DOLANFORM";
	@Override
	public void populate4Test() {

		User user = createUser(USERNAME_DOLAN, NAME_DOLAN, EMAIL_DOLAN);
		addUserToSession("root");
		token = addUserToSession(USERNAME_DOLAN);
		s = createSpreadSheet(user, "DOLANFORM", ROWS, COLUMNS, SID);

	}

	@Test
	public void success() {
		Document doc = new Document();
		
		s.getCellfromSS(1, 1).setContent(new Literal(5));
		s.getCellfromSS(1, 2).setContent(new Reference(s.getCellfromSS(1, 1)));
		s.getCellfromSS(1, 3).setContent(new ADD(new ArgRef(s.getCellfromSS(1, 1)),new ArgRef(s.getCellfromSS(1, 2))));
		s.getCellfromSS(2, 1).setContent(new ADD(new ArgLit(5),new ArgRef(s.getCellfromSS(1, 3))));
		doc.setRootElement(s.exportToXML());
		SpreadSheet test=new SpreadSheet();
		test.importFromXML(doc.getRootElement());
		Document comparable = new Document();
		comparable.setRootElement(test.exportToXML());
//		XMLOutputter xml = new XMLOutputter();
//		xml.setFormat(Format.getPrettyFormat());
//		System.out.println(xml.outputString(doc));
//		System.out.println("#######################################################\n####################################################");
//		System.out.println(xml.outputString(comparable));
		String testFounder = comparable.getRootElement().getAttributeValue(
				"founder");
		String sGenFounder = doc.getRootElement().getAttributeValue(
				"founder");
		String testName = comparable.getRootElement().getAttributeValue(
				"name");
		String sGenName = doc.getRootElement().getAttributeValue(
				"name");
		String testID = comparable.getRootElement().getAttributeValue(
				"id");
		String sGenID = doc.getRootElement().getAttributeValue(
				"id");
		String testML = comparable.getRootElement().getAttributeValue(
				"maxlines");
		String sGenML = doc.getRootElement().getAttributeValue(
				"maxlines");
		String testMC = comparable.getRootElement().getAttributeValue(
				"maxcolumns");
		String sGenMC = doc.getRootElement().getAttributeValue(
				"maxcolumns");
		String testData = comparable.getRootElement().getAttributeValue(
				"data");
		String sGenData = doc.getRootElement().getAttributeValue(
				"data");        
		assertEquals(testFounder, sGenFounder);
		assertEquals(testName, sGenName);
		assertEquals(testID, sGenID);
		assertEquals(testML, sGenML);
		assertEquals(testMC, sGenMC);
		assertEquals(testData, sGenData);

	}
	@Test(expected=SessionExpiredException.class)
	public void sessionexp(){
		removeUserFromSession(token);
		ImportDocumentService IDS = new ImportDocumentService(SID, token);
		IDS.execute();
	}
}











