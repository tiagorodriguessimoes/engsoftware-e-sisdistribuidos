package pt.tecnico.bubbledocs.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;

public class GetUserInfoServiceTest extends BubbleDocsServiceTest{

    private static final String USERNAME_JOSE = "jose";
    private static final String USERNAME_ANTONIO = "António";
    private static final String EMAIL_ANTONIO = "antonio@es.com";
    private static final String NAME_ANTONIO ="António";

    @Override
    public void populate4Test() {
    	addUserToSession("root");
        createUser(USERNAME_ANTONIO, NAME_ANTONIO, EMAIL_ANTONIO);
        addUserToSession("ars");
    }

    @Test
    public void success() {
    	GetUserInfoService service = new GetUserInfoService(USERNAME_ANTONIO);
    	service.execute();
        assertEquals(NAME_ANTONIO, service.getName());
        assertEquals(EMAIL_ANTONIO, service.getEmail());
    }
    
    @Test(expected = UserDoesNotExistException.class)
    public void UserDoesNotExist(){
    	GetUserInfoService service = new GetUserInfoService(USERNAME_JOSE);
    	service.execute();
    }
}
