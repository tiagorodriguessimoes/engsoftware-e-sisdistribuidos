package pt.tecnico.bubbledocs.service;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.ColumnsWithoutSizeException;
import pt.tecnico.bubbledocs.exception.EmptyNameException;
import pt.tecnico.bubbledocs.exception.RowsWithoutSizeException;

public class CreateSpreadSheetServiceTest extends BubbleDocsServiceTest {

    private String ars;
    
    //userfields
    private static final String USERNAME = "ars";
    private static final String USERNAME_DOES_NOT_EXIST = "no-one";
    //ssfields 
    private static final String NAME = "ES_Sheet";
    private static final String SS_DOES_NOT_EXIST = "no-one";
    private static final int ROWS = 10;
    private static final int COLUMNS = 10;
    private static final String SID = "fd";

    @Override
    public void populate4Test() {
    	User user = createUser(USERNAME, "António Rito Silva","ars@es2015.crl");
    	addUserToSession("root");
        ars = addUserToSession(USERNAME);
        createSpreadSheet(user, NAME, ROWS, COLUMNS, SID);
    }

    @Test
    public void success() {
    	
    	
        CreateSpreadSheetService service = new CreateSpreadSheetService(ars, SS_DOES_NOT_EXIST, 5,
                5);
        service.execute();

	// SS is the domain class that represents a SpreadSheet
        SpreadSheet ss = getSpreadSheet(USERNAME_DOES_NOT_EXIST);
        System.out.println(ss.getMaxcolumns());
        assertEquals(USERNAME, getUserFromSession(ars).getUsername());
        assertEquals(USERNAME_DOES_NOT_EXIST, ss.getName());
        assertEquals(Integer.toString(5), Integer.toString(ss.getMaxcolumns()));
        assertEquals(Integer.toString(5), Integer.toString(ss.getMaxlines()));
    }

    @Test(expected = EmptyNameException.class)
    public void emptyUsername() {
    	CreateSpreadSheetService service = new CreateSpreadSheetService(ars, "", 10, 10);
        service.execute();
    }

    @Test(expected = ColumnsWithoutSizeException.class)
    public void columnsWithoutSize() {
    	CreateSpreadSheetService service = new CreateSpreadSheetService(ars, NAME, 10, 0);
        service.execute();
    }

    @Test(expected = RowsWithoutSizeException.class)
    public void rowsWithoutSize() {
    	CreateSpreadSheetService service = new CreateSpreadSheetService(ars, NAME, 0, 10);
        service.execute();
    }
}
