package pt.tecnico.bubbledocs.integration.component;


import static org.junit.Assert.*;
import mockit.Expectations;
import mockit.Mocked;

import org.junit.Test;

import mockit.Expectations;
import mockit.Mocked;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.BubbleDocsException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.SessionExpiredException;
import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;
import pt.tecnico.bubbledocs.integration.DeleteUserIntegrator;
import pt.tecnico.bubbledocs.integration.RenewPasswordIntegrator;
import pt.tecnico.bubbledocs.service.remote.IDRemoteServices;

public class RenewPasswordIntegrationTest extends BubbleDocsIntegrationTest{

	@Mocked
	IDRemoteServices remote;
	
	private static final String root = "root";
	private static final String USERNAME_JOSE = "jose";
	private static final String EMAIL_JOSE ="JoseFerreira@es.com";
	private static final String NAME_JOSE = "José Ferreira";
	private String token;
	
	@Override
	public void populate4Test() {
		addUserToSession(root);
		createUser(USERNAME_JOSE, NAME_JOSE ,EMAIL_JOSE);
		token = addUserToSession(USERNAME_JOSE);
	}
	
	@Test
	public void success(){
		remote.renewPassword(USERNAME_JOSE);
	}
	
	@Test(expected=UserDoesNotExistException.class)
	public void userdoesnotexist(){
		RenewPasswordIntegrator renewpasswordintegrator = new RenewPasswordIntegrator("idonotexist");
		renewpasswordintegrator.execute();
	}
	
	@Test(expected=UserDoesNotExistException.class)
	public void userisnull(){
		RenewPasswordIntegrator renewpasswordintegrator = new RenewPasswordIntegrator(null);
		renewpasswordintegrator.execute();
	}
	
	@Test(expected=UserDoesNotExistException.class)
	public void userisnull2(){
		RenewPasswordIntegrator renewpasswordintegrator = new RenewPasswordIntegrator(" ");
		renewpasswordintegrator.execute();
	}
	
	@Test(expected = SessionExpiredException.class)
	public void UserNotInSession() throws BubbleDocsException {
//		DeleteUserIntegrator removeuserintegrator = new DeleteUserIntegrator(token, NAME_JOSE);
//		removeuserintegrator.execute();
		RenewPasswordIntegrator renewpasswordintegrator = new RenewPasswordIntegrator(USERNAME_JOSE);
		renewpasswordintegrator.execute();
	}
}
