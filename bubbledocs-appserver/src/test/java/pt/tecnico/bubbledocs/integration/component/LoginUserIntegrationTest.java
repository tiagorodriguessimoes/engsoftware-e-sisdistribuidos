package pt.tecnico.bubbledocs.integration.component;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import mockit.Expectations;
import mockit.Mocked;

import org.joda.time.LocalTime;
import org.joda.time.Seconds;
import org.junit.Test;

import pt.tecnico.bubbledocs.domain.BubbleDocs;
import pt.tecnico.bubbledocs.domain.SessionManager;
import pt.tecnico.bubbledocs.domain.SessionUser;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.tecnico.bubbledocs.exception.UnavailableServiceException;
import pt.tecnico.bubbledocs.integration.LoginUserIntegrator;
import pt.tecnico.bubbledocs.service.LoginUserService;
import pt.tecnico.bubbledocs.service.remote.IDRemoteServices;
import pt.tecnico.bubbledocs.service.remote.StoreRemoteServices;

public class LoginUserIntegrationTest extends BubbleDocsIntegrationTest{

	@Mocked
	IDRemoteServices remote;

	private static final String USERNAME = "jp";
	private static final String PASSWORD = "jp#";
	private static final String USERNAME_INVALID = "random";
	private static final String PASSWORD_INVALID = "jpp#";

	@Override
	public void populate4Test() {
		createUser(USERNAME, "José Pedro" , "jp@es2015.crl");
		addUserToSession(USERNAME);
	}

	@Test
	public void success() {
		LoginUserIntegrator service = new LoginUserIntegrator(USERNAME, PASSWORD);
		service.execute();
		LocalTime currentTime = new LocalTime();
		String token = service.getUserToken();
		User user = getUserFromSession(token);

		assertEquals(USERNAME, user.getUsername());
	}

	
	@Test
	public void successLoginTwice() {
		LoginUserService service = new LoginUserService(USERNAME, PASSWORD);
		service.execute();
		service.getUserToken();
		service.execute();
		String token2 = service.getUserToken();
		User user = getUserFromSession(token2);
		assertEquals(USERNAME, user.getUsername());
	}

	@Test(expected = LoginBubbleDocsException.class)
	public void loginUnknownUser() {
		LoginUserIntegrator service = new LoginUserIntegrator("jp2", "jp");
		service.execute();
	}

	/**
	 * @throws LoginBubbleDocsException
	 * @throws RemoteInvocationException
	 * @test try to login with bad password
	 */
	@Test(expected = LoginBubbleDocsException.class)
	public void loginUserWithinWrongPassword() {
		LoginUserIntegrator service = new LoginUserIntegrator(USERNAME, PASSWORD_INVALID);
		service.execute();
	}


	@Test(expected = UnavailableServiceException.class) 
	public void remoteExpt(){
		new Expectations(){
			{
				remote.loginUser(USERNAME,PASSWORD);
				result = new RemoteInvocationException();

			}
		};
		remote.loginUser(USERNAME,PASSWORD);
		LoginUserIntegrator service = new LoginUserIntegrator(USERNAME,PASSWORD);
		service.execute();
	}

}
