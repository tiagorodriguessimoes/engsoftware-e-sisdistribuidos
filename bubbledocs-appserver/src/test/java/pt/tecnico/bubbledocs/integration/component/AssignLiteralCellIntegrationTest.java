package pt.tecnico.bubbledocs.integration.component;

import mockit.Expectations;
import mockit.Mocked;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.Cell;
import pt.tecnico.bubbledocs.domain.Literal;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.OperationException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.exception.UserNotInSessionException;
import pt.tecnico.bubbledocs.integration.AssignLiteralCellIntegrator;
import pt.tecnico.bubbledocs.service.AssignLiteralCellService;
import pt.tecnico.bubbledocs.service.remote.IDRemoteServices;

public class AssignLiteralCellIntegrationTest extends BubbleDocsIntegrationTest{
	
	private static final String USERNAME_JOSE = "jose";
	private static final String EMAIL_JOSE ="JoseFerreira@es.com";
	private static final String USERNAME_ANTONIO = "António";		
	private static final String EMAIL_ANTONIO = "antonio@es.com";
	private static final String USERNAME_DOES_NOT_EXIST = "no-one";

	@Override
	public void populate4Test() {

		User a = createUser(USERNAME_ANTONIO, "António Rito Silva", EMAIL_ANTONIO); 
		addUserToSession(USERNAME_ANTONIO);
		SpreadSheet ss = createSpreadSheet(a,"examplesheet", 7, 4, "id0"); 			

		createUser(USERNAME_JOSE, "José Ferreira" ,EMAIL_JOSE);
		addUserToSession(USERNAME_JOSE);

		for(Cell c : ss.getCellSet()){		
			if(c.getLin() == 2 && c.getCol() == 3){
				c.setContent(new Literal (5));
				c.setHavecontent(true);
			}
			if(c.getLin() == 6 && c.getCol() == 1){
				c.setContent(new Literal (10));
				c.setHavecontent(true);
			}
			if(c.getLin() == 3 && c.getCol() == 3){
				c.setContent(new Literal (9));
				c.setHavecontent(true);
			}
		}
	}

	@Test
	public void success() {
		AssignLiteralCellIntegrator service = new AssignLiteralCellIntegrator(USERNAME_ANTONIO, "id0", "2;3", "5");
		service.execute();

	}

	@Test(expected = UserNotInSessionException.class)
	public void Userdoesntexist() {
		AssignLiteralCellIntegrator service = new AssignLiteralCellIntegrator(USERNAME_DOES_NOT_EXIST, "id0", "2;3", "5");
		service.execute();		
	}

	@Test(expected = SpreadSheetDoesNotExist.class)
	public void SSdoesntexist() {
		AssignLiteralCellIntegrator service = new AssignLiteralCellIntegrator(USERNAME_ANTONIO, "falseid", "2;3", "5");
		service.execute();
	}

	@Test(expected = OperationException.class)
	public void CellisOutofBonds() {
		AssignLiteralCellIntegrator service = new AssignLiteralCellIntegrator(USERNAME_ANTONIO, "id0", "20;12", "10");
		service.execute();
	}

	@Test(expected = UserNoPermissionException.class)
	public void UserWrongWritePermission() {
		AssignLiteralCellIntegrator service = new AssignLiteralCellIntegrator(USERNAME_JOSE, "id0", "2;3", "10");
		service.execute();
	}

}
