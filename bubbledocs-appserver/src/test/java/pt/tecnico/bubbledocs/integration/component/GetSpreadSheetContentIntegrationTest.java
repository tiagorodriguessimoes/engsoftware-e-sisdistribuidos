package pt.tecnico.bubbledocs.integration.component;

import static org.junit.Assert.assertEquals;
import mockit.Expectations;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.Cell;
import pt.tecnico.bubbledocs.domain.Literal;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserDoesNotExistException;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.integration.GetSpreadSheetContentIntegrator;
import pt.tecnico.bubbledocs.service.GetSpreadSheetContentService;

public class GetSpreadSheetContentIntegrationTest extends BubbleDocsIntegrationTest{
	
	private static final String USERNAME_JOSE = "jose";
    private static final String USERNAME_ANTONIO = "Antonio";
    private static final String EMAIL_ANTONIO = "antonio@es.com";
    private static final String EMAIL_JOSE = "ZE@es.com";
    private static final String NAME_ANTONIO ="Antonio";
    private static final String NAME_JOSE ="ZE";
    private static String JOSE_TOKEN = "ars2";
    private static String ANTONIO_TOKEN = "ars";
    private static final String SPREADSHEET_1 = "spreadsheet1";
    private static final String SID_1 = "id1";
    private static final String SPREADSHEET_2 = "spreadsheet1";
    private static final String SID_2 = "id2";

    @Override
    public void populate4Test() {
    	addUserToSession("root");
        User user = createUser(USERNAME_ANTONIO, NAME_ANTONIO, EMAIL_ANTONIO);
        User user2 = createUser(USERNAME_JOSE, "JOSE", EMAIL_JOSE);
        SpreadSheet sheet = createSpreadSheet(user, SPREADSHEET_1, 5, 5, SID_1);
        Cell cell = sheet.getCellfromSS(1, 1);
        Literal literal = new Literal(10);
        cell.setContent(literal);
        ANTONIO_TOKEN = addUserToSession(USERNAME_ANTONIO);
        JOSE_TOKEN = addUserToSession(USERNAME_JOSE);
    }

    @Test
    public void success() {
    	GetSpreadSheetContentIntegrator service = new GetSpreadSheetContentIntegrator(ANTONIO_TOKEN, SID_1);
    	service.execute();
    }
    

    @Test(expected = SpreadSheetDoesNotExist.class)
    public void SpreadSheetDoesNotExist(){
    	GetSpreadSheetContentIntegrator service = new GetSpreadSheetContentIntegrator(ANTONIO_TOKEN, SID_2);
    	service.execute();
    }

    @Test(expected = UserNoPermissionException.class)
    public void UserNoPermission(){
    	GetSpreadSheetContentIntegrator service = new GetSpreadSheetContentIntegrator(JOSE_TOKEN, SID_1);
    	service.execute(); 
    }
	
}
