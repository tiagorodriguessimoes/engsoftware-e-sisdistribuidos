package pt.tecnico.bubbledocs.integration.component;

import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import mockit.Expectations;
import mockit.Mocked;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.DuplicateEmailException;
import pt.tecnico.bubbledocs.exception.DuplicateUsernameException;
import pt.tecnico.bubbledocs.exception.InvalidEmailException;
import pt.tecnico.bubbledocs.exception.InvalidUsernameException;
import pt.tecnico.bubbledocs.exception.LoginBubbleDocsException;
import pt.tecnico.bubbledocs.exception.NotSessionNorRootException;
import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.tecnico.bubbledocs.exception.RootNotInSessionException;
import pt.tecnico.bubbledocs.exception.SessionExpiredException;
import pt.tecnico.bubbledocs.exception.UnavailableServiceException;
import pt.tecnico.bubbledocs.integration.DeleteUserIntegrator;
import pt.tecnico.bubbledocs.service.DeleteUserService;
import pt.tecnico.bubbledocs.service.GetUserInfoService;
import pt.tecnico.bubbledocs.service.remote.IDRemoteServices;


public class RemoveUserIntegrationTest extends BubbleDocsIntegrationTest{
	
	@Mocked
	IDRemoteServices remote;

	private static final String root = "root";
	private static final String USERNAME_2_SMALL = "i";
	private static final String EMAIL_2_SMALL = "i@es";
	private static final String USERNAME_2_BIG = "qwertyuiop";
	private static final String EMAIL_2_BIG = "i@es";
	private static final String USERNAME_JOSE = "jose";
	private static final String EMAIL_JOSE ="JoseFerreira@es.com";
	private static final String USERNAME_ANTONIO = "António";
	private static final String EMAIL_ANTONIO = "antonio@es.com";
	private static final String USERNAME_CARLOS = "Carlos";
	private static final String USERNAME_RITA = "Rita";
	private static final String EMAIL_RITA = "Rita@Es";
	private static final String PASSWORD_RITA = "Rita123";
	private static final String EMAIL_EMPTY = "e";
	private static final String USERNAME_DOES_NOT_EXIST = "no-one";
	private static final String SPREADSHEET_NAME = "spread";
	private static final String SPREADSHEET_ID = "spreadsheet1";
	private static final String USERNAME_TO_DELETE = "smf";

	@Override
	public void populate4Test() {
		createUser(USERNAME_ANTONIO,"António",EMAIL_ANTONIO);
		User smf = createUser(USERNAME_TO_DELETE, "Sérgio Fernandes","smf@es");
		createSpreadSheet(smf, SPREADSHEET_NAME, 20, 20, SPREADSHEET_ID);
		addUserToSession(root);
	}
	@Test
	public void success(){
		DeleteUserService service = new DeleteUserService(root, USERNAME_TO_DELETE);
		service.execute();
		boolean deleted = getUserFromUsername(USERNAME_TO_DELETE) == null;
		assertTrue("user was not deleted", deleted);
		assertNull("Spreadsheet was not deleted", getSpreadSheet(SPREADSHEET_NAME));
	}

	@Test(expected=LoginBubbleDocsException.class)
	public void LoginBubbleDocsException(){
		DeleteUserIntegrator removeuserintegrator = new DeleteUserIntegrator(root, USERNAME_RITA);
		removeuserintegrator.execute();
	}
	
	@Test(expected = SessionExpiredException.class)
	public void successToDeleteIsInSession() {
		addUserToSession(root);
		String token = addUserToSession(USERNAME_TO_DELETE);
		DeleteUserService service = new DeleteUserService(root,USERNAME_TO_DELETE);
		service.execute();
		getUserFromSession(token);
	}

	@Test(expected = NotSessionNorRootException.class)
	public void notRootUser() {
		String ars = addUserToSession(USERNAME_ANTONIO);
		new DeleteUserService(ars, USERNAME_TO_DELETE).execute();
	}

	@Test(expected = RootNotInSessionException.class)
	public void rootNotInSession() {
		removeUserFromSession(root);
		new DeleteUserService(root, USERNAME_TO_DELETE).execute();
	}

	@Test(expected = NotSessionNorRootException.class)
	public void notInSessionAndNotRoot() {
		String ars = addUserToSession(USERNAME_ANTONIO);
		removeUserFromSession(ars);
		new DeleteUserService(ars, USERNAME_TO_DELETE).execute();
	}

	@Test(expected = NotSessionNorRootException.class)
	public void accessUserDoesNotExist() {
		new DeleteUserService(USERNAME_DOES_NOT_EXIST, USERNAME_TO_DELETE).execute();
	}

	@Test(expected=UnavailableServiceException.class)
	public void remoteInvocationMOCK(){
//		new Expectations(){
//			{
//				remote.removeUser(USERNAME_RITA);
//				result = new RemoteInvocationException();
//			}
//		};
//		DeleteUserIntegrator removeuserintegrator = new DeleteUserIntegrator(root, USERNAME_ANTONIO);
//		removeuserintegrator.execute();
	} 
}
