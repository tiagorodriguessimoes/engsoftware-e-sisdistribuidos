package pt.tecnico.bubbledocs.integration.component;

import mockit.Expectations;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.Cell;
import pt.tecnico.bubbledocs.domain.Reference;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.OperationException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.integration.AssignReferenceCellIntegrator;
import pt.tecnico.bubbledocs.service.AddAccessToSheetService;
import pt.tecnico.bubbledocs.service.AssignReferenceCellService;

public class AssignReferenceCellIntegrationTest extends BubbleDocsIntegrationTest {

	// the tokens
	private String root;
	private String ars;

	private static final String USERNAME = "ars";
	private static final String USERNAME2 = "dolan";



	@Override
	public void populate4Test() {    	

		User a = createUser(USERNAME, "António Rito Silva","jf@es2015.crl");
		addUserToSession(USERNAME);
		SpreadSheet ss = createSpreadSheet(a,"examplesheet", 7, 4, "id0");

		User b = createUser(USERNAME2, "Yolanda","yola@es2015.crl");
		addUserToSession(USERNAME2);
		AddAccessToSheetService service = new AddAccessToSheetService(USERNAME2, ss.getId(), false);
		service.execute();

		for(Cell c : ss.getCellSet()){	
			if(c.getLin() == 2 && c.getCol() == 3){
				c.setContent(new Reference(ss.getCellfromSS(1, 1)));
				c.setHavecontent(true);
			}
			if(c.getLin() == 6 && c.getCol() == 1){
				c.setContent(new Reference(ss.getCellfromSS(2, 3)));
				c.setHavecontent(true);
			}
			if(c.getLin() == 3 && c.getCol() == 3){
				c.setContent(new Reference(ss.getCellfromSS(3, 3)));
				c.setHavecontent(true);
			}
		}

	};


	@Test
	public void success() { 
		AssignReferenceCellIntegrator service = new AssignReferenceCellIntegrator(USERNAME, "id0", "2;3", "1;1");
		service.execute();
	}

	@Test(expected = OperationException.class) 
	public void outofbounds(){
		AssignReferenceCellIntegrator service = new AssignReferenceCellIntegrator(USERNAME, "id0", "8;8", "1;1");
		service.execute();
	}


	@Test(expected= SpreadSheetDoesNotExist.class)
	public void noSpread(){
		AssignReferenceCellIntegrator service = new AssignReferenceCellIntegrator(USERNAME, "falseid", "2;3", "5;1");
		service.execute();
	}

	@Test(expected = UserNoPermissionException.class)
	public void UserWrongWritePermission() {
		AssignReferenceCellIntegrator service = new AssignReferenceCellIntegrator(USERNAME2, "id0", "2;3", "1;2");
		service.execute();
	}
}
