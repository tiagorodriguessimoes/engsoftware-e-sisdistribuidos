package pt.tecnico.bubbledocs.integration.component;

import static org.junit.Assert.assertEquals;
import mockit.Expectations;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.ColumnsWithoutSizeException;
import pt.tecnico.bubbledocs.exception.EmptyNameException;
import pt.tecnico.bubbledocs.exception.RowsWithoutSizeException;
import pt.tecnico.bubbledocs.integration.CreateSpreadSheetIntegrator;
import pt.tecnico.bubbledocs.service.CreateSpreadSheetService;

public class CreateSpreadSheetIntegrationTest extends BubbleDocsIntegrationTest {
	private String ars;

	//userfields
	private static final String USERNAME = "ars";
	//ssfields 
	private static final String NAME = "ES_Sheet";
	private static final String SS_DOES_NOT_EXIST = "no-one";
	private static final int ROWS = 10;
	private static final int COLUMNS = 10;
	private static final String SID = "fd";

	@Override
	public void populate4Test() {
		User user = createUser(USERNAME, "António Rito Silva","ars@es2015.crl");
		addUserToSession("root");
		ars = addUserToSession(USERNAME);
		createSpreadSheet(user, NAME, ROWS, COLUMNS, SID);
	}

	@Test
	public void success() {
		CreateSpreadSheetIntegrator service = new CreateSpreadSheetIntegrator(ars, SS_DOES_NOT_EXIST, 5, 5);
		service.execute();

		// SS is the domain class that represents a SpreadSheet
		SpreadSheet ss = getSpreadSheet(SS_DOES_NOT_EXIST);
		assertEquals(SS_DOES_NOT_EXIST, ss.getName());
		assertEquals(Integer.toString(5), Integer.toString(ss.getMaxcolumns()));
		assertEquals(Integer.toString(5), Integer.toString(ss.getMaxlines()));
	}

	@Test(expected = EmptyNameException.class)
	public void emptyUsername() {
		CreateSpreadSheetIntegrator service = new CreateSpreadSheetIntegrator(ars, "", 10, 10);
		service.execute();
	}

	@Test(expected = ColumnsWithoutSizeException.class)
	public void columnsWithoutSize() {
		CreateSpreadSheetIntegrator service = new CreateSpreadSheetIntegrator(ars, NAME, 10, 0);
		service.execute();
	}

	@Test(expected = RowsWithoutSizeException.class)
	public void rowsWithoutSize() {
		CreateSpreadSheetIntegrator service = new CreateSpreadSheetIntegrator(ars, NAME, 0, 10);
		service.execute();
	}
}

