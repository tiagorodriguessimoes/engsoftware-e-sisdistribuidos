package pt.tecnico.bubbledocs.integration.component;

import mockit.Expectations;
import mockit.Mocked;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;

import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UnavailableServiceException;
import pt.tecnico.bubbledocs.integration.ExportDocumentIntegrator;

import pt.tecnico.bubbledocs.service.remote.StoreRemoteServices;


import static org.junit.Assert.assertNotNull;

public class ExportSpreadSheetIntegrationTest extends BubbleDocsIntegrationTest{
	@Mocked
	StoreRemoteServices store;
	
	private static final String USERNAME_JOSE = "jose";
	private static final String EMAIL_JOSE ="JoseFerreira@es.com";
	private static org.jdom2.Document bytegen;
	private String token;
	private String token2;
	private String comparable= null;
	@Override
	public void populate4Test() {    	
		User u= createUser(USERNAME_JOSE, "José Ferreira" ,EMAIL_JOSE);
		User a=createUser("Faker","Faker","Faker@getRekt.com");
		token=addUserToSession(USERNAME_JOSE);
		token2=addUserToSession("Faker");
		SpreadSheet ss = createSpreadSheet(u,"examplesheet", 7, 4, "id0");
		bytegen= new org.jdom2.Document();
		bytegen.setRootElement(ss.exportToXML());
		
	};
	@Test
	public void success(){
		
		ExportDocumentIntegrator EDI= new ExportDocumentIntegrator(token, "id0");
		EDI.execute();

		comparable = new String(EDI.getDocAsByte());
		assertNotNull(comparable);
	}
	
	@Test(expected = SpreadSheetDoesNotExist.class)
	public void spreadDoesNotExist(){
		ExportDocumentIntegrator EDI = new ExportDocumentIntegrator(token, "id1");
		EDI.execute();
	}
	
	
	@Test(expected = UnavailableServiceException.class)
	public void cannotStore(){
		
		removeUserFromSession(token2);
		ExportDocumentIntegrator EDI = new ExportDocumentIntegrator("Faker", "id0");
		EDI.execute();
		
	}
	
	@Test(expected = UnavailableServiceException.class)
	public void Remoteinv(){
		

		new Expectations(){
			{	
				store.storeDocument(USERNAME_JOSE, "id0", bytegen.toString().getBytes());
				result=new UnavailableServiceException();
			}
			
		};
		store.storeDocument(USERNAME_JOSE, "id0", bytegen.toString().getBytes());
		ExportDocumentIntegrator EDI = new ExportDocumentIntegrator(token, "id0");
		EDI.execute();

	}
	
	
}
