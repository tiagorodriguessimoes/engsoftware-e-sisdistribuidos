package pt.tecnico.bubbledocs.integration.component;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import pt.tecnico.bubbledocs.exception.DuplicateEmailException;
import pt.tecnico.bubbledocs.exception.DuplicateUsernameException;
import pt.tecnico.bubbledocs.exception.InvalidEmailException;
import pt.tecnico.bubbledocs.exception.InvalidUsernameException;
import pt.tecnico.bubbledocs.exception.RemoteInvocationException;
import pt.tecnico.bubbledocs.exception.UnavailableServiceException;
import pt.tecnico.bubbledocs.integration.CreateUserIntegrator;
import pt.tecnico.bubbledocs.integration.DeleteUserIntegrator;
import pt.tecnico.bubbledocs.integration.ExportDocumentIntegrator;
import pt.tecnico.bubbledocs.service.remote.IDRemoteServices;
import mockit.Expectations;
import mockit.Mocked;

public class CreateUserIntegrationTest extends BubbleDocsIntegrationTest{

	@Mocked
	IDRemoteServices remote;

	private static final String root = "root";
	private static final String USERNAME_2_SMALL = "i";
	private static final String EMAIL_2_SMALL = "i@es";
	private static final String USERNAME_2_BIG = "qwertyuiop";
	private static final String EMAIL_2_BIG = "i@es";
	private static final String USERNAME_JOSE = "jose";
	private static final String EMAIL_JOSE ="JoseFerreira@es.com";
	private static final String USERNAME_ANTONIO = "António";
	private static final String EMAIL_ANTONIO = "antonio@es.com";
	private static final String USERNAME_CARLOS = "Carlos";
	private static final String USERNAME_RITA = "Rita";
	private static final String EMAIL_RITA = "Rita@Es";
	private static final String EMAIL_EMPTY = "e";
	private static final String NAME_JOSE ="José Ferreira";

	@Override
	public void populate4Test() {
		addUserToSession(root);
		createUser(USERNAME_ANTONIO, "António" ,EMAIL_ANTONIO);

	}
	@Test
	public void success(){
		CreateUserIntegrator createuserintegrator = new CreateUserIntegrator(root, USERNAME_JOSE, EMAIL_JOSE, NAME_JOSE);
		createuserintegrator.execute();
		assertEquals(true, hasUser(USERNAME_JOSE));
	}

	@Test(expected=DuplicateEmailException.class)
	public void duplicatEmail(){
		CreateUserIntegrator createuserintegrator = new CreateUserIntegrator(root, "Duper", EMAIL_ANTONIO, "duplicate");
		createuserintegrator.execute();
	}

	@Test(expected=DuplicateUsernameException.class)
	public void duplicatUsername(){
		CreateUserIntegrator createuserintegrator = new CreateUserIntegrator(root, USERNAME_ANTONIO, "dup@duplicate", "duplicate");
		createuserintegrator.execute();
	}
	@Test(expected=InvalidEmailException.class)
	public void invalidEmail(){
		CreateUserIntegrator createuserintegrator = new CreateUserIntegrator(root, USERNAME_RITA, EMAIL_EMPTY, "rituxa");
		createuserintegrator.execute();
	}

	@Test(expected=InvalidUsernameException.class)
	public void username2Small(){
		CreateUserIntegrator createuserintegrator = new CreateUserIntegrator(root, USERNAME_2_SMALL, EMAIL_2_SMALL, "dolan");
		createuserintegrator.execute();
	}

	@Test(expected=InvalidUsernameException.class)
	public void username2Big(){
		CreateUserIntegrator createuserintegrator = new CreateUserIntegrator(root, USERNAME_2_BIG, EMAIL_2_BIG, "bigguy");
		createuserintegrator.execute();
	}

	
	@Test(expected = UnavailableServiceException.class)
	public void Remoteinv(){
//		remote.createUser(USERNAME_RITA,EMAIL_RITA);
//		CreateUserIntegrator createuserintegrator = new CreateUserIntegrator(root, USERNAME_RITA, EMAIL_RITA, "rituxa");
//		createuserintegrator.execute();

	}
}
