package pt.tecnico.bubbledocs.integration.component;

import static org.junit.Assert.assertEquals;
import mockit.Expectations;

import org.junit.Test;

import pt.tecnico.bubbledocs.domain.Cell;
import pt.tecnico.bubbledocs.domain.Literal;
import pt.tecnico.bubbledocs.domain.SpreadSheet;
import pt.tecnico.bubbledocs.domain.User;
import pt.tecnico.bubbledocs.exception.OperationException;
import pt.tecnico.bubbledocs.exception.SpreadSheetDoesNotExist;
import pt.tecnico.bubbledocs.exception.UserNoPermissionException;
import pt.tecnico.bubbledocs.integration.AssignBinaryFunctionToCellIntegrator;
import pt.tecnico.bubbledocs.service.AssignBinaryFucntionToCellService;

public class AssignBinaryFunctionToCellIntegrationTest extends BubbleDocsIntegrationTest{	
	
    private static final String USERNAME = "ars";
    private static final String USERNAME2 = "arse";
    private static final String USERNAME_DOES_NOT_EXIST = "no-one";
    private String token;
    private String token2;

	@Override
    public void populate4Test() {
        User a = createUser(USERNAME, "António Rito Silva","jf@es2015.crl");
        token = addUserToSession(USERNAME);
        SpreadSheet ss = createSpreadSheet(a,"examplesheet", 7, 4, "id0");
        
        
        createUser(USERNAME2, "Yolanda","yola@es2015.crl");
        token2 = addUserToSession(USERNAME2);
        
        for(Cell c : ss.getCellSet()){		
			if(c.getLin() == 2 && c.getCol() == 3){
				c.setContent(new Literal (5));
				c.setHavecontent(true);
			}
			if(c.getLin() == 6 && c.getCol() == 1){
				c.setContent(new Literal (10));
				c.setHavecontent(true);
			}
			if(c.getLin() == 3 && c.getCol() == 3){
				c.setContent(new Literal (9));
				c.setHavecontent(true);
			}
        }
	}																		
	@Test
    public void success() {
    	AssignBinaryFunctionToCellIntegrator abftcintegrator = new AssignBinaryFunctionToCellIntegrator(USERNAME, "id0", "2;2", "ADD(5,3)");
    	abftcintegrator.execute();
       // assertEquals(8, abftcintegrator.getResult()); //verifica o resultado do servico
	}
	
	@Test(expected = OperationException.class)
	public void outofbounds(){
		AssignBinaryFunctionToCellIntegrator abftcintegrator = new AssignBinaryFunctionToCellIntegrator(USERNAME, "id0", "8;8", "ADD(5,3)");
		abftcintegrator.execute();
	}
	
	@Test(expected = UserNoPermissionException.class)
	public void UserWrongWritePermission() {
		AssignBinaryFunctionToCellIntegrator abftcintegrator = new AssignBinaryFunctionToCellIntegrator(USERNAME2, "id0", "2;3", "ADD(5, 3)");
		abftcintegrator.execute();
	}
	
}
