package pt.ulisboa.tecnico.bubbledocs.domain;

public class User {
	private String userid;
	private String mail;
	private String password;
	
	public User(String userid, String mail) {
		this.userid = userid;
		this.mail = mail;
		//this.password = generateRandomPass();
	}
	
	public String getUserid() {
		return userid;
	}
	
	public void setUserid(String userid) {
		this.userid = userid;
	}
	
	public String getMail() {
		return mail;
	}
	
	public void setMail(String mail) {
		this.mail = mail;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	
	public String toString() {
		return "User [userid=" + userid + ", mail=" + mail + ", password="
				+ password + "]";
	}
}
