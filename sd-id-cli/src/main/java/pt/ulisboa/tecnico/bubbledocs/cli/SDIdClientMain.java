package pt.ulisboa.tecnico.bubbledocs.cli;

import static javax.xml.ws.BindingProvider.ENDPOINT_ADDRESS_PROPERTY;

import java.util.Map;
import java.util.Random;

import javax.xml.registry.JAXRException;
import javax.xml.ws.BindingProvider;

import com.sun.xml.ws.client.RequestContext;

import example.ws.uddi.UDDINaming;
import pt.ulisboa.tecnico.bubbledocs.impl.ReservedXML;
import pt.ulisboa.tecnico.sdis.id.ws.*;
import static javax.xml.bind.DatatypeConverter.printHexBinary;

import java.io.IOException;
import java.io.StringReader;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class SDIdClientMain {
	public static void main(String[] args) throws JAXRException,
			EmailAlreadyExists_Exception, InvalidEmail_Exception,
			InvalidUser_Exception, UserAlreadyExists_Exception,
			SDIdClientException, NoSuchAlgorithmException, 
			InvalidKeyException, IllegalBlockSizeException, 
			BadPaddingException, NoSuchPaddingException{
		if (args.length < 2) {
			System.err.println("Argument(s) missing!");
			System.err.printf("Usage: java %s uddiURL name%n",
					SDIdClient.class.getName());
			return;
		}
		
		String uddiURL = args[0];
		String name = args[1];
		System.out.println("args[0]=" + uddiURL + "|args[1]=" + name
				+ "|userId=" + args[2] + "|password=" + args[3]);
		String userId = args[2];
		String password = args[3];
		SDIdClient client = new SDIdClient(uddiURL);

		System.out.printf("Contacting UDDI at %s%n", uddiURL);
		UDDINaming uddiNaming = new UDDINaming(uddiURL);

		System.out.printf("Looking for '%s'%n", name);
		String endpointAddress = uddiNaming.lookup(name);

		if (endpointAddress == null) {
			System.out.println("Not found!");
			return;
		} else {
			System.out.printf("Found %s%n", endpointAddress);
		}
		client.createStub();

		// converting password to byte array
		final byte[] bPassword = password.getBytes();

		// generating a DES key
		KeyGenerator keyGen = KeyGenerator.getInstance("DES");
		keyGen.init(56);
		Key kc = keyGen.generateKey();

//		// Generating Cipher
		Cipher cipher = Cipher.getInstance("DES/ECB/PKCS5Padding");

//		// Encryptation
		cipher.init(Cipher.ENCRYPT_MODE, kc);
		byte[] encryptedPass = cipher.doFinal(bPassword);

		Random rand = new Random();
		int nounce = rand.nextInt(9999 + 1);
		ReservedXML xml = new ReservedXML("SD-ID", nounce);
		byte[] reserved = xml.convertXMLtoByte();
		byte[] returned = client.requestAuthentication(userId, reserved);
		if(returned==null){System.out.println("NULL RETURN");}
		Document doc=new Document();;
		/*String document = new String(returned);
		try{
			SAXBuilder builder = new SAXBuilder();
			//doc= new Document();
			doc = builder.build(new StringReader(document));}
		catch(JDOMException | IOException e){
			//throw e;
		}
		XMLOutputter out = new XMLOutputter();
		out.setFormat(Format.getPrettyFormat());
		System.out.println(out.outputString(doc));*/
	}
}
