package pt.ulisboa.tecnico.bubbledocs.cli;

import java.util.List;
import java.util.Map;

import javax.xml.ws.BindingProvider;

import pt.ulisboa.tecnico.sdis.id.ws.*;



public class SDIdClient {

	
	/** WS service */
    SDId_Service service = null;

    /** WS port (interface) */
    SDId port = null;

    /** WS endpoint address */
    // default value is defined by WSDL
    private String wsURL = null;


    /** output option **/
    private boolean verbose = false;

    public boolean isVerbose() {
        return verbose;
    }
    public void setVerbose(boolean verbose) {
        this.verbose = verbose;
    }


    /** constructor with provided web service URL */
    public SDIdClient(String wsURL) throws SDIdClientException {
        this.wsURL = "http://localhost:8080/sd-id/endpoint";
    	//this.wsURL = wsURL;
        createStub();
    }

    /** default constructor uses default endpoint address */
    public SDIdClient() {
        createStub();
    }


    /** Stub creation and configuration */
    protected void createStub() {
        if (verbose)
            System.out.println("Creating stub ...");
        service = new SDId_Service();
        port = service.getSDIdImplPort();

        if (wsURL != null) {
            if (verbose)
                System.out.println("Setting endpoint address ...");
            BindingProvider bindingProvider = (BindingProvider) port;
            Map<String, Object> requestContext = bindingProvider.getRequestContext();
            requestContext.put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, wsURL);
        }
    }
    
    
    public void createUser(String userId, String emailAddress){
    	try {
			port.createUser(userId, emailAddress);
			
		} catch (EmailAlreadyExists_Exception e) {
			e.getMessage();
		} catch (InvalidEmail_Exception e) {
			e.getMessage();
		} catch (InvalidUser_Exception e) {
			e.getMessage();
		} catch (UserAlreadyExists_Exception e) {
			e.getMessage();
		}
    }
    
    public void renewPassword(String userId){
    	try {
			port.renewPassword(userId);
		} catch (UserDoesNotExist_Exception e) {
			e.getMessage();
		}
    }
    
    public void removeUser(String userId){
    	try {
			port.removeUser(userId);
		} catch (UserDoesNotExist_Exception e) {
			e.getMessage();
		}
    }
    
    public byte[] requestAuthentication(String userId, byte[] reserved){
    	try {
			port.requestAuthentication(userId, reserved);
		} catch (AuthReqFailed_Exception e) {
			e.getMessage();
		}
		return null;
    	
    }
}
