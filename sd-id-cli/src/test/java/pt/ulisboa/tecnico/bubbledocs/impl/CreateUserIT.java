//package pt.ulisboa.tecnico.sdis.id.ws.it;
package pt.ulisboa.tecnico.bubbledocs.impl;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import pt.ulisboa.tecnico.sdis.id.ws.EmailAlreadyExists_Exception;
import pt.ulisboa.tecnico.sdis.id.ws.InvalidEmail_Exception;
import pt.ulisboa.tecnico.sdis.id.ws.InvalidUser_Exception;
import pt.ulisboa.tecnico.sdis.id.ws.SDId;
import pt.ulisboa.tecnico.sdis.id.ws.SDId_Service;
import pt.ulisboa.tecnico.sdis.id.ws.UserAlreadyExists_Exception;

// classes generated by wsimport from WSDL

/**
 * Test suite
 */
public class CreateUserIT {
	private static SDId ID_CLIENT;

    // tests
    // assertEquals(expected, actual);

    // public void createUser(CreateUser parameters)
    // throws EmailAlreadyExists_Exception, InvalidEmail_Exception,
    // UserAlreadyExists;
	@BeforeClass
	public static void oneTimeSetUp(){
		SDId_Service service = new SDId_Service();
		ID_CLIENT= service.getSDIdImplPort();
	}

	@AfterClass
	public static void oneTimeTearDown(){
		ID_CLIENT = null;
	}
	
    @Test
    public void testCreateUser() throws Exception {
        ID_CLIENT.createUser("user1", "user1@domain.net");
    }

    @Test(expected = InvalidUser_Exception.class)
    public void testCreateNullUser() throws Exception {
        ID_CLIENT.createUser(null, "user2@domain.net");
    }

    @Test(expected = InvalidUser_Exception.class)
    public void testCreateEmptyUser() throws Exception {
        ID_CLIENT.createUser("", "user2@domain.net");
    }

    @Test(expected = EmailAlreadyExists_Exception.class)
    public void testEmailExists() throws Exception {
        ID_CLIENT.createUser("user3a", "user3@domain.net");
        ID_CLIENT.createUser("user3b", "user3@domain.net");
    }

    @Test(expected = InvalidEmail_Exception.class)
    public void testInvalidEmail() throws Exception {
        ID_CLIENT.createUser("user4", "user4");
    }

    @Test(expected = InvalidEmail_Exception.class)
    public void testIncompleteEmail() throws Exception {
        ID_CLIENT.createUser("user5", "user5@");
    }
    
    @Test(expected = InvalidEmail_Exception.class)
    public void testIncompleteEmail2() throws Exception {
        ID_CLIENT.createUser("user6", "@domain.net");
    }

    @Test(expected = InvalidEmail_Exception.class)
    public void testEmptyEmail() throws Exception {
        ID_CLIENT.createUser("user7", "");
    }

    @Test(expected = InvalidEmail_Exception.class)
    public void testNullEmail() throws Exception {
        ID_CLIENT.createUser("user8", null);
    }

    @Test(expected = UserAlreadyExists_Exception.class)
    public void testUserAlreadyExists() throws Exception {
        ID_CLIENT.createUser("user9", "user9a@domain.net");
        ID_CLIENT.createUser("user9", "user9b@domain.net");
    }

}
